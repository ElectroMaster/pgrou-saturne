
CREATE SEQUENCE public.seq_modeevaluation;

CREATE TABLE public.modeevaluation (
                modeevaluation_id INTEGER NOT NULL DEFAULT nextval('public.seq_modeevaluation'),
                modeevaluation_libelle VARCHAR(128) NOT NULL,
                modeevaluation_notevrai REAL NOT NULL,
                modeevaluaton_notefaux REAL NOT NULL,
                CONSTRAINT pk_modeevaluation PRIMARY KEY (modeevaluation_id)
);


ALTER SEQUENCE public.seq_modeevaluation OWNED BY public.modeevaluation.modeevaluation_id;

CREATE SEQUENCE public.seq_typeacces;

CREATE TABLE public.typeacces (
                typeacces_id INTEGER NOT NULL DEFAULT nextval('public.seq_typeacces'),
                typeacces_libelle VARCHAR(32) NOT NULL,
                CONSTRAINT pk_typeacces PRIMARY KEY (typeacces_id)
);


ALTER SEQUENCE public.seq_typeacces OWNED BY public.typeacces.typeacces_id;

CREATE SEQUENCE public.seq_menu;

CREATE TABLE public.menu (
                menu_id INTEGER NOT NULL DEFAULT nextval('public.seq_menu'),
                menu_libelle VARCHAR(64) NOT NULL,
                menu_ordre INTEGER DEFAULT 1 NOT NULL,
                menu_fonction VARCHAR(128),
                menu_pere_id INTEGER,
                CONSTRAINT pk_menu PRIMARY KEY (menu_id)
);


ALTER SEQUENCE public.seq_menu OWNED BY public.menu.menu_id;

CREATE SEQUENCE public.seq_typeimage;

CREATE TABLE public.typeimage (
                typeimage_id INTEGER NOT NULL DEFAULT nextval('public.seq_typeimage'),
                typeimage_libelle VARCHAR(8) NOT NULL,
                typeimage_mime VARCHAR(64) NOT NULL,
                CONSTRAINT pk_typeimage PRIMARY KEY (typeimage_id)
);


ALTER SEQUENCE public.seq_typeimage OWNED BY public.typeimage.typeimage_id;

CREATE SEQUENCE public.seq_image;

CREATE TABLE public.image (
                image_id INTEGER NOT NULL DEFAULT nextval('public.seq_image'),
                image_libelle VARCHAR(64),
                image_contenu BYTEA NOT NULL,
                image_hauteur INTEGER,
                typeimage_id INTEGER NOT NULL,
                CONSTRAINT pk_image PRIMARY KEY (image_id)
);


ALTER SEQUENCE public.seq_image OWNED BY public.image.image_id;

CREATE SEQUENCE public.seq_motcle;

CREATE TABLE public.motcle (
                motcle_id INTEGER NOT NULL DEFAULT nextval('public.seq_motcle'),
                motcle_libelle VARCHAR(64) NOT NULL,
                CONSTRAINT pk_motcle PRIMARY KEY (motcle_id)
);


ALTER SEQUENCE public.seq_motcle OWNED BY public.motcle.motcle_id;

CREATE SEQUENCE public.seq_groupe;

CREATE TABLE public.groupe (
                groupe_id INTEGER NOT NULL DEFAULT nextval('public.seq_groupe'),
                groupe_libelle VARCHAR(32) NOT NULL,
                groupe_valide BOOLEAN DEFAULT TRUE NOT NULL,
                CONSTRAINT pk_groupe PRIMARY KEY (groupe_id)
);


ALTER SEQUENCE public.seq_groupe OWNED BY public.groupe.groupe_id;

CREATE SEQUENCE public.seq_typequestion;

CREATE TABLE public.typequestion (
                typequestion_id INTEGER NOT NULL DEFAULT nextval('public.seq_typequestion'),
                typequestion_libelle VARCHAR(128) NOT NULL,
                CONSTRAINT pk_typequestion PRIMARY KEY (typequestion_id)
);


ALTER SEQUENCE public.seq_typequestion OWNED BY public.typequestion.typequestion_id;

CREATE SEQUENCE public.seq_typequestionnaire;

CREATE TABLE public.typequestionnaire (
                typequestionnaire_id INTEGER NOT NULL DEFAULT nextval('public.seq_typequestionnaire'),
                typequestionnaire_libelle VARCHAR(128) NOT NULL,
                CONSTRAINT pk_typequestionnaire PRIMARY KEY (typequestionnaire_id)
);


ALTER SEQUENCE public.seq_typequestionnaire OWNED BY public.typequestionnaire.typequestionnaire_id;

CREATE SEQUENCE public.seq_profil;

CREATE TABLE public.profil (
                profil_id INTEGER NOT NULL DEFAULT nextval('public.seq_profil'),
                profil_libelle VARCHAR(128) NOT NULL,
                profil_extension_id INTEGER,
                CONSTRAINT pk_profil PRIMARY KEY (profil_id)
);


ALTER SEQUENCE public.seq_profil OWNED BY public.profil.profil_id;

CREATE TABLE public.menuprofil (
                menu_id INTEGER NOT NULL,
                profil_id INTEGER NOT NULL,
                CONSTRAINT pk_menuprofil PRIMARY KEY (menu_id, profil_id)
);


CREATE SEQUENCE public.seq_personne;

CREATE TABLE public.personne (
                personne_id INTEGER NOT NULL DEFAULT nextval('public.seq_personne'),
                personne_nom VARCHAR(128) NOT NULL,
                personne_prenom VARCHAR(128) NOT NULL,
                personne_uid VARCHAR(128),
                personne_password VARCHAR(264),
                personne_email VARCHAR(256),
                personne_numeroetudiant VARCHAR(16),
                personne_valide BOOLEAN DEFAULT TRUE NOT NULL,
                profil_id INTEGER,
                CONSTRAINT pk_personne PRIMARY KEY (personne_id)
);


ALTER SEQUENCE public.seq_personne OWNED BY public.personne.personne_id;

CREATE SEQUENCE public.seq_questionnaire;

CREATE TABLE public.questionnaire (
                questionnaire_id INTEGER NOT NULL DEFAULT nextval('public.seq_questionnaire'),
                questionnaire_libelle VARCHAR(128) NOT NULL,
                questionnaire_duree TIME,
                typequestionnaire_id INTEGER NOT NULL,
                questionnaire_description TEXT NOT NULL,
                questionnaire_valide BOOLEAN DEFAULT FALSE NOT NULL,
                questionnaire_texte TEXT,
                personne_id INTEGER NOT NULL,
                typeacces_id INTEGER NOT NULL,
                CONSTRAINT pk_questionnaire PRIMARY KEY (questionnaire_id)
);


ALTER SEQUENCE public.seq_questionnaire OWNED BY public.questionnaire.questionnaire_id;

CREATE SEQUENCE public.seq_evaluation;

CREATE TABLE public.evaluation (
                evaluation_id INTEGER NOT NULL DEFAULT nextval('public.seq_evaluation'),
                questionnaire_id INTEGER NOT NULL,
                Evaluation_date DATE,
                evaluation_depart TIME,
                evaluation_duree TIME,
                evaluation_closed BOOLEAN DEFAULT FALSE NOT NULL,
                modeevaluation_id INTEGER NOT NULL,
                personne_id INTEGER NOT NULL,
                CONSTRAINT pk_evaluation PRIMARY KEY (evaluation_id)
);


ALTER SEQUENCE public.seq_evaluation OWNED BY public.evaluation.evaluation_id;

CREATE TABLE public.evaluationgroupe (
                groupe_id INTEGER NOT NULL,
                evaluation_id INTEGER NOT NULL,
                CONSTRAINT pk_evaluationgroupe PRIMARY KEY (groupe_id, evaluation_id)
);


CREATE SEQUENCE public.seq_question;

CREATE TABLE public.question (
                question_id INTEGER NOT NULL DEFAULT nextval('public.seq_question'),
                typequestion_id INTEGER NOT NULL,
                question_texte TEXT,
                image_id INTEGER,
                question_valide BOOLEAN DEFAULT TRUE NOT NULL,
                question_solutiontexte TEXT,
                question_solutionentier INTEGER,
                question_solutionreel DOUBLE PRECISION,
                personne_id INTEGER NOT NULL,
                typeacces_id INTEGER NOT NULL,
                CONSTRAINT pk_question PRIMARY KEY (question_id)
);


ALTER SEQUENCE public.seq_question OWNED BY public.question.question_id;

CREATE TABLE public.questionmotcle (
                motcle_id INTEGER NOT NULL,
                question_id INTEGER NOT NULL,
                CONSTRAINT pk_questionmotcle PRIMARY KEY (motcle_id, question_id)
);


CREATE TABLE public.comportequestion (
                questionnaire_id INTEGER NOT NULL,
                question_id INTEGER NOT NULL,
                CONSTRAINT pk_comportequestion PRIMARY KEY (questionnaire_id, question_id)
);


CREATE SEQUENCE public.seq_reponsepossible;

CREATE TABLE public.reponsepossible (
                reponsepossible_id INTEGER NOT NULL DEFAULT nextval('public.seq_reponsepossible'),
                question_id INTEGER NOT NULL,
                reponsepossible_texte TEXT,
                image_id INTEGER,
                reponsepossible_estsolution BOOLEAN DEFAULT FALSE NOT NULL,
                CONSTRAINT pk_reponsepossible PRIMARY KEY (reponsepossible_id)
);


ALTER SEQUENCE public.seq_reponsepossible OWNED BY public.reponsepossible.reponsepossible_id;

CREATE TABLE public.connexion (
                connexion_code VARCHAR(32) NOT NULL,
                personne_id INTEGER NOT NULL,
                connexion_expire TIMESTAMP NOT NULL,
                CONSTRAINT pk_connexion PRIMARY KEY (connexion_code)
);


CREATE TABLE public.evaluationpersonne (
                personne_id INTEGER NOT NULL,
                evaluation_id INTEGER NOT NULL,
                evaluationpersonne_code VARCHAR(32),
                CONSTRAINT pk_evaluationpersonne PRIMARY KEY (personne_id, evaluation_id)
);


CREATE SEQUENCE public.seq_reponsequestionnaire;

CREATE TABLE public.reponsequestionnaire (
                ReponseQuestionnaire_ID INTEGER NOT NULL DEFAULT nextval('public.seq_reponsequestionnaire'),
                personne_id INTEGER NOT NULL,
                evaluation_id INTEGER NOT NULL,
                reponsequestionnaire_depart TIMESTAMP NOT NULL,
                reponsequestionnaire_fin TIMESTAMP,
                CONSTRAINT pk_reponsequestionnaire PRIMARY KEY (ReponseQuestionnaire_ID)
);


ALTER SEQUENCE public.seq_reponsequestionnaire OWNED BY public.reponsequestionnaire.ReponseQuestionnaire_ID;

CREATE TABLE public.reponsequestion (
                reponsequestionnaire_id INTEGER NOT NULL,
                question_id INTEGER NOT NULL,
                reponsequestion_ordre INTEGER NOT NULL,
                reponsequestion_texte TEXT,
                ReponseQuestion_Entier INTEGER,
                ReponseQuestion_Reel DOUBLE PRECISION,
                reponsequestion_score DOUBLE PRECISION NOT NULL,
                CONSTRAINT pk_reponsequestion PRIMARY KEY (reponsequestionnaire_id, question_id)
);


CREATE TABLE public.reponsequestionchoix (
                ReponseQuestionnaire_ID INTEGER NOT NULL,
                question_id INTEGER NOT NULL,
                reponsepossible_id INTEGER NOT NULL,
                reponsequestionchoix_ordre INTEGER NOT NULL,
                reponsequestionchoix_select BOOLEAN DEFAULT FALSE NOT NULL,
                CONSTRAINT pk_reponsequestionchoix PRIMARY KEY (ReponseQuestionnaire_ID, question_id, reponsepossible_id)
);


CREATE TABLE public.appartient (
                personne_id INTEGER NOT NULL,
                groupe_id INTEGER NOT NULL,
                CONSTRAINT pk_appartient PRIMARY KEY (personne_id, groupe_id)
);


ALTER TABLE public.evaluation ADD CONSTRAINT modeevaluation_evaluation_fk
FOREIGN KEY (modeevaluation_id)
REFERENCES public.modeevaluation (modeevaluation_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.questionnaire ADD CONSTRAINT typeacces_questionnaire_fk
FOREIGN KEY (typeacces_id)
REFERENCES public.typeacces (typeacces_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.question ADD CONSTRAINT typeacces_question_fk
FOREIGN KEY (typeacces_id)
REFERENCES public.typeacces (typeacces_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.menu ADD CONSTRAINT menu_menu_fk
FOREIGN KEY (menu_pere_id)
REFERENCES public.menu (menu_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.menuprofil ADD CONSTRAINT menu_menuprofil_fk
FOREIGN KEY (menu_id)
REFERENCES public.menu (menu_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.image ADD CONSTRAINT typeimage_image_fk
FOREIGN KEY (typeimage_id)
REFERENCES public.typeimage (typeimage_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.question ADD CONSTRAINT image_question_fk
FOREIGN KEY (image_id)
REFERENCES public.image (image_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.reponsepossible ADD CONSTRAINT image_reponsepossible_fk
FOREIGN KEY (image_id)
REFERENCES public.image (image_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.questionmotcle ADD CONSTRAINT motcle_questionmotcle_fk
FOREIGN KEY (motcle_id)
REFERENCES public.motcle (motcle_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.appartient ADD CONSTRAINT groupe_appartient_fk
FOREIGN KEY (groupe_id)
REFERENCES public.groupe (groupe_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.evaluationgroupe ADD CONSTRAINT groupe_evaluationgroupe_fk
FOREIGN KEY (groupe_id)
REFERENCES public.groupe (groupe_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.question ADD CONSTRAINT typequestion_question_fk
FOREIGN KEY (typequestion_id)
REFERENCES public.typequestion (typequestion_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.questionnaire ADD CONSTRAINT typequestionnaire_questionnaire_fk
FOREIGN KEY (typequestionnaire_id)
REFERENCES public.typequestionnaire (typequestionnaire_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.menuprofil ADD CONSTRAINT profil_menuprofil_fk
FOREIGN KEY (profil_id)
REFERENCES public.profil (profil_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.profil ADD CONSTRAINT profil_profil_fk
FOREIGN KEY (profil_extension_id)
REFERENCES public.profil (profil_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.personne ADD CONSTRAINT profil_personne_fk
FOREIGN KEY (profil_id)
REFERENCES public.profil (profil_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.appartient ADD CONSTRAINT personne_appartient_fk
FOREIGN KEY (personne_id)
REFERENCES public.personne (personne_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.reponsequestionnaire ADD CONSTRAINT personne_reponsequestionnaire_fk
FOREIGN KEY (personne_id)
REFERENCES public.personne (personne_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.evaluationpersonne ADD CONSTRAINT personne_evaluationpresonne_fk
FOREIGN KEY (personne_id)
REFERENCES public.personne (personne_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.connexion ADD CONSTRAINT personne_connexion_fk
FOREIGN KEY (personne_id)
REFERENCES public.personne (personne_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.question ADD CONSTRAINT personne_question_fk
FOREIGN KEY (personne_id)
REFERENCES public.personne (personne_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.evaluation ADD CONSTRAINT personne_evaluation_fk
FOREIGN KEY (personne_id)
REFERENCES public.personne (personne_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.questionnaire ADD CONSTRAINT personne_questionnaire_fk
FOREIGN KEY (personne_id)
REFERENCES public.personne (personne_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.comportequestion ADD CONSTRAINT questionnaire_comportequestions_fk
FOREIGN KEY (questionnaire_id)
REFERENCES public.questionnaire (questionnaire_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.evaluation ADD CONSTRAINT questionnaire_evaluation_fk
FOREIGN KEY (questionnaire_id)
REFERENCES public.questionnaire (questionnaire_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.reponsequestionnaire ADD CONSTRAINT evaluation_reponsequestionnaire_fk
FOREIGN KEY (evaluation_id)
REFERENCES public.evaluation (evaluation_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.evaluationgroupe ADD CONSTRAINT evaluation_evaluationgroupe_fk
FOREIGN KEY (evaluation_id)
REFERENCES public.evaluation (evaluation_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.evaluationpersonne ADD CONSTRAINT evaluation_evaluationpresonne_fk
FOREIGN KEY (evaluation_id)
REFERENCES public.evaluation (evaluation_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.reponsepossible ADD CONSTRAINT question_reponsepossible_fk
FOREIGN KEY (question_id)
REFERENCES public.question (question_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.comportequestion ADD CONSTRAINT question_comportequestions_fk
FOREIGN KEY (question_id)
REFERENCES public.question (question_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.reponsequestion ADD CONSTRAINT question_reponsequestion_fk
FOREIGN KEY (question_id)
REFERENCES public.question (question_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.questionmotcle ADD CONSTRAINT question_questionmotcle_fk
FOREIGN KEY (question_id)
REFERENCES public.question (question_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.reponsequestionchoix ADD CONSTRAINT reponsepossible_reponsequestionselection_fk
FOREIGN KEY (reponsepossible_id)
REFERENCES public.reponsepossible (reponsepossible_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.reponsequestion ADD CONSTRAINT reponsequestionnaire_reponsequestion_fk
FOREIGN KEY (reponsequestionnaire_id)
REFERENCES public.reponsequestionnaire (ReponseQuestionnaire_ID)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.reponsequestionchoix ADD CONSTRAINT reponsequestion_reponsequestionselection_fk
FOREIGN KEY (ReponseQuestionnaire_ID, question_id)
REFERENCES public.reponsequestion (reponsequestionnaire_id, question_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
