/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.dao;

import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.centrale.saturne.items.ItemManager;
import org.centrale.saturne.items.Profil;

/**
 *
 * @author utilisateur
 */
public class ProfilDAOImpl implements ProfilDAO {

    public ProfilDAOImpl() {
    }

    @Override
    public Profil findById(int id) {
        return findById(new Integer(id));
    }

    @Override
    public Collection<Profil> getAll() {
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Profil.findAll", Profil.class);
        Collection theList = theQuery.getResultList();
        return theList;    }

    @Override
    public Profil findById(Integer key) {
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Profil.findByProfilId", Profil.class);
        theQuery.setParameter("profilId", key);
        Profil profil = (Profil) theQuery.getSingleResult();
        return profil;    }


}
