/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.dao;

import java.util.Collection;
import org.centrale.saturne.items.Question;

/**
 *
 * @author antoinehurard
 */
public interface QuestionDAO extends DAO <Question, Integer> {
    public Collection<Question> getAll();
    public Question findById(Integer id);
    public void remove(Question question);
}
