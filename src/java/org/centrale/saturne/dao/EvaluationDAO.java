/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.dao;

import java.util.Collection;
import org.centrale.saturne.items.Evaluation;

/**
 *
 * @author GUO
 */
public interface EvaluationDAO {
    public Collection<Evaluation> getAll();
    public void create(Evaluation eval);
    public Evaluation findById(Integer id);
    public Evaluation findById(int id);
    public void remove(Evaluation eval);
    public void update(Integer id, Evaluation evaluation);
    
}
