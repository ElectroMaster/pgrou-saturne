/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.dao;

import java.util.Collection;
import org.centrale.saturne.items.Motcle;

/**
 *
 * @author antoinehurard
 */
public interface MotcleDAO extends DAO <Motcle, Integer> {
    public Collection<Motcle> getAll();
    public Motcle findById(Integer id);
    public void remove(Motcle motcle);
    public void update(Integer id, Motcle aMotcle);
}
