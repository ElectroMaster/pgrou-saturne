/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.dao;

import java.util.Collection;
import org.centrale.saturne.items.Groupe;
import org.centrale.saturne.items.Personne;

/**
 *
 * @author thibault
 */
public interface GroupeDAO extends DAO <Groupe,Integer> {
    public Collection<Groupe> getAll();
    public void create(Groupe grp);
    public Groupe findById(Integer id);
    public Groupe findById(int id);
    public void remove(Groupe grp);
    public void addMembre(Integer id,Integer idprs);
    public void removeMembre(Integer grpid, Integer idprs);
    public Collection<Groupe> getBySearch(String mot);
}
