/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.dao;

import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import org.centrale.saturne.items.ItemManager;
import org.centrale.saturne.items.Motcle;

/**
 *
 * @author antoinehurard
 */
public class MotcleDAOImpl implements MotcleDAO{
    @Override
    public Collection<Motcle> getAll(){
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Motcle.findAll", Motcle.class);
        Collection<Motcle> theList = theQuery.getResultList();
        return theList;
    }
    
    @Override
    public Motcle findById(Integer id){
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Motcle.findByMotcleId",
                Motcle.class);
        theQuery.setParameter("motcleId", id);
        Motcle motcle = (Motcle) theQuery.getSingleResult();
        return motcle;
    }
    
    public Motcle findById(int id){
        return findById(new Integer(id));
    }
    
    @Override
    public void remove(Motcle motcle){
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.remove(motcle);
        em.flush();
        transaction.commit();
    }
    
    @Override
    public void update(Integer id, Motcle aMotcle){
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        Motcle motcle = this.findById(id);
        motcle.setMotcleLibelle(aMotcle.getMotcleLibelle());
        em.flush();
        transaction.commit();
        
    }
}
