/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.dao;

import java.util.Collection;
import org.centrale.saturne.items.Typequestion;

/**
 *
 * @author antoinehurard
 */
public interface TypequestionDAO extends DAO <Typequestion, Integer>{
    public Collection<Typequestion> getAll();
}
