/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.dao;

import java.util.Collection;
import org.centrale.saturne.items.Modeevaluation;

/**
 *
 * @author GUO
 */
public interface ModeevaluationDAO extends DAO<Modeevaluation, Integer> {
    public Collection<Modeevaluation> getAll();
}
