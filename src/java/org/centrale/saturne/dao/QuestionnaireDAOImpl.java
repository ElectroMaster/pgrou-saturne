/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.dao;

import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import org.centrale.saturne.items.ItemManager;
import org.centrale.saturne.items.Questionnaire;

/**
 *
 * @author exia
 */
public class QuestionnaireDAOImpl implements QuestionnaireDAO{
    @Override
    public Collection<Questionnaire> getAll(){
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Questionnaire.findAll", Questionnaire.class);
        Collection theList = theQuery.getResultList();
        return theList;
    }
    
    @Override
    public Questionnaire findById(Integer id){
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Questionnaire.findByQuestionnaireId",
                Questionnaire.class);
        theQuery.setParameter("questionnaireId", id);
        Questionnaire questionnaire = (Questionnaire) theQuery.getSingleResult();
        return questionnaire;
    }
    
    public Questionnaire findById(int id){
        return findById(new Integer(id));
    }
    
    @Override
    public void remove(Questionnaire questionnaire){
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.remove(questionnaire);
        em.flush();
        transaction.commit();
    }
    
    @Override
    public void update(Integer id, Questionnaire aQuestionnaire){
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        Questionnaire questionnaire = this.findById(id);
        questionnaire.setQuestionnaireDuree(aQuestionnaire.getQuestionnaireDuree());
        questionnaire.setQuestionnaireLibelle(aQuestionnaire.getQuestionnaireLibelle());
        questionnaire.setTypeaccesId(aQuestionnaire.getTypeaccesId());
        questionnaire.setTypequestionnaireId(aQuestionnaire.getTypequestionnaireId());
        questionnaire.setQuestionnaireDescription(aQuestionnaire.getQuestionnaireDescription());
//        questionnaire.setPersonneId(aQuestionnaire.getPersonneId());
        em.flush();
        transaction.commit();
        
    }
}
