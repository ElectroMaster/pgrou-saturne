/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.dao;

import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.centrale.saturne.items.ItemManager;
import org.centrale.saturne.items.Typequestionnaire;

/**
 *
 * @author exia
 */
public class TypequestionnaireDAOImpl implements TypequestionnaireDAO{
    @Override
    public Collection<Typequestionnaire> getAll(){
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Typequestionnaire.findAll",Typequestionnaire.class);
        Collection<Typequestionnaire> theList = theQuery.getResultList();
        return theList;
    }
}
