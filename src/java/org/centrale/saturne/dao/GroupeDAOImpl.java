/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.dao;

import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import org.centrale.saturne.items.Groupe;
import org.centrale.saturne.items.ItemManager;
import org.centrale.saturne.items.Personne;

/**
 *
 * @author thibault
 */
public class GroupeDAOImpl implements GroupeDAO {

    @Override
    public Collection<Groupe> getAll() {
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Groupe.findAll", Groupe.class);
        Collection theList = theQuery.getResultList();
        return theList;
    }

    @Override
    public void create(Groupe grp) {
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.persist(grp);
        em.flush();
        transaction.commit();
    }

    @Override
    public Groupe findById(Integer id) {
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Groupe.findByGroupeId", Groupe.class);
        theQuery.setParameter("groupeId",id);
        Groupe grp = (Groupe) theQuery.getSingleResult();
        
        return grp;
    }
    
    @Override
    public Groupe findById(int id){
        return findById(new Integer(id));
    }
    
    @Override
    public void remove(Groupe grp){
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.remove(grp);
        em.flush();
        transaction.commit();
    }

    @Override
    public void addMembre(Integer id, Integer idprs) {
        String idStr = Integer.toString(id);
        String idprsStr = Integer.toString(idprs);
        
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        Query query = em.createNativeQuery("INSERT INTO appartient (personne_id,groupe_id) "
                + "VALUES (" +idStr+ "," +idprsStr+ ")");
        transaction.commit();
    }

    @Override
    public void removeMembre(Integer grpid, Integer idprs) {
        String idgrpStr = Integer.toString(grpid);
        String idprsStr = Integer.toString(idprs);
        
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        Query query = em.createNativeQuery("DELETE FROM appartient "
                + "WHERE (groupe_id=? AND personne_id=? )");
        query.setParameter(1, idgrpStr);
        query.setParameter(2, idprsStr);
        transaction.commit();
    }

    @Override
    public Collection<Groupe> getBySearch(String mot) {
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Groupe.findAll", Groupe.class);
        Collection<Groupe> theList = theQuery.getResultList();
        ArrayList<Groupe> finalList = new ArrayList<>();
        for( Groupe grp : theList){
            if(grp.getGroupeLibelle().toLowerCase().contains(mot.toLowerCase())){
                finalList.add(grp);
            }
        }
        return finalList;
    }

}
