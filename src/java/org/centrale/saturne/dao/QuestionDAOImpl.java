/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.dao;

import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import org.centrale.saturne.items.ItemManager;
import org.centrale.saturne.items.Question;

/**
 *
 * @author antoinehurard
 */
public class QuestionDAOImpl implements QuestionDAO{
    @Override
    public Collection<Question> getAll(){
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Question.findAll", Question.class);
        Collection<Question> theList = theQuery.getResultList();
        return theList;
    }
    
    @Override
    public Question findById(Integer id){
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Question.findByQuestionId",
                Question.class);
        theQuery.setParameter("questionId", id);
        Question question = (Question) theQuery.getSingleResult();
        return question;
    }
    
    public Question findById(int id){
        return findById(new Integer(id));
    }
    
    @Override
    public void remove(Question question){
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.remove(question);
        em.flush();
        transaction.commit();
    }
}
