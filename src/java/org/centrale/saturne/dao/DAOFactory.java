
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.dao;

/**
 *
 * @author exia
 */
public abstract class DAOFactory {
    public static QuestionnaireDAO getQuestionnaireDAO(){
        return new QuestionnaireDAOImpl();
    }
    public static TypeaccesDAO getTypeaccesDAO(){
        return new TypeaccesDAOImpl();
    }
    
    public static TypequestionnaireDAO getTypequestionnaireDAO(){
        return new TypequestionnaireDAOImpl();
    }
    
    public static GroupeDAO getGroupeDAO(){
        return new GroupeDAOImpl();
    }
    
    public static PersonneDAO getPersonneDAO(){
        return new PersonneDAOImpl();
    }
    
    public static TypequestionDAO getTypequestionDAO(){
        return new TypequestionDAOImpl();
    }
    
    public static QuestionDAO getQuestionDAO(){
        return new QuestionDAOImpl();
    }
    
     public static ModeevaluationDAO getModeevaluationDAO(){
        return new ModeevaluationDAOImpl();
    }
     
    public static EvaluationDAO getEvaluationDAO(){
        return new EvaluationDAOImpl();
    }

    public static ProfilDAO getProfilDAO() {
        return new ProfilDAOImpl();
    }
    
    public static MotcleDAO getMotcleDAO(){
        return new MotcleDAOImpl();

    }
}

