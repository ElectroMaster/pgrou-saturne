
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.centrale.saturne.items.ItemManager;
import org.centrale.saturne.items.Personne;
import org.centrale.saturne.items.Profil;

/**
 *
 * @author Dimitri
 */
public class PersonneDAOImpl implements PersonneDAO {

    @Override
    public Personne findById(int id) {
        return findById(new Integer(id));
    }

    @Override
    public Collection<Personne> getAll() {
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Personne.findAll", Personne.class);
        Collection theList = theQuery.getResultList();
        return theList;
    }

    @Override
    public Personne findById(Integer key) {
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Personne.findByPersonneId", Personne.class);
        theQuery.setParameter("personneId", key);
        Personne personne = (Personne) theQuery.getSingleResult();
        return personne;
    }

    @Override
    public void remove(Personne item) {
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.remove(item);
        em.flush();
        transaction.commit();
    }

    @Override
    public void create(Personne item) {
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.persist(item);
        em.flush();
        transaction.commit();
    }

    @Override
    public Personne create(String Nom, String Prenom, String email, String Login, String password, Profil profil) {
        Personne personne = new Personne();
        personne.setPersonnePrenom(Prenom);
        personne.setPersonneNom(Nom);
        personne.setPersonneEmail(email);
        personne.setPersonneUid(Login);
        personne.setPersonneEncodedPassword(password);
        personne.setProfilId(profil);
        PersonneDAO personneDAO = DAOFactory.getPersonneDAO();
        personneDAO.create(personne);
        return personne;
    }

    @Override
    public Personne findByUid(String login) {
        Personne personne = null;

        try {
            EntityManager em = ItemManager.getEntityManager();
            Query theQuery = em.createNamedQuery("Personne.findByPersonneUid", Personne.class);
            theQuery.setParameter("personneUid", login);
            personne = (Personne) theQuery.getSingleResult();
        } catch (NoResultException ex) {
            Logger.getLogger(PersonneDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return personne;

    }

    @Override
    public Collection<Personne> getBySearch(String mot) {
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Personne.findAll", Personne.class);
        Collection<Personne> theList = theQuery.getResultList();
        ArrayList<Personne> finalList = new ArrayList<>();
        for (Personne prs : theList) {
            if (prs.getPersonneNom().toLowerCase().contains(mot.toLowerCase())
                    || prs.getPersonnePrenom().toLowerCase().contains(mot.toLowerCase())) {
                finalList.add(prs);
            }
        }
        return finalList;
    }

    @Override
    public void update(Integer id, Personne aPersonne) {
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        Personne personne = this.findById(id);
        personne.setPersonneNom(aPersonne.getPersonneNom());
        personne.setPersonnePrenom(aPersonne.getPersonnePrenom());
        personne.setPersonneEmail(aPersonne.getPersonneEmail());
        personne.setPersonneUid(aPersonne.getPersonneUid());
        personne.setProfilId(aPersonne.getProfilId());
        em.flush();
        transaction.commit();

    }
}
