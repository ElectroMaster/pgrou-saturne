/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.dao;

import java.util.Collection;
import org.centrale.saturne.items.Questionnaire;

/**
 *
 * @author exia
 */
public interface QuestionnaireDAO extends DAO <Questionnaire, Integer> {
    public Collection<Questionnaire> getAll();
    public Questionnaire findById(Integer id);
    public void remove(Questionnaire questionnaire);
    public void update(Integer id, Questionnaire aQuestionnaire);
    
}
