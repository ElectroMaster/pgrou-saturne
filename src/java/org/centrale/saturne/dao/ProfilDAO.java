/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.dao;

import java.util.Collection;
import org.centrale.saturne.items.Personne;
import org.centrale.saturne.items.Profil;

/**
 *
 * @author utilisateur
 */
public interface ProfilDAO extends DAO <Profil,Integer>{
    public Profil findById( int id);
    
    public Collection<Profil> getAll();

    public Profil findById(Integer key);

}
