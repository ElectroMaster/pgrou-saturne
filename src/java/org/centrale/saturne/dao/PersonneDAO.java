
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.dao;

import java.util.Collection;
import org.centrale.saturne.items.Personne;
import org.centrale.saturne.items.Profil;

/**
 *
 * @author Dimitri
 */
public interface PersonneDAO extends DAO<Personne, Integer> {

    public Personne findById(int id);
    
    public Personne findByUid(String login);

    public Personne create(String Nom, String Prenom, String email, String Login, String password, Profil profil);

    public Collection<Personne> getAll();

    public Personne findById(Integer key);

    public void remove(Personne item);

    public void create(Personne item);
    
    public void update(Integer id, Personne item);
    
    public Collection<Personne> getBySearch(String mot);
}

