/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.dao;

import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import org.centrale.saturne.items.Evaluation;
import org.centrale.saturne.items.ItemManager;

/**
 *
 * @author GUO
 */
public class EvaluationDAOImpl implements EvaluationDAO {
    
    @Override
    public Collection<Evaluation> getAll() {
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Evaluation.findAll", Evaluation.class);
        Collection theList = theQuery.getResultList();
        return theList;
    }

    @Override
    public void create(Evaluation eval) {
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.persist(eval);
        em.flush();
        transaction.commit();
    }

    @Override
    public Evaluation findById(Integer id) {
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Evaluation.findByEvaluationId", Evaluation.class);
        theQuery.setParameter("evaluationId",id);
        Evaluation eval = (Evaluation) theQuery.getSingleResult();
        
        return eval;
    }
    
    @Override
    public Evaluation findById(int id){
        return findById(new Integer(id));
    }
    
    @Override
    public void remove(Evaluation eval){
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.remove(eval);
        em.flush();
        transaction.commit();
    }
    
     @Override
    public void update(Integer id, Evaluation evaluation){
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        Evaluation evaluaiton = this.findById(id);
        evaluaiton.setQuestionnaireId(evaluation.getQuestionnaireId());
        evaluation.setEvaluationDate(evaluation.getEvaluationDate());
        evaluaiton.setEvaluationDepart(evaluation.getEvaluationDepart());
        evaluation.setEvaluationDuree(evaluation.getEvaluationDuree());
        evaluation.setModeevaluationId(evaluation.getModeevaluationId());
        em.flush();
        transaction.commit();
        
    }
    
}
