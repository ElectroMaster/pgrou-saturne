/*
 * Projet Saturne
 * 
 * Gestion de QCM en ligne
 *
 * Ecole Centrale Nantes - Fevrier 2019
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;

/**
 *
 * @author antoinehurard
 */
public abstract class Item implements Serializable,Comparable {

    private static final long serialVersionUID = 1L;

    private EntityManager em;

    public Item() {
        this.em = ItemManager.getEntityManager();
    }
    
    /**
     * Copy constructor
     *
     * @param item
     */
    public Item(Item item) {
        this.em = item.em;
    }

    /**
     * Get item integer ID
     *
     * @return null by default
     */
    public Integer getId() {
        return null;
    }

    /**
     * Get any kind of object
     *
     * @param theClass
     * @param id
     * @return
     */
    public static final Serializable getItemById(Class theClass, int id) {
        return ItemManager.getFromId(theClass, id);
    }

    /**
     * Get any kind of object
     *
     * @param theClass
     * @param id
     * @return
     */
    public static final Serializable getItemById(String theClass, int id) {
        try {
            Class realClass = Class.forName(theClass);
            return getItemById(realClass, id);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Item.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Get any kind of object
     *
     * @param owner
     * @param theClass1
     * @param id1
     * @param theClass2
     * @param id2
     * @return
     */
    public static final Serializable getItemByKey(Class owner, Class theClass1, int id1, Class theClass2, int id2) {
        HashMap<Class, Integer> elements = new HashMap<Class, Integer>();
        elements.put(theClass1, id1);
        elements.put(theClass2, id2);
        return ItemManager.getUniqueFromKey(owner, elements);
    }

    /**
     * Get any kind of object
     *
     * @param owner
     * @param theClass1
     * @param id1
     * @param theClass2
     * @param id2
     * @param theClass3
     * @param id3
     * @return
     */
    public static final Serializable getItemByKey(Class owner, Class theClass1, int id1,
                                                    Class theClass2, int id2,
                                                    Class theClass3, int id3) {
        HashMap<Class, Integer> elements = new HashMap<Class, Integer>();
        elements.put(theClass1, id1);
        elements.put(theClass2, id2);
        elements.put(theClass3, id3);
        return ItemManager.getUniqueFromKey(owner, elements);
    }

    /**
     * Get manager
     *
     * @return
     */
    public static EntityManager getManager() {
        return ItemManager.getEntityManager();
    }

    /**
     * Shuffle item collection
     *
     * @param liste
     */
    public static void shuffle(Collection liste) {
        ArrayList temp = new ArrayList();
        for (Object i : liste) {
            temp.add(i);
        }
        int size = liste.size();
        for (int i = 0; i < size * 2; i++) {
            // Get 2 random values
            int i1 = (int) Math.ceil(Math.random() * size);
            int i2 = (int) Math.ceil(Math.random() * size);
            // switch values
            Object it = temp.get(i1);
            temp.set(i1, temp.get(i2));
            temp.set(i2, it);
        }

        liste.clear();
        for (Object i : temp) {
            liste.add(i);
        }
    }

    /**
     * Item comparizon
     * @param o
     * @return 
     */
    public int compareTo(Object o) {
        if (o == null) {
            return -1;
        } else if (o instanceof Item) {
            if (this.getClass().getName().equals(o.getClass().getName())) {
                return (this.getId().compareTo(((Item) o).getId()));
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }
}
