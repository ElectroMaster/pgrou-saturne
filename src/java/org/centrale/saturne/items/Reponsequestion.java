/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "reponsequestion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reponsequestion.findAll", query = "SELECT r FROM Reponsequestion r")
    , @NamedQuery(name = "Reponsequestion.findByReponsequestionnaireId", query = "SELECT r FROM Reponsequestion r WHERE r.reponsequestionPK.reponsequestionnaireId = :reponsequestionnaireId")
    , @NamedQuery(name = "Reponsequestion.findByQuestionId", query = "SELECT r FROM Reponsequestion r WHERE r.reponsequestionPK.questionId = :questionId")
    , @NamedQuery(name = "Reponsequestion.findByReponsequestionOrdre", query = "SELECT r FROM Reponsequestion r WHERE r.reponsequestionOrdre = :reponsequestionOrdre")
    , @NamedQuery(name = "Reponsequestion.findByReponsequestionTexte", query = "SELECT r FROM Reponsequestion r WHERE r.reponsequestionTexte = :reponsequestionTexte")
    , @NamedQuery(name = "Reponsequestion.findByReponsequestionEntier", query = "SELECT r FROM Reponsequestion r WHERE r.reponsequestionEntier = :reponsequestionEntier")
    , @NamedQuery(name = "Reponsequestion.findByReponsequestionReel", query = "SELECT r FROM Reponsequestion r WHERE r.reponsequestionReel = :reponsequestionReel")
    , @NamedQuery(name = "Reponsequestion.findByReponsequestionScore", query = "SELECT r FROM Reponsequestion r WHERE r.reponsequestionScore = :reponsequestionScore")})
public class Reponsequestion implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ReponsequestionPK reponsequestionPK;
    @Basic(optional = false)
    @Column(name = "reponsequestion_ordre")
    private int reponsequestionOrdre;
    @Column(name = "reponsequestion_texte")
    private String reponsequestionTexte;
    @Column(name = "reponsequestion_entier")
    private Integer reponsequestionEntier;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "reponsequestion_reel")
    private Double reponsequestionReel;
    @Basic(optional = false)
    @Column(name = "reponsequestion_score")
    private double reponsequestionScore;
    @JoinColumn(name = "question_id", referencedColumnName = "question_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Question question;
    @JoinColumn(name = "reponsequestionnaire_id", referencedColumnName = "reponsequestionnaire_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Reponsequestionnaire reponsequestionnaire;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reponsequestion")
    private Collection<Reponsequestionchoix> reponsequestionchoixCollection;

    public Reponsequestion() {
    }

    public Reponsequestion(ReponsequestionPK reponsequestionPK) {
        this.reponsequestionPK = reponsequestionPK;
    }

    public Reponsequestion(ReponsequestionPK reponsequestionPK, int reponsequestionOrdre, double reponsequestionScore) {
        this.reponsequestionPK = reponsequestionPK;
        this.reponsequestionOrdre = reponsequestionOrdre;
        this.reponsequestionScore = reponsequestionScore;
    }

    public Reponsequestion(int reponsequestionnaireId, int questionId) {
        this.reponsequestionPK = new ReponsequestionPK(reponsequestionnaireId, questionId);
    }

    public ReponsequestionPK getReponsequestionPK() {
        return reponsequestionPK;
    }

    public void setReponsequestionPK(ReponsequestionPK reponsequestionPK) {
        this.reponsequestionPK = reponsequestionPK;
    }

    public int getReponsequestionOrdre() {
        return reponsequestionOrdre;
    }

    public void setReponsequestionOrdre(int reponsequestionOrdre) {
        this.reponsequestionOrdre = reponsequestionOrdre;
    }

    public String getReponsequestionTexte() {
        return reponsequestionTexte;
    }

    public void setReponsequestionTexte(String reponsequestionTexte) {
        this.reponsequestionTexte = reponsequestionTexte;
    }

    public Integer getReponsequestionEntier() {
        return reponsequestionEntier;
    }

    public void setReponsequestionEntier(Integer reponsequestionEntier) {
        this.reponsequestionEntier = reponsequestionEntier;
    }

    public Double getReponsequestionReel() {
        return reponsequestionReel;
    }

    public void setReponsequestionReel(Double reponsequestionReel) {
        this.reponsequestionReel = reponsequestionReel;
    }

    public double getReponsequestionScore() {
        return reponsequestionScore;
    }

    public void setReponsequestionScore(double reponsequestionScore) {
        this.reponsequestionScore = reponsequestionScore;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Reponsequestionnaire getReponsequestionnaire() {
        return reponsequestionnaire;
    }

    public void setReponsequestionnaire(Reponsequestionnaire reponsequestionnaire) {
        this.reponsequestionnaire = reponsequestionnaire;
    }

    @XmlTransient
    public Collection<Reponsequestionchoix> getReponsequestionchoixCollection() {
        return reponsequestionchoixCollection;
    }

    public void setReponsequestionchoixCollection(Collection<Reponsequestionchoix> reponsequestionchoixCollection) {
        this.reponsequestionchoixCollection = reponsequestionchoixCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reponsequestionPK != null ? reponsequestionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reponsequestion)) {
            return false;
        }
        Reponsequestion other = (Reponsequestion) object;
        if ((this.reponsequestionPK == null && other.reponsequestionPK != null) || (this.reponsequestionPK != null && !this.reponsequestionPK.equals(other.reponsequestionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Reponsequestion[ reponsequestionPK=" + reponsequestionPK + " ]";
    }
    
}
