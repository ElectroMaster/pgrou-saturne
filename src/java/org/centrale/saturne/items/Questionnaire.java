/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "questionnaire")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Questionnaire.findAll", query = "SELECT q FROM Questionnaire q")
    , @NamedQuery(name = "Questionnaire.findByQuestionnaireId", query = "SELECT q FROM Questionnaire q WHERE q.questionnaireId = :questionnaireId")
    , @NamedQuery(name = "Questionnaire.findByQuestionnaireLibelle", query = "SELECT q FROM Questionnaire q WHERE q.questionnaireLibelle = :questionnaireLibelle")
    , @NamedQuery(name = "Questionnaire.findByQuestionnaireDuree", query = "SELECT q FROM Questionnaire q WHERE q.questionnaireDuree = :questionnaireDuree")
    , @NamedQuery(name = "Questionnaire.findByQuestionnaireDescription", query = "SELECT q FROM Questionnaire q WHERE q.questionnaireDescription = :questionnaireDescription")
    , @NamedQuery(name = "Questionnaire.findByQuestionnaireValide", query = "SELECT q FROM Questionnaire q WHERE q.questionnaireValide = :questionnaireValide")
    , @NamedQuery(name = "Questionnaire.findByQuestionnaireTexte", query = "SELECT q FROM Questionnaire q WHERE q.questionnaireTexte = :questionnaireTexte")})
public class Questionnaire extends Item implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "questionnaire_id")
    private Integer questionnaireId;
    @Basic(optional = false)
    @Column(name = "questionnaire_libelle")
    private String questionnaireLibelle;
    @Column(name = "questionnaire_duree")
    @Temporal(TemporalType.TIME)
    private Date questionnaireDuree;
    @Basic(optional = false)
    @Column(name = "questionnaire_description")
    private String questionnaireDescription;
    @Basic(optional = false)
    @Column(name = "questionnaire_valide")
    private boolean questionnaireValide;
    @Column(name = "questionnaire_texte")
    private String questionnaireTexte;
    @ManyToMany(mappedBy = "questionnaireCollection")
    private Collection<Question> questionCollection;
    @JoinColumn(name = "personne_id", referencedColumnName = "personne_id")
    @ManyToOne(optional = false)
    private Personne personneId;
    @JoinColumn(name = "typeacces_id", referencedColumnName = "typeacces_id")
    @ManyToOne(optional = false)
    private Typeacces typeaccesId;
    @JoinColumn(name = "typequestionnaire_id", referencedColumnName = "typequestionnaire_id")
    @ManyToOne(optional = false)
    private Typequestionnaire typequestionnaireId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "questionnaireId")
    private Collection<Evaluation> evaluationCollection;

    public Questionnaire() {
    }

    public Questionnaire(Integer questionnaireId) {
        this.questionnaireId = questionnaireId;
    }

    public Questionnaire(Integer questionnaireId, String questionnaireLibelle, String questionnaireDescription, boolean questionnaireValide) {
        this.questionnaireId = questionnaireId;
        this.questionnaireLibelle = questionnaireLibelle;
        this.questionnaireDescription = questionnaireDescription;
        this.questionnaireValide = questionnaireValide;
    }

    public Integer getQuestionnaireId() {
        return questionnaireId;
    }

    public void setQuestionnaireId(Integer questionnaireId) {
        this.questionnaireId = questionnaireId;
    }

    public String getQuestionnaireLibelle() {
        return questionnaireLibelle;
    }

    public void setQuestionnaireLibelle(String questionnaireLibelle) {
        this.questionnaireLibelle = questionnaireLibelle;
    }

    public Date getQuestionnaireDuree() {
        return questionnaireDuree;
    }

    public void setQuestionnaireDuree(Date questionnaireDuree) {
        this.questionnaireDuree = questionnaireDuree;
    }

    public String getQuestionnaireDescription() {
        return questionnaireDescription;
    }

    public void setQuestionnaireDescription(String questionnaireDescription) {
        this.questionnaireDescription = questionnaireDescription;
    }

    public boolean getQuestionnaireValide() {
        return questionnaireValide;
    }

    public void setQuestionnaireValide(boolean questionnaireValide) {
        this.questionnaireValide = questionnaireValide;
    }

    public String getQuestionnaireTexte() {
        return questionnaireTexte;
    }

    public void setQuestionnaireTexte(String questionnaireTexte) {
        this.questionnaireTexte = questionnaireTexte;
    }

    @XmlTransient
    public Collection<Question> getQuestionCollection() {
        return questionCollection;
    }

    public void setQuestionCollection(Collection<Question> questionCollection) {
        this.questionCollection = questionCollection;
    }

    public Personne getPersonneId() {
        return personneId;
    }

    public void setPersonneId(Personne personneId) {
        this.personneId = personneId;
    }

    public Typeacces getTypeaccesId() {
        return typeaccesId;
    }

    public void setTypeaccesId(Typeacces typeaccesId) {
        this.typeaccesId = typeaccesId;
    }

    public Typequestionnaire getTypequestionnaireId() {
        return typequestionnaireId;
    }

    public void setTypequestionnaireId(Typequestionnaire typequestionnaireId) {
        this.typequestionnaireId = typequestionnaireId;
    }

    @XmlTransient
    public Collection<Evaluation> getEvaluationCollection() {
        return evaluationCollection;
    }

    public void setEvaluationCollection(Collection<Evaluation> evaluationCollection) {
        this.evaluationCollection = evaluationCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (questionnaireId != null ? questionnaireId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Questionnaire)) {
            return false;
        }
        Questionnaire other = (Questionnaire) object;
        if ((this.questionnaireId == null && other.questionnaireId != null) || (this.questionnaireId != null && !this.questionnaireId.equals(other.questionnaireId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Questionnaire[ questionnaireId=" + questionnaireId + " ]";
    }
    
    public static Questionnaire getItemById(int id) {
        return (Questionnaire) (getItemById(Questionnaire.class, id));
    }
}
