/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "profil")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Profil.findAll", query = "SELECT p FROM Profil p")
    , @NamedQuery(name = "Profil.findByProfilId", query = "SELECT p FROM Profil p WHERE p.profilId = :profilId")
    , @NamedQuery(name = "Profil.findByProfilLibelle", query = "SELECT p FROM Profil p WHERE p.profilLibelle = :profilLibelle")})
public class Profil extends Item implements Serializable {

    private static final long serialVersionUID = 1L;
    //role list
    public static final int ADMINISTRATEUR = 1;
    public static final int DIRECTION = 2;
    public static final int ENSEIGNANT = 3;
    public static final int ELEVE = 4;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "profil_id")
    private Integer profilId;
    @Basic(optional = false)
    @Column(name = "profil_libelle")
    private String profilLibelle;
    @ManyToMany(mappedBy = "profilCollection")
    private Collection<Menu> menuCollection;
    @OneToMany(mappedBy = "profilExtensionId")
    private Collection<Profil> profilCollection;
    @JoinColumn(name = "profil_extension_id", referencedColumnName = "profil_id")
    @ManyToOne
    private Profil profilExtensionId;
    @OneToMany(mappedBy = "profilId")
    private Collection<Personne> personneCollection;

    public Profil() {
    }

    public Profil(Integer profilId) {
        this.profilId = profilId;
    }

    public Profil(Integer profilId, String profilLibelle) {
        this.profilId = profilId;
        this.profilLibelle = profilLibelle;
    }

    public Integer getProfilId() {
        return profilId;
    }

    public void setProfilId(Integer profilId) {
        this.profilId = profilId;
    }

    public String getProfilLibelle() {
        return profilLibelle;
    }

    public void setProfilLibelle(String profilLibelle) {
        this.profilLibelle = profilLibelle;
    }

    @XmlTransient
    public Collection<Menu> getMenuCollection() {
        return menuCollection;
    }

    public void setMenuCollection(Collection<Menu> menuCollection) {
        this.menuCollection = menuCollection;
    }

    @XmlTransient
    public Collection<Profil> getProfilCollection() {
        return profilCollection;
    }

    public void setProfilCollection(Collection<Profil> profilCollection) {
        this.profilCollection = profilCollection;
    }

    public Profil getProfilExtensionId() {
        return profilExtensionId;
    }

    public void setProfilExtensionId(Profil profilExtensionId) {
        this.profilExtensionId = profilExtensionId;
    }

    @XmlTransient
    public Collection<Personne> getPersonneCollection() {
        return personneCollection;
    }

    public void setPersonneCollection(Collection<Personne> personneCollection) {
        this.personneCollection = personneCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (profilId != null ? profilId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Profil)) {
            return false;
        }
        Profil other = (Profil) object;
        if ((this.profilId == null && other.profilId != null) || (this.profilId != null && !this.profilId.equals(other.profilId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Profil[ profilId=" + profilId + " ]";
    }
    
    // ADDED GET REQUESTS ------------------------------------------------------
    
    public static Profil getItemById(int id) {
        return (Profil) (getItemById(Profil.class, id));
    }
    
    public static void createElement(int role_id, String label ){
        Profil aProfil = Profil.getItemById(role_id);
        // if this role not exist, we add it
        if(aProfil == null){
            aProfil.setProfilId(role_id);
            aProfil.setProfilLibelle(label);
            ItemManager.create(aProfil);
        }
    }
    
    /*-----------------------------------------------------*/
    public static void createDefault(){
        // role: adminisrateur, direction, enseignant, eleve
        createElement(ADMINISTRATEUR, "Administrateur");
        createElement(DIRECTION, "Direction");
        createElement(ENSEIGNANT, "Enseigant");
        createElement(ELEVE, "Eleve");
        
        // TODO: different role can see different menu
        
        
    }
    
    
}
