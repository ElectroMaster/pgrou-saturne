/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author antoinehurard
 */
@Embeddable
public class EvaluationpersonnePK implements Serializable {

    @Basic(optional = false)
    @Column(name = "personne_id")
    private int personneId;
    @Basic(optional = false)
    @Column(name = "evaluation_id")
    private int evaluationId;

    public EvaluationpersonnePK() {
    }

    public EvaluationpersonnePK(int personneId, int evaluationId) {
        this.personneId = personneId;
        this.evaluationId = evaluationId;
    }

    public int getPersonneId() {
        return personneId;
    }

    public void setPersonneId(int personneId) {
        this.personneId = personneId;
    }

    public int getEvaluationId() {
        return evaluationId;
    }

    public void setEvaluationId(int evaluationId) {
        this.evaluationId = evaluationId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) personneId;
        hash += (int) evaluationId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EvaluationpersonnePK)) {
            return false;
        }
        EvaluationpersonnePK other = (EvaluationpersonnePK) object;
        if (this.personneId != other.personneId) {
            return false;
        }
        if (this.evaluationId != other.evaluationId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.EvaluationpersonnePK[ personneId=" + personneId + ", evaluationId=" + evaluationId + " ]";
    }
    
}
