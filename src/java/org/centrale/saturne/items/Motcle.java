/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "motcle")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Motcle.findAll", query = "SELECT m FROM Motcle m")
    , @NamedQuery(name = "Motcle.findByMotcleId", query = "SELECT m FROM Motcle m WHERE m.motcleId = :motcleId")
    , @NamedQuery(name = "Motcle.findByMotcleLibelle", query = "SELECT m FROM Motcle m WHERE m.motcleLibelle = :motcleLibelle")})
public class Motcle extends Item implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "motcle_id")
    private Integer motcleId;
    @Basic(optional = false)
    @Column(name = "motcle_libelle")
    private String motcleLibelle;
    @JoinTable(name = "questionmotcle", joinColumns = {
        @JoinColumn(name = "motcle_id", referencedColumnName = "motcle_id")}, inverseJoinColumns = {
        @JoinColumn(name = "question_id", referencedColumnName = "question_id")})
    @ManyToMany
    private Collection<Question> questionCollection;

    public Motcle() {
    }

    public Motcle(Integer motcleId) {
        this.motcleId = motcleId;
    }

    public Motcle(Integer motcleId, String motcleLibelle) {
        this.motcleId = motcleId;
        this.motcleLibelle = motcleLibelle;
    }

    public Integer getMotcleId() {
        return motcleId;
    }

    public void setMotcleId(Integer motcleId) {
        this.motcleId = motcleId;
    }

    public String getMotcleLibelle() {
        return motcleLibelle;
    }

    public void setMotcleLibelle(String motcleLibelle) {
        this.motcleLibelle = motcleLibelle;
    }
    
    // Questions ---------------------------------------------------------------
    @XmlTransient
    public Collection<Question> getQuestionCollection() {
        return questionCollection;
    }

    public void setQuestionCollection(Collection<Question> questionCollection) {
        this.questionCollection = questionCollection;
    }
    
    public void addQuestion(Question aQuestion) {
        if ((aQuestion != null) && (!this.getQuestionCollection().contains(aQuestion))) {
            this.getQuestionCollection().add(aQuestion);
            aQuestion.addMotcle(this);
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (motcleId != null ? motcleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Motcle)) {
            return false;
        }
        Motcle other = (Motcle) object;
        if ((this.motcleId == null && other.motcleId != null) || (this.motcleId != null && !this.motcleId.equals(other.motcleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Motcle[ motcleId=" + motcleId + " ]";
    }
    
}
