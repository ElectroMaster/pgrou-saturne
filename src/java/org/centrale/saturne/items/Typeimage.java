/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "typeimage")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Typeimage.findAll", query = "SELECT t FROM Typeimage t")
    , @NamedQuery(name = "Typeimage.findByTypeimageId", query = "SELECT t FROM Typeimage t WHERE t.typeimageId = :typeimageId")
    , @NamedQuery(name = "Typeimage.findByTypeimageLibelle", query = "SELECT t FROM Typeimage t WHERE t.typeimageLibelle = :typeimageLibelle")
    , @NamedQuery(name = "Typeimage.findByTypeimageMime", query = "SELECT t FROM Typeimage t WHERE t.typeimageMime = :typeimageMime")})
public class Typeimage implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "typeimage_id")
    private Integer typeimageId;
    @Basic(optional = false)
    @Column(name = "typeimage_libelle")
    private String typeimageLibelle;
    @Basic(optional = false)
    @Column(name = "typeimage_mime")
    private String typeimageMime;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "typeimageId")
    private Collection<Image> imageCollection;

    public Typeimage() {
    }

    public Typeimage(Integer typeimageId) {
        this.typeimageId = typeimageId;
    }

    public Typeimage(Integer typeimageId, String typeimageLibelle, String typeimageMime) {
        this.typeimageId = typeimageId;
        this.typeimageLibelle = typeimageLibelle;
        this.typeimageMime = typeimageMime;
    }

    public Integer getTypeimageId() {
        return typeimageId;
    }

    public void setTypeimageId(Integer typeimageId) {
        this.typeimageId = typeimageId;
    }

    public String getTypeimageLibelle() {
        return typeimageLibelle;
    }

    public void setTypeimageLibelle(String typeimageLibelle) {
        this.typeimageLibelle = typeimageLibelle;
    }

    public String getTypeimageMime() {
        return typeimageMime;
    }

    public void setTypeimageMime(String typeimageMime) {
        this.typeimageMime = typeimageMime;
    }

    @XmlTransient
    public Collection<Image> getImageCollection() {
        return imageCollection;
    }

    public void setImageCollection(Collection<Image> imageCollection) {
        this.imageCollection = imageCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (typeimageId != null ? typeimageId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Typeimage)) {
            return false;
        }
        Typeimage other = (Typeimage) object;
        if ((this.typeimageId == null && other.typeimageId != null) || (this.typeimageId != null && !this.typeimageId.equals(other.typeimageId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Typeimage[ typeimageId=" + typeimageId + " ]";
    }
    
}
