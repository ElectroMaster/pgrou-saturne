/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author antoinehurard
 */
@Embeddable
public class ReponsequestionchoixPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "reponsequestionnaire_id")
    private int reponsequestionnaireId;
    @Basic(optional = false)
    @Column(name = "question_id")
    private int questionId;
    @Basic(optional = false)
    @Column(name = "reponsepossible_id")
    private int reponsepossibleId;

    public ReponsequestionchoixPK() {
    }

    public ReponsequestionchoixPK(int reponsequestionnaireId, int questionId, int reponsepossibleId) {
        this.reponsequestionnaireId = reponsequestionnaireId;
        this.questionId = questionId;
        this.reponsepossibleId = reponsepossibleId;
    }

    public int getReponsequestionnaireId() {
        return reponsequestionnaireId;
    }

    public void setReponsequestionnaireId(int reponsequestionnaireId) {
        this.reponsequestionnaireId = reponsequestionnaireId;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getReponsepossibleId() {
        return reponsepossibleId;
    }

    public void setReponsepossibleId(int reponsepossibleId) {
        this.reponsepossibleId = reponsepossibleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) reponsequestionnaireId;
        hash += (int) questionId;
        hash += (int) reponsepossibleId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReponsequestionchoixPK)) {
            return false;
        }
        ReponsequestionchoixPK other = (ReponsequestionchoixPK) object;
        if (this.reponsequestionnaireId != other.reponsequestionnaireId) {
            return false;
        }
        if (this.questionId != other.questionId) {
            return false;
        }
        if (this.reponsepossibleId != other.reponsepossibleId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.ReponsequestionchoixPK[ reponsequestionnaireId=" + reponsequestionnaireId + ", questionId=" + questionId + ", reponsepossibleId=" + reponsepossibleId + " ]";
    }
    
}
