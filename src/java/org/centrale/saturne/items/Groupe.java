/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "groupe")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Groupe.findAll", query = "SELECT g FROM Groupe g")
    , @NamedQuery(name = "Groupe.findByGroupeId", query = "SELECT g FROM Groupe g WHERE g.groupeId = :groupeId")
    , @NamedQuery(name = "Groupe.findByGroupeLibelle", query = "SELECT g FROM Groupe g WHERE g.groupeLibelle = :groupeLibelle")
    , @NamedQuery(name = "Groupe.findByGroupeValide", query = "SELECT g FROM Groupe g WHERE g.groupeValide = :groupeValide")})
public class Groupe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "groupe_id")
    private Integer groupeId;
    @Basic(optional = false)
    @Column(name = "groupe_libelle")
    private String groupeLibelle;
    @Basic(optional = false)
    @Column(name = "groupe_valide")
    private boolean groupeValide;
    @JoinTable(name = "appartient", joinColumns = {
        @JoinColumn(name = "groupe_id", referencedColumnName = "groupe_id")}, inverseJoinColumns = {
        @JoinColumn(name = "personne_id", referencedColumnName = "personne_id")})
    @ManyToMany
    private Collection<Personne> personneCollection;
    @ManyToMany(mappedBy = "groupeCollection")
    private Collection<Evaluation> evaluationCollection;

    public Groupe() {
    }

    public Groupe(Integer groupeId) {
        this.groupeId = groupeId;
    }

    public Groupe(Integer groupeId, String groupeLibelle, boolean groupeValide) {
        this.groupeId = groupeId;
        this.groupeLibelle = groupeLibelle;
        this.groupeValide = groupeValide;
    }

    public Integer getGroupeId() {
        return groupeId;
    }

    public void setGroupeId(Integer groupeId) {
        this.groupeId = groupeId;
    }

    public String getGroupeLibelle() {
        return groupeLibelle;
    }

    public void setGroupeLibelle(String groupeLibelle) {
        this.groupeLibelle = groupeLibelle;
    }

    public boolean getGroupeValide() {
        return groupeValide;
    }

    public void setGroupeValide(boolean groupeValide) {
        this.groupeValide = groupeValide;
    }

    @XmlTransient
    public Collection<Personne> getPersonneCollection() {
        return personneCollection;
    }

    public void setPersonneCollection(Collection<Personne> personneCollection) {
        this.personneCollection = personneCollection;
    }

    @XmlTransient
    public Collection<Evaluation> getEvaluationCollection() {
        return evaluationCollection;
    }

    public void setEvaluationCollection(Collection<Evaluation> evaluationCollection) {
        this.evaluationCollection = evaluationCollection;
    }

    public void addPersonne(Personne personne) {
        if ((personne != null) && (!this.getPersonneCollection().contains(personne))) {
            this.getPersonneCollection().add(personne);
            personne.addGroupe(this);
        }
    }

    public void removePersonne(Personne personne) {
        if ((personne != null) && (this.getPersonneCollection().contains(personne))) {
            this.getPersonneCollection().remove(personne);
            personne.removeGroupe(this);
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupeId != null ? groupeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Groupe)) {
            return false;
        }
        Groupe other = (Groupe) object;
        if ((this.groupeId == null && other.groupeId != null) || (this.groupeId != null && !this.groupeId.equals(other.groupeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Groupe[ groupeId=" + groupeId + " ]";
    }

}
