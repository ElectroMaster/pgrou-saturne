/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "image")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Image.findAll", query = "SELECT i FROM Image i")
    , @NamedQuery(name = "Image.findByImageId", query = "SELECT i FROM Image i WHERE i.imageId = :imageId")
    , @NamedQuery(name = "Image.findByImageLibelle", query = "SELECT i FROM Image i WHERE i.imageLibelle = :imageLibelle")
    , @NamedQuery(name = "Image.findByImageHauteur", query = "SELECT i FROM Image i WHERE i.imageHauteur = :imageHauteur")})
public class Image implements Serializable {

    @Basic(optional = false)
    @Lob
    @Column(name = "image_contenu")
    private byte[] imageContenu;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "image_id")
    private Integer imageId;
    @Column(name = "image_libelle")
    private String imageLibelle;
    @Column(name = "image_hauteur")
    private Integer imageHauteur;
    @JoinColumn(name = "typeimage_id", referencedColumnName = "typeimage_id")
    @ManyToOne(optional = false)
    private Typeimage typeimageId;
    @OneToMany(mappedBy = "imageId")
    private Collection<Question> questionCollection;
    @OneToMany(mappedBy = "imageId")
    private Collection<Reponsepossible> reponsepossibleCollection;

    public Image() {
    }

    public Image(Integer imageId) {
        this.imageId = imageId;
    }

    public Image(Integer imageId, byte[] imageContenu) {
        this.imageId = imageId;
        this.imageContenu = imageContenu;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public String getImageLibelle() {
        return imageLibelle;
    }

    public void setImageLibelle(String imageLibelle) {
        this.imageLibelle = imageLibelle;
    }

    public byte[] getImageContenu() {
        return imageContenu;
    }

    public void setImageContenu(byte[] imageContenu) {
        this.imageContenu = imageContenu;
    }

    public Integer getImageHauteur() {
        return imageHauteur;
    }

    public void setImageHauteur(Integer imageHauteur) {
        this.imageHauteur = imageHauteur;
    }

    public Typeimage getTypeimageId() {
        return typeimageId;
    }

    public void setTypeimageId(Typeimage typeimageId) {
        this.typeimageId = typeimageId;
    }

    @XmlTransient
    public Collection<Question> getQuestionCollection() {
        return questionCollection;
    }

    public void setQuestionCollection(Collection<Question> questionCollection) {
        this.questionCollection = questionCollection;
    }

    @XmlTransient
    public Collection<Reponsepossible> getReponsepossibleCollection() {
        return reponsepossibleCollection;
    }

    public void setReponsepossibleCollection(Collection<Reponsepossible> reponsepossibleCollection) {
        this.reponsepossibleCollection = reponsepossibleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (imageId != null ? imageId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Image)) {
            return false;
        }
        Image other = (Image) object;
        if ((this.imageId == null && other.imageId != null) || (this.imageId != null && !this.imageId.equals(other.imageId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Image[ imageId=" + imageId + " ]";
    }

//    public byte[] getImageContenu() {
//        return imageContenu;
//    }
//
//    public void setImageContenu(byte[] imageContenu) {
//        this.imageContenu = imageContenu;
//    }
    
}
