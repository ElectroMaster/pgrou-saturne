/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "evaluation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Evaluation.findAll", query = "SELECT e FROM Evaluation e")
    , @NamedQuery(name = "Evaluation.findByEvaluationId", query = "SELECT e FROM Evaluation e WHERE e.evaluationId = :evaluationId")
    , @NamedQuery(name = "Evaluation.findByEvaluationDate", query = "SELECT e FROM Evaluation e WHERE e.evaluationDate = :evaluationDate")
    , @NamedQuery(name = "Evaluation.findByEvaluationDepart", query = "SELECT e FROM Evaluation e WHERE e.evaluationDepart = :evaluationDepart")
    , @NamedQuery(name = "Evaluation.findByEvaluationDuree", query = "SELECT e FROM Evaluation e WHERE e.evaluationDuree = :evaluationDuree")
    , @NamedQuery(name = "Evaluation.findByEvaluationClosed", query = "SELECT e FROM Evaluation e WHERE e.evaluationClosed = :evaluationClosed")})
public class Evaluation extends Item implements Serializable  {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "evaluation_id")
    private Integer evaluationId;
    @Column(name = "evaluation_date")
    @Temporal(TemporalType.DATE)
    private Date evaluationDate;
    @Column(name = "evaluation_depart")
    @Temporal(TemporalType.TIME)
    private Date evaluationDepart;
    @Column(name = "evaluation_duree")
    @Temporal(TemporalType.TIME)
    private Date evaluationDuree;
    @Basic(optional = false)
    @Column(name = "evaluation_closed")
    private boolean evaluationClosed;
    @JoinTable(name = "evaluationgroupe", joinColumns = {
        @JoinColumn(name = "evaluation_id", referencedColumnName = "evaluation_id")}, inverseJoinColumns = {
        @JoinColumn(name = "groupe_id", referencedColumnName = "groupe_id")})
    @ManyToMany
    private Collection<Groupe> groupeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluation")
    private Collection<Evaluationpersonne> evaluationpersonneCollection;
    @JoinColumn(name = "modeevaluation_id", referencedColumnName = "modeevaluation_id")
    @ManyToOne(optional = false)
    private Modeevaluation modeevaluationId;
    @JoinColumn(name = "personne_id", referencedColumnName = "personne_id")
    @ManyToOne(optional = false)
    private Personne personneId;
    @JoinColumn(name = "questionnaire_id", referencedColumnName = "questionnaire_id")
    @ManyToOne(optional = false)
    private Questionnaire questionnaireId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluationId")
    private Collection<Reponsequestionnaire> reponsequestionnaireCollection;

    public Evaluation() {
    }

    public Evaluation(Integer evaluationId) {
        this.evaluationId = evaluationId;
    }

    public Evaluation(Integer evaluationId, boolean evaluationClosed) {
        this.evaluationId = evaluationId;
        this.evaluationClosed = evaluationClosed;
    }

    public Integer getEvaluationId() {
        return evaluationId;
    }

    public void setEvaluationId(Integer evaluationId) {
        this.evaluationId = evaluationId;
    }

    public Date getEvaluationDate() {
        return evaluationDate;
    }

    public void setEvaluationDate(Date evaluationDate) {
        this.evaluationDate = evaluationDate;
    }

    public Date getEvaluationDepart() {
        return evaluationDepart;
    }

    public void setEvaluationDepart(Date evaluationDepart) {
        this.evaluationDepart = evaluationDepart;
    }

    public Date getEvaluationDuree() {
        return evaluationDuree;
    }

    public void setEvaluationDuree(Date evaluationDuree) {
        this.evaluationDuree = evaluationDuree;
    }

    public boolean getEvaluationClosed() {
        return evaluationClosed;
    }

    public void setEvaluationClosed(boolean evaluationClosed) {
        this.evaluationClosed = evaluationClosed;
    }

    @XmlTransient
    public Collection<Groupe> getGroupeCollection() {
        return groupeCollection;
    }

    public void setGroupeCollection(Collection<Groupe> groupeCollection) {
        this.groupeCollection = groupeCollection;
    }

    @XmlTransient
    public Collection<Evaluationpersonne> getEvaluationpersonneCollection() {
        return evaluationpersonneCollection;
    }

    public void setEvaluationpersonneCollection(Collection<Evaluationpersonne> evaluationpersonneCollection) {
        this.evaluationpersonneCollection = evaluationpersonneCollection;
    }

    public Modeevaluation getModeevaluationId() {
        return modeevaluationId;
    }

    public void setModeevaluationId(Modeevaluation modeevaluationId) {
        this.modeevaluationId = modeevaluationId;
    }

    public Personne getPersonneId() {
        return personneId;
    }

    public void setPersonneId(Personne personneId) {
        this.personneId = personneId;
    }

    public Questionnaire getQuestionnaireId() {
        return questionnaireId;
    }

    public void setQuestionnaireId(Questionnaire questionnaireId) {
        this.questionnaireId = questionnaireId;
    }

    @XmlTransient
    public Collection<Reponsequestionnaire> getReponsequestionnaireCollection() {
        return reponsequestionnaireCollection;
    }

    public void setReponsequestionnaireCollection(Collection<Reponsequestionnaire> reponsequestionnaireCollection) {
        this.reponsequestionnaireCollection = reponsequestionnaireCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (evaluationId != null ? evaluationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evaluation)) {
            return false;
        }
        Evaluation other = (Evaluation) object;
        if ((this.evaluationId == null && other.evaluationId != null) || (this.evaluationId != null && !this.evaluationId.equals(other.evaluationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Evaluation[ evaluationId=" + evaluationId + " ]";
    }
    
}
