/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import static org.centrale.saturne.items.Item.getItemById;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "modeevaluation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Modeevaluation.findAll", query = "SELECT m FROM Modeevaluation m")
    , @NamedQuery(name = "Modeevaluation.findByModeevaluationId", query = "SELECT m FROM Modeevaluation m WHERE m.modeevaluationId = :modeevaluationId")
    , @NamedQuery(name = "Modeevaluation.findByModeevaluationLibelle", query = "SELECT m FROM Modeevaluation m WHERE m.modeevaluationLibelle = :modeevaluationLibelle")
    , @NamedQuery(name = "Modeevaluation.findByModeevaluationNotevrai", query = "SELECT m FROM Modeevaluation m WHERE m.modeevaluationNotevrai = :modeevaluationNotevrai")
    , @NamedQuery(name = "Modeevaluation.findByModeevaluatonNotefaux", query = "SELECT m FROM Modeevaluation m WHERE m.modeevaluatonNotefaux = :modeevaluatonNotefaux")})
public class Modeevaluation extends Item implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "modeevaluation_id")
    private Integer modeevaluationId;
    @Basic(optional = false)
    @Column(name = "modeevaluation_libelle")
    private String modeevaluationLibelle;
    @Basic(optional = false)
    @Column(name = "modeevaluation_notevrai")
    private float modeevaluationNotevrai;
    @Basic(optional = false)
    @Column(name = "modeevaluaton_notefaux")
    private float modeevaluatonNotefaux;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "modeevaluationId")
    private Collection<Evaluation> evaluationCollection;

    public Modeevaluation() {
    }

    public Modeevaluation(Integer modeevaluationId) {
        this.modeevaluationId = modeevaluationId;
    }

    public Modeevaluation(Integer modeevaluationId, String modeevaluationLibelle, float modeevaluationNotevrai, float modeevaluatonNotefaux) {
        this.modeevaluationId = modeevaluationId;
        this.modeevaluationLibelle = modeevaluationLibelle;
        this.modeevaluationNotevrai = modeevaluationNotevrai;
        this.modeevaluatonNotefaux = modeevaluatonNotefaux;
    }

    public Integer getModeevaluationId() {
        return modeevaluationId;
    }

    public void setModeevaluationId(Integer modeevaluationId) {
        this.modeevaluationId = modeevaluationId;
    }

    public String getModeevaluationLibelle() {
        return modeevaluationLibelle;
    }

    public void setModeevaluationLibelle(String modeevaluationLibelle) {
        this.modeevaluationLibelle = modeevaluationLibelle;
    }

    public float getModeevaluationNotevrai() {
        return modeevaluationNotevrai;
    }

    public void setModeevaluationNotevrai(float modeevaluationNotevrai) {
        this.modeevaluationNotevrai = modeevaluationNotevrai;
    }

    public float getModeevaluatonNotefaux() {
        return modeevaluatonNotefaux;
    }

    public void setModeevaluatonNotefaux(float modeevaluatonNotefaux) {
        this.modeevaluatonNotefaux = modeevaluatonNotefaux;
    }

    @XmlTransient
    public Collection<Evaluation> getEvaluationCollection() {
        return evaluationCollection;
    }

    public void setEvaluationCollection(Collection<Evaluation> evaluationCollection) {
        this.evaluationCollection = evaluationCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (modeevaluationId != null ? modeevaluationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Modeevaluation)) {
            return false;
        }
        Modeevaluation other = (Modeevaluation) object;
        if ((this.modeevaluationId == null && other.modeevaluationId != null) || (this.modeevaluationId != null && !this.modeevaluationId.equals(other.modeevaluationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Modeevaluation[ modeevaluationId=" + modeevaluationId + " ]";
    }
    
    public static Modeevaluation getItemById(int id) {
        return (Modeevaluation) (getItemById(Modeevaluation.class, id));
    }
}
