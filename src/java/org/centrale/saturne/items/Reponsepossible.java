/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "reponsepossible")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reponsepossible.findAll", query = "SELECT r FROM Reponsepossible r")
    , @NamedQuery(name = "Reponsepossible.findByReponsepossibleId", query = "SELECT r FROM Reponsepossible r WHERE r.reponsepossibleId = :reponsepossibleId")
    , @NamedQuery(name = "Reponsepossible.findByReponsepossibleTexte", query = "SELECT r FROM Reponsepossible r WHERE r.reponsepossibleTexte = :reponsepossibleTexte")
    , @NamedQuery(name = "Reponsepossible.findByReponsepossibleEstsolution", query = "SELECT r FROM Reponsepossible r WHERE r.reponsepossibleEstsolution = :reponsepossibleEstsolution")})
public class Reponsepossible extends Item implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "reponsepossible_id")
    private Integer reponsepossibleId;
    @Column(name = "reponsepossible_texte")
    private String reponsepossibleTexte;
    @Basic(optional = false)
    @Column(name = "reponsepossible_estsolution")
    private boolean reponsepossibleEstsolution;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reponsepossible")
    private Collection<Reponsequestionchoix> reponsequestionchoixCollection;
    @JoinColumn(name = "image_id", referencedColumnName = "image_id")
    @ManyToOne
    private Image imageId;
    @JoinColumn(name = "question_id", referencedColumnName = "question_id")
    @ManyToOne(optional = false)
    private Question questionId;

    public Reponsepossible() {
    }

    public Reponsepossible(Integer reponsepossibleId) {
        this.reponsepossibleId = reponsepossibleId;
    }

    public Reponsepossible(Integer reponsepossibleId, boolean reponsepossibleEstsolution) {
        this.reponsepossibleId = reponsepossibleId;
        this.reponsepossibleEstsolution = reponsepossibleEstsolution;
    }

    public Integer getReponsepossibleId() {
        return reponsepossibleId;
    }

    public void setReponsepossibleId(Integer reponsepossibleId) {
        this.reponsepossibleId = reponsepossibleId;
    }

    public String getReponsepossibleTexte() {
        return reponsepossibleTexte;
    }

    public void setReponsepossibleTexte(String reponsepossibleTexte) {
        this.reponsepossibleTexte = reponsepossibleTexte;
    }

    public boolean getReponsepossibleEstsolution() {
        return reponsepossibleEstsolution;
    }

    public void setReponsepossibleEstsolution(boolean reponsepossibleEstsolution) {
        this.reponsepossibleEstsolution = reponsepossibleEstsolution;
    }

    @XmlTransient
    public Collection<Reponsequestionchoix> getReponsequestionchoixCollection() {
        return reponsequestionchoixCollection;
    }

    public void setReponsequestionchoixCollection(Collection<Reponsequestionchoix> reponsequestionchoixCollection) {
        this.reponsequestionchoixCollection = reponsequestionchoixCollection;
    }

    public Image getImageId() {
        return imageId;
    }

    public void setImageId(Image imageId) {
        this.imageId = imageId;
    }

    public Question getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Question questionId) {
        this.questionId = questionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reponsepossibleId != null ? reponsepossibleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reponsepossible)) {
            return false;
        }
        Reponsepossible other = (Reponsepossible) object;
        if ((this.reponsepossibleId == null && other.reponsepossibleId != null) || (this.reponsepossibleId != null && !this.reponsepossibleId.equals(other.reponsepossibleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Reponsepossible[ reponsepossibleId=" + reponsepossibleId + " ]";
    }
    
}
