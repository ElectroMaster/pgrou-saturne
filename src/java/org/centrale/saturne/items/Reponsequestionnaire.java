/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "reponsequestionnaire")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reponsequestionnaire.findAll", query = "SELECT r FROM Reponsequestionnaire r")
    , @NamedQuery(name = "Reponsequestionnaire.findByReponsequestionnaireId", query = "SELECT r FROM Reponsequestionnaire r WHERE r.reponsequestionnaireId = :reponsequestionnaireId")
    , @NamedQuery(name = "Reponsequestionnaire.findByReponsequestionnaireDepart", query = "SELECT r FROM Reponsequestionnaire r WHERE r.reponsequestionnaireDepart = :reponsequestionnaireDepart")
    , @NamedQuery(name = "Reponsequestionnaire.findByReponsequestionnaireFin", query = "SELECT r FROM Reponsequestionnaire r WHERE r.reponsequestionnaireFin = :reponsequestionnaireFin")})
public class Reponsequestionnaire implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "reponsequestionnaire_id")
    private Integer reponsequestionnaireId;
    @Basic(optional = false)
    @Column(name = "reponsequestionnaire_depart")
    @Temporal(TemporalType.TIMESTAMP)
    private Date reponsequestionnaireDepart;
    @Column(name = "reponsequestionnaire_fin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date reponsequestionnaireFin;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reponsequestionnaire")
    private Collection<Reponsequestion> reponsequestionCollection;
    @JoinColumn(name = "evaluation_id", referencedColumnName = "evaluation_id")
    @ManyToOne(optional = false)
    private Evaluation evaluationId;
    @JoinColumn(name = "personne_id", referencedColumnName = "personne_id")
    @ManyToOne(optional = false)
    private Personne personneId;

    public Reponsequestionnaire() {
    }

    public Reponsequestionnaire(Integer reponsequestionnaireId) {
        this.reponsequestionnaireId = reponsequestionnaireId;
    }

    public Reponsequestionnaire(Integer reponsequestionnaireId, Date reponsequestionnaireDepart) {
        this.reponsequestionnaireId = reponsequestionnaireId;
        this.reponsequestionnaireDepart = reponsequestionnaireDepart;
    }

    public Integer getReponsequestionnaireId() {
        return reponsequestionnaireId;
    }

    public void setReponsequestionnaireId(Integer reponsequestionnaireId) {
        this.reponsequestionnaireId = reponsequestionnaireId;
    }

    public Date getReponsequestionnaireDepart() {
        return reponsequestionnaireDepart;
    }

    public void setReponsequestionnaireDepart(Date reponsequestionnaireDepart) {
        this.reponsequestionnaireDepart = reponsequestionnaireDepart;
    }

    public Date getReponsequestionnaireFin() {
        return reponsequestionnaireFin;
    }

    public void setReponsequestionnaireFin(Date reponsequestionnaireFin) {
        this.reponsequestionnaireFin = reponsequestionnaireFin;
    }

    @XmlTransient
    public Collection<Reponsequestion> getReponsequestionCollection() {
        return reponsequestionCollection;
    }

    public void setReponsequestionCollection(Collection<Reponsequestion> reponsequestionCollection) {
        this.reponsequestionCollection = reponsequestionCollection;
    }

    public Evaluation getEvaluationId() {
        return evaluationId;
    }

    public void setEvaluationId(Evaluation evaluationId) {
        this.evaluationId = evaluationId;
    }

    public Personne getPersonneId() {
        return personneId;
    }

    public void setPersonneId(Personne personneId) {
        this.personneId = personneId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reponsequestionnaireId != null ? reponsequestionnaireId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reponsequestionnaire)) {
            return false;
        }
        Reponsequestionnaire other = (Reponsequestionnaire) object;
        if ((this.reponsequestionnaireId == null && other.reponsequestionnaireId != null) || (this.reponsequestionnaireId != null && !this.reponsequestionnaireId.equals(other.reponsequestionnaireId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Reponsequestionnaire[ reponsequestionnaireId=" + reponsequestionnaireId + " ]";
    }
    
}
