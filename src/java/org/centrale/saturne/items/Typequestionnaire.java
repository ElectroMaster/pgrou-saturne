/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "typequestionnaire")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Typequestionnaire.findAll", query = "SELECT t FROM Typequestionnaire t")
    , @NamedQuery(name = "Typequestionnaire.findByTypequestionnaireId", query = "SELECT t FROM Typequestionnaire t WHERE t.typequestionnaireId = :typequestionnaireId")
    , @NamedQuery(name = "Typequestionnaire.findByTypequestionnaireLibelle", query = "SELECT t FROM Typequestionnaire t WHERE t.typequestionnaireLibelle = :typequestionnaireLibelle")})
public class Typequestionnaire extends Item implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "typequestionnaire_id")
    private Integer typequestionnaireId;
    @Basic(optional = false)
    @Column(name = "typequestionnaire_libelle")
    private String typequestionnaireLibelle;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "typequestionnaireId")
    private Collection<Questionnaire> questionnaireCollection;

    public Typequestionnaire() {
    }

    public Typequestionnaire(Integer typequestionnaireId) {
        this.typequestionnaireId = typequestionnaireId;
    }

    public Typequestionnaire(Integer typequestionnaireId, String typequestionnaireLibelle) {
        this.typequestionnaireId = typequestionnaireId;
        this.typequestionnaireLibelle = typequestionnaireLibelle;
    }

    public Integer getTypequestionnaireId() {
        return typequestionnaireId;
    }

    public void setTypequestionnaireId(Integer typequestionnaireId) {
        this.typequestionnaireId = typequestionnaireId;
    }

    public String getTypequestionnaireLibelle() {
        return typequestionnaireLibelle;
    }

    public void setTypequestionnaireLibelle(String typequestionnaireLibelle) {
        this.typequestionnaireLibelle = typequestionnaireLibelle;
    }

    @XmlTransient
    public Collection<Questionnaire> getQuestionnaireCollection() {
        return questionnaireCollection;
    }

    public void setQuestionnaireCollection(Collection<Questionnaire> questionnaireCollection) {
        this.questionnaireCollection = questionnaireCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (typequestionnaireId != null ? typequestionnaireId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Typequestionnaire)) {
            return false;
        }
        Typequestionnaire other = (Typequestionnaire) object;
        if ((this.typequestionnaireId == null && other.typequestionnaireId != null) || (this.typequestionnaireId != null && !this.typequestionnaireId.equals(other.typequestionnaireId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Typequestionnaire[ typequestionnaireId=" + typequestionnaireId + " ]";
    }
    
        /*-----------------------------------------------------------------*/
    public static Typequestionnaire getItemById(int id) {
        return (Typequestionnaire) (getItemById(Typequestionnaire.class, id));
    }
    
}
