/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "reponsequestionchoix")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reponsequestionchoix.findAll", query = "SELECT r FROM Reponsequestionchoix r")
    , @NamedQuery(name = "Reponsequestionchoix.findByReponsequestionnaireId", query = "SELECT r FROM Reponsequestionchoix r WHERE r.reponsequestionchoixPK.reponsequestionnaireId = :reponsequestionnaireId")
    , @NamedQuery(name = "Reponsequestionchoix.findByQuestionId", query = "SELECT r FROM Reponsequestionchoix r WHERE r.reponsequestionchoixPK.questionId = :questionId")
    , @NamedQuery(name = "Reponsequestionchoix.findByReponsepossibleId", query = "SELECT r FROM Reponsequestionchoix r WHERE r.reponsequestionchoixPK.reponsepossibleId = :reponsepossibleId")
    , @NamedQuery(name = "Reponsequestionchoix.findByReponsequestionchoixOrdre", query = "SELECT r FROM Reponsequestionchoix r WHERE r.reponsequestionchoixOrdre = :reponsequestionchoixOrdre")
    , @NamedQuery(name = "Reponsequestionchoix.findByReponsequestionchoixSelect", query = "SELECT r FROM Reponsequestionchoix r WHERE r.reponsequestionchoixSelect = :reponsequestionchoixSelect")})
public class Reponsequestionchoix implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ReponsequestionchoixPK reponsequestionchoixPK;
    @Basic(optional = false)
    @Column(name = "reponsequestionchoix_ordre")
    private int reponsequestionchoixOrdre;
    @Basic(optional = false)
    @Column(name = "reponsequestionchoix_select")
    private boolean reponsequestionchoixSelect;
    @JoinColumn(name = "reponsepossible_id", referencedColumnName = "reponsepossible_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Reponsepossible reponsepossible;
    @JoinColumns({
        @JoinColumn(name = "reponsequestionnaire_id", referencedColumnName = "reponsequestionnaire_id", insertable = false, updatable = false)
        , @JoinColumn(name = "question_id", referencedColumnName = "question_id", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Reponsequestion reponsequestion;

    public Reponsequestionchoix() {
    }

    public Reponsequestionchoix(ReponsequestionchoixPK reponsequestionchoixPK) {
        this.reponsequestionchoixPK = reponsequestionchoixPK;
    }

    public Reponsequestionchoix(ReponsequestionchoixPK reponsequestionchoixPK, int reponsequestionchoixOrdre, boolean reponsequestionchoixSelect) {
        this.reponsequestionchoixPK = reponsequestionchoixPK;
        this.reponsequestionchoixOrdre = reponsequestionchoixOrdre;
        this.reponsequestionchoixSelect = reponsequestionchoixSelect;
    }

    public Reponsequestionchoix(int reponsequestionnaireId, int questionId, int reponsepossibleId) {
        this.reponsequestionchoixPK = new ReponsequestionchoixPK(reponsequestionnaireId, questionId, reponsepossibleId);
    }

    public ReponsequestionchoixPK getReponsequestionchoixPK() {
        return reponsequestionchoixPK;
    }

    public void setReponsequestionchoixPK(ReponsequestionchoixPK reponsequestionchoixPK) {
        this.reponsequestionchoixPK = reponsequestionchoixPK;
    }

    public int getReponsequestionchoixOrdre() {
        return reponsequestionchoixOrdre;
    }

    public void setReponsequestionchoixOrdre(int reponsequestionchoixOrdre) {
        this.reponsequestionchoixOrdre = reponsequestionchoixOrdre;
    }

    public boolean getReponsequestionchoixSelect() {
        return reponsequestionchoixSelect;
    }

    public void setReponsequestionchoixSelect(boolean reponsequestionchoixSelect) {
        this.reponsequestionchoixSelect = reponsequestionchoixSelect;
    }

    public Reponsepossible getReponsepossible() {
        return reponsepossible;
    }

    public void setReponsepossible(Reponsepossible reponsepossible) {
        this.reponsepossible = reponsepossible;
    }

    public Reponsequestion getReponsequestion() {
        return reponsequestion;
    }

    public void setReponsequestion(Reponsequestion reponsequestion) {
        this.reponsequestion = reponsequestion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reponsequestionchoixPK != null ? reponsequestionchoixPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reponsequestionchoix)) {
            return false;
        }
        Reponsequestionchoix other = (Reponsequestionchoix) object;
        if ((this.reponsequestionchoixPK == null && other.reponsequestionchoixPK != null) || (this.reponsequestionchoixPK != null && !this.reponsequestionchoixPK.equals(other.reponsequestionchoixPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Reponsequestionchoix[ reponsequestionchoixPK=" + reponsequestionchoixPK + " ]";
    }
    
}
