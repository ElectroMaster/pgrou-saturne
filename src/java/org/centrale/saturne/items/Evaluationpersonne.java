/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "evaluationpersonne")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Evaluationpersonne.findAll", query = "SELECT e FROM Evaluationpersonne e")
    , @NamedQuery(name = "Evaluationpersonne.findByPersonneId", query = "SELECT e FROM Evaluationpersonne e WHERE e.evaluationpersonnePK.personneId = :personneId")
    , @NamedQuery(name = "Evaluationpersonne.findByEvaluationId", query = "SELECT e FROM Evaluationpersonne e WHERE e.evaluationpersonnePK.evaluationId = :evaluationId")
    , @NamedQuery(name = "Evaluationpersonne.findByEvaluationpersonneCode", query = "SELECT e FROM Evaluationpersonne e WHERE e.evaluationpersonneCode = :evaluationpersonneCode")})
public class Evaluationpersonne implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EvaluationpersonnePK evaluationpersonnePK;
    @Column(name = "evaluationpersonne_code")
    private String evaluationpersonneCode;
    @JoinColumn(name = "evaluation_id", referencedColumnName = "evaluation_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Evaluation evaluation;
    @JoinColumn(name = "personne_id", referencedColumnName = "personne_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Personne personne;

    public Evaluationpersonne() {
    }

    public Evaluationpersonne(EvaluationpersonnePK evaluationpersonnePK) {
        this.evaluationpersonnePK = evaluationpersonnePK;
    }

    public Evaluationpersonne(int personneId, int evaluationId) {
        this.evaluationpersonnePK = new EvaluationpersonnePK(personneId, evaluationId);
    }

    public EvaluationpersonnePK getEvaluationpersonnePK() {
        return evaluationpersonnePK;
    }

    public void setEvaluationpersonnePK(EvaluationpersonnePK evaluationpersonnePK) {
        this.evaluationpersonnePK = evaluationpersonnePK;
    }

    public String getEvaluationpersonneCode() {
        return evaluationpersonneCode;
    }

    public void setEvaluationpersonneCode(String evaluationpersonneCode) {
        this.evaluationpersonneCode = evaluationpersonneCode;
    }

    public Evaluation getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Evaluation evaluation) {
        this.evaluation = evaluation;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (evaluationpersonnePK != null ? evaluationpersonnePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evaluationpersonne)) {
            return false;
        }
        Evaluationpersonne other = (Evaluationpersonne) object;
        if ((this.evaluationpersonnePK == null && other.evaluationpersonnePK != null) || (this.evaluationpersonnePK != null && !this.evaluationpersonnePK.equals(other.evaluationpersonnePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Evaluationpersonne[ evaluationpersonnePK=" + evaluationpersonnePK + " ]";
    }
    
}
