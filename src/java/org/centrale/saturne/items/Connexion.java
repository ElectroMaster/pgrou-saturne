/*
 * Projet Saturne
 * 
 * Gestion de QCM en ligne
 *
 * Ecole Centrale Nantes - Fevrier 2019
 */
package org.centrale.saturne.items;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import org.centrale.saturne.utilities.Utilities;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "connexion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Connexion.findAll", query = "SELECT c FROM Connexion c")
    , @NamedQuery(name = "Connexion.findByConnexionCode", query = "SELECT c FROM Connexion c WHERE c.connexionCode = :connexionCode")
    , @NamedQuery(name = "Connexion.findByConnexionExpire", query = "SELECT c FROM Connexion c WHERE c.connexionExpire = :connexionExpire")
    , @NamedQuery(name = "Connexion.findConnexionExpireBefore", query = "SELECT c FROM Connexion c WHERE c.connexionExpire <= :connexionExpire")})
public class Connexion extends Item {

    private static final int MAXCODE = 32;
    private static final long DELTA = 1800000;

    @Id
    @Basic(optional = false)
    @Column(name = "connexion_code")
    private String connexionCode;
    @Basic(optional = false)
    @Column(name = "connexion_expire")
    @Temporal(TemporalType.TIMESTAMP)
    private Date connexionExpire;
    @JoinColumn(name = "personne_id", referencedColumnName = "personne_id")
    @ManyToOne(optional = false)
    private Personne personneId;

    // -------------------------------------------------------------------------
    public Connexion() {
        super();
    }

    public Connexion(String connexionCode) {
        super();
        this.connexionCode = connexionCode;
    }

    public Connexion(String connexionCode, Date connexionExpire) {
        super();
        this.connexionCode = connexionCode;
        this.connexionExpire = connexionExpire;
    }

    public static Connexion getConnexion(String code) {
        return ItemManager.getConnexion(code);
    }

    // -------------------------------------------------------------------------
    // Authentication code
    public String getConnexionCode() {
        return connexionCode;
    }

    public void setConnexionCode() {
        this.connexionCode = createRandomCode();
    }

    public void setConnexionCode(String connexionCode) {
        this.connexionCode = connexionCode;
    }

    // -------------------------------------------------------------------------
    // Connexion expiration date
    public Date getConnexionExpire() {
        return connexionExpire;
    }

    public void setConnexionExpire() {
        this.connexionExpire = Utilities.getCurrentTime();
        this.expand();
    }

    public void setConnexionExpire(Date connexionExpire) {
        this.connexionExpire = connexionExpire;
    }

    public void expand() {
        if (this.connexionExpire != null) {
            long current = Utilities.getCurrentTime().getTime();
            long nextTime = current + DELTA;
            if (nextTime > this.connexionExpire.getTime()) {
                Date newExpire = new Date(current + DELTA);
                this.setConnexionExpire(newExpire);
            }
        }
    }

    // -------------------------------------------------------------------------
    // Connected person
    public Personne getPersonneId() {
        return personneId;
    }

    public void setPersonneId(Personne personneId) {
        if (this.personneId != personneId) {
            if (this.personneId != null) {
                this.personneId.removeConnexion(this);
            }
            this.personneId = personneId;
            if (personneId != null) {
                personneId.addConnexion(this);
            }
        }
    }

    // -------------------------------------------------------------------------
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (connexionCode != null ? connexionCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Connexion)) {
            return false;
        }
        Connexion other = (Connexion) object;
        if ((this.connexionCode == null && other.connexionCode != null) || (this.connexionCode != null && !this.connexionCode.equals(other.connexionCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.astrolab.saturne.item.Connexion[ connexionCode=" + connexionCode + " ]";
    }

    // -------------------------------------------------------------------------
    // Tools
    /**
     * Check if connected user can use page
     *
     * @param page
     * @return
     */
    /*public boolean canUsePage(String page) {
        Personne personne = this.getPersonneId();
        Collection<Menu> listeMenus = personne.getMenuCollection();

        boolean found = false;
        for (Menu menu : listeMenus) {
            if (menu.getMenuFonction().equals(page)) {
                found = true;
                break;
            }
        }
        return found;
    }*/

    /**
     * Create unique random code
     *
     * @return code
     */
    private static String createRandomCode() {
        StringBuilder code = new StringBuilder();

        // Start with character
        code.append((char) ('A' + (int) (Math.random() * 26)));

        // Fill with current timestamp
        long now = Calendar.getInstance().getTime().getTime();
        int index = 0;
        long value = now;
        while ((value > 0) || (index < MAXCODE - 5)) {
            code.append((char) ('0' + (value % 10)));
            value = value / 10;
            index++;
        }
        // Add any sequance of 4 characters
        code.append((char) ('A' + (int) (Math.random() * 26)));
        code.append((char) ('A' + (int) (Math.random() * 26)));
        code.append((char) ('A' + (int) (Math.random() * 26)));
        code.append((char) ('A' + (int) (Math.random() * 26)));

        return code.toString();
    }


    /**
     * Remove old connections
     */
    public static void removeOldConnexions() {
        ItemManager.removeOldConnexions();
    }
}
