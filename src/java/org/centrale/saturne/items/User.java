/*
 * Projet Saturne
 * 
 * Gestion de QCM en ligne
 *
 * Ecole Centrale Nantes - Fevrier 2019
 */
package org.centrale.saturne.items;

/**
 * User Management for Login
 * @author antoinehurard
 */
public class User {
    private String login;
    private String passw;
    
    public User(){
        
    }
    
    public String getLogin(){
        return login;
    }
    
    public void setLogin(String login){
        this.login = login;
    }
    
    public String getPassw(){
        return passw;
    }
    
    public void setPassw(String passw){
        this.passw = passw;
    }
}
