/*
 * Projet Saturne
 * 
 * Gestion de QCM en ligne
 *
 * Ecole Centrale Nantes - Fevrier 2019
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityTransaction;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.codec.digest.DigestUtils;
import org.centrale.saturne.ldap.LDAPUser;
// For apache DigestUtils, use https://commons.apache.org/proper/commons-codec/download_codec.cgi to get JAR

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "personne")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Personne.findAll", query = "SELECT p FROM Personne p")
    , @NamedQuery(name = "Personne.findByPersonneId", query = "SELECT p FROM Personne p WHERE p.personneId = :personneId")
    , @NamedQuery(name = "Personne.findByPersonneNom", query = "SELECT p FROM Personne p WHERE p.personneNom = :personneNom")
    , @NamedQuery(name = "Personne.findByPersonnePrenom", query = "SELECT p FROM Personne p WHERE p.personnePrenom = :personnePrenom")
    , @NamedQuery(name = "Personne.findByPersonneUid", query = "SELECT p FROM Personne p WHERE p.personneUid = :personneUid")
    , @NamedQuery(name = "Personne.findByPersonnePassword", query = "SELECT p FROM Personne p WHERE p.personnePassword = :personnePassword")
    , @NamedQuery(name = "Personne.findByPersonneEmail", query = "SELECT p FROM Personne p WHERE p.personneEmail = :personneEmail")
    , @NamedQuery(name = "Personne.findByPersonneNumeroetudiant", query = "SELECT p FROM Personne p WHERE p.personneNumeroetudiant = :personneNumeroetudiant")
    , @NamedQuery(name = "Personne.findByPersonneValide", query = "SELECT p FROM Personne p WHERE p.personneValide = :personneValide")})
public class Personne extends Item implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "personne_id")
    private Integer personneId;
    @Basic(optional = false)
    @Column(name = "personne_nom")
    private String personneNom;
    @Basic(optional = false)
    @Column(name = "personne_prenom")
    private String personnePrenom;
    @Column(name = "personne_uid")
    private String personneUid;
    @Column(name = "personne_password")
    private String personnePassword;
    @Column(name = "personne_email")
    private String personneEmail;
    @Column(name = "personne_numeroetudiant")
    private String personneNumeroetudiant;
    @Basic(optional = false)
    @Column(name = "personne_valide")
    private boolean personneValide;
    @ManyToMany(mappedBy = "personneCollection")
    private Collection<Groupe> groupeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personne")
    private Collection<Evaluationpersonne> evaluationpersonneCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personneId")
    private Collection<Questionnaire> questionnaireCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personneId")
    private Collection<Question> questionCollection;
    @JoinColumn(name = "profil_id", referencedColumnName = "profil_id")
    @ManyToOne
    private Profil profilId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personneId")
    private Collection<Connexion> connexionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personneId")
    private Collection<Evaluation> evaluationCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personneId")
    private Collection<Reponsequestionnaire> reponsequestionnaireCollection;

    public Personne() {
    }

    public Personne(Integer personneId) {
        this.personneId = personneId;
    }

    public Personne(Integer personneId, String personneNom, String personnePrenom, boolean personneValide) {
        this.personneId = personneId;
        this.personneNom = personneNom;
        this.personnePrenom = personnePrenom;
        this.personneValide = personneValide;
    }

    // ADDED GET REQUESTS ------------------------------------------------------
    public static Personne getItemById(int id) {
        return (Personne) (getItemById(Personne.class, id));
    }

    public static Personne getPersonneByUID(String uid) {
        return ItemManager.getPersonneByUID(uid);
    }

    // Non-LDAP Connexion ------------------------------------------------------
    public void setPersonneEncodedPassword(String personnePassword) {
        this.setPersonnePassword(encodePassword(personnePassword));
    }

    /**
     * Encode password using SHA256 algorithm
     *
     * @param password
     * @return
     */
    private String encodePassword(String password) {
        byte[] encodedBytes = DigestUtils.sha256(password);
        String encoded = new String(encodedBytes);
        return encoded;
    }

    public boolean validPassword(String password) {
        if ((this.personnePassword != null) && (!this.personnePassword.isEmpty())
                && (password != null) && (!password.isEmpty())) {
            if (this.personnePassword.equals(encodePassword(password))) {
                return true;
            }
        }
        return false;
    }

    public Integer getPersonneId() {
        return personneId;
    }

    public void setPersonneId(Integer personneId) {
        this.personneId = personneId;
    }

    public String getPersonneNom() {
        return personneNom;
    }

    public void setPersonneNom(String personneNom) {
        this.personneNom = personneNom;
    }

    public String getPersonnePrenom() {
        return personnePrenom;
    }

    public void setPersonnePrenom(String personnePrenom) {
        this.personnePrenom = personnePrenom;
    }

    public String getPersonneUid() {
        return personneUid;
    }

    public void setPersonneUid(String personneUid) {
        this.personneUid = personneUid;
    }

    public String getPersonnePassword() {
        return personnePassword;
    }

    public void setPersonnePassword(String personnePassword) {
        this.personnePassword = personnePassword;
    }

    public String getPersonneEmail() {
        return personneEmail;
    }

    public void setPersonneEmail(String personneEmail) {
        this.personneEmail = personneEmail;
    }

    public String getPersonneNumeroetudiant() {
        return personneNumeroetudiant;
    }

    public void setPersonneNumeroetudiant(String personneNumeroetudiant) {
        this.personneNumeroetudiant = personneNumeroetudiant;
    }

    public boolean getPersonneValide() {
        return personneValide;
    }

    public void setPersonneValide(boolean personneValide) {
        this.personneValide = personneValide;
    }

    @XmlTransient
    public Collection<Groupe> getGroupeCollection() {
        return groupeCollection;
    }

    public void setGroupeCollection(Collection<Groupe> groupeCollection) {
        this.groupeCollection = groupeCollection;
    }

    public void addGroupe(Groupe groupe) {
        if ((groupe != null) && (!this.getGroupeCollection().contains(groupe))) {
            this.getGroupeCollection().add(groupe);
            groupe.addPersonne(this);
        }
    }

    public void removeGroupe(Groupe groupe) {
        if ((groupe != null) && (this.getGroupeCollection().contains(groupe))) {
            this.getGroupeCollection().remove(groupe);
            groupe.removePersonne(this);
        }
    }

    @XmlTransient
    public Collection<Evaluationpersonne> getEvaluationpersonneCollection() {
        return evaluationpersonneCollection;
    }

    public void setEvaluationpersonneCollection(Collection<Evaluationpersonne> evaluationpersonneCollection) {
        this.evaluationpersonneCollection = evaluationpersonneCollection;
    }

    @XmlTransient
    public Collection<Questionnaire> getQuestionnaireCollection() {
        return questionnaireCollection;
    }

    public void setQuestionnaireCollection(Collection<Questionnaire> questionnaireCollection) {
        this.questionnaireCollection = questionnaireCollection;
    }

    @XmlTransient
    public Collection<Question> getQuestionCollection() {
        return questionCollection;
    }

    public void setQuestionCollection(Collection<Question> questionCollection) {
        this.questionCollection = questionCollection;
    }

    public Profil getProfilId() {
        return profilId;
    }

    public void setProfilId(Profil profilId) {
        this.profilId = profilId;
    }

    // Connexion ---------------------------------------------------------------
    @XmlTransient
    public Collection<Connexion> getConnexionCollection() {
        return connexionCollection;
    }

    public void setConnexionCollection(Collection<Connexion> connexionCollection) {
        this.connexionCollection = connexionCollection;
    }

    public void addConnexion(Connexion aConnexion) {
        if ((aConnexion != null) && (!this.getConnexionCollection().contains(aConnexion))) {
            this.getConnexionCollection().add(aConnexion);
            aConnexion.setPersonneId(this);
        }
    }

    public void removeConnexion(Connexion aConnexion) {
        if ((aConnexion != null) && (this.getConnexionCollection().contains(aConnexion))) {
            this.getConnexionCollection().remove(aConnexion);
            aConnexion.setPersonneId(null);
        }
    }
    //--------------------------------------------------------------------------

    @XmlTransient
    public Collection<Evaluation> getEvaluationCollection() {
        return evaluationCollection;
    }

    public void setEvaluationCollection(Collection<Evaluation> evaluationCollection) {
        this.evaluationCollection = evaluationCollection;
    }

    @XmlTransient
    public Collection<Reponsequestionnaire> getReponsequestionnaireCollection() {
        return reponsequestionnaireCollection;
    }

    public void setReponsequestionnaireCollection(Collection<Reponsequestionnaire> reponsequestionnaireCollection) {
        this.reponsequestionnaireCollection = reponsequestionnaireCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personneId != null ? personneId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Personne)) {
            return false;
        }
        Personne other = (Personne) object;
        if ((this.personneId == null && other.personneId != null) || (this.personneId != null && !this.personneId.equals(other.personneId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Personne[ personneId=" + personneId + " ]";
    }

    /*------------------------------------------------------------------------*/
    private static void createElement(int id, String nom, String prenom, String uid, String email, int profil) {
        Personne defaultUser = Personne.getItemById(id);
        // if this row has no data, we add data
        // if not empty we do nothing
        if (defaultUser == null) {
            defaultUser = new Personne();
            defaultUser.setPersonneNom(nom);
            defaultUser.setPersonnePrenom(prenom);
            defaultUser.setPersonneEmail(email);
            defaultUser.setPersonneUid(uid);
            defaultUser.setPersonneValide(true);
            // add it in database Personne table
            ItemManager.create(defaultUser);
        }

        if (defaultUser.profilId == null) {
            EntityTransaction transaction = ItemManager.begin();
            defaultUser.setProfilId(Profil.getItemById(profil));
            ItemManager.commit(transaction);

        }

    }

    /**
     * if no data in table, create data before application start
     */
    private static void createDefault() {
        // we need to add profil firstly. 
        // TODO: need to optimize, because Maybe profile.createDefault will be called two times...
        Profil.createDefault();
        createElement(1, "Administrateur", "", "", "", 1);
    }

}
