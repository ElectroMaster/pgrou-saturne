/*
 * Projet Saturne
 * 
 * Gestion de QCM en ligne
 *
 * Ecole Centrale Nantes - Fevrier 2019
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import org.centrale.saturne.utilities.Utilities;

/**
 * Manages Database items
 *
 * @author antoinehurard
 */
public class ItemManager {
    private static final String PUNAME = "saturnePU";
    private EntityManagerFactory emf = null;
    private EntityManager em;
    private static ItemManager theItemManager;

    private static String dbUser;
    private static String dbPass;
    private static String dbURL;
    private static String dbDriver;

    /**
     * Create an ItemManager Called only once.
     */
    private ItemManager() {
        if(emf == null){
        // Create the factory
        emf = Persistence.createEntityManagerFactory(PUNAME);
        // Clear cache
        emf.getCache().evictAll();
        // Create unique enity manager
        em = emf.createEntityManager();
        em.clear();
        }

    }

    /**
     * Get the unique ItemManager
     *
     * @return
     */
    public static ItemManager getManager() {
        if (theItemManager == null) {
            // First call
            theItemManager = new ItemManager();
            try {
                Map<String, Object> properties = theItemManager.em.getProperties();
                dbUser = (String) properties.get("javax.persistence.jdbc.user");
                dbPass = (String) properties.get("javax.persistence.jdbc.password");
                dbURL = (String) properties.get("javax.persistence.jdbc.url");
                dbDriver = (String) properties.get("javax.persistence.jdbc.driver");
            } catch (Exception ex) {
                Logger.getLogger(ItemManager.class.getName()).log(Level.INFO, null, ex);
            }
        }
        return theItemManager;
    }

    /**
     * Get EntityManager
     *
     * @return
     */
    public static EntityManager getEntityManager() {
        return getManager().em;
    }

//    public static EntityTransaction getTransaction() {
//        ItemManager manager = getManager();
//        return em.getTransaction();
//    }

    /**
     *  Get transaction (not begin yet.)
     * @return 
     */
    public static EntityTransaction getTransaction(){
        ItemManager manager = getManager();
        return manager.em.getTransaction();
    }
    
    
    
    /**
     * Start transaction
     *
     * @return
     */
    public static EntityTransaction begin() {
        EntityTransaction transaction = getManager().em.getTransaction();
        transaction.begin();
        return transaction;
    }

    /**
     * Commit transaction
     *
     * @param transaction
     */
    public static void commit(EntityTransaction transaction) {
        if (transaction != null) {
            // There is a transaction
            EntityManager em = getManager().em;
            em.flush();
            transaction.commit();
        }
    }

    /**
     * Create new item
     *
     * @param item
     */
    public static void create(Item item) {
        try {
            boolean weStartedTransaction = false;
            EntityManager em = getManager().em;
            EntityTransaction transaction = em.getTransaction();
            if (!transaction.isActive()) {
                // Never start an active transaction
                transaction.begin();
                weStartedTransaction = true;
            }
            em.persist(item);
            if (weStartedTransaction) {
                // we commit transaction only if we started it
                transaction.commit();
                em.merge(item);
                em.refresh(item);
            }
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(ItemManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PersistenceException ex) {
            Logger.getLogger(ItemManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Get any kind of object
     *
     * @param theClass
     * @return
     */
    public static Collection<Serializable> getAll(Class theClass) {
        EntityManager em = ItemManager.getManager().em;
        try {
            String name = theClass.getSimpleName();
            Query query = em.createNamedQuery(name + ".findAll", theClass);
            Collection<Serializable> returned = (Collection<Serializable>) query.getResultList();
            return returned;
        } catch (NoResultException ex) {
            return new ArrayList<Serializable>();
        }
    }

    /**
     * Get any kind of object by its ID
     *
     * @param theClass
     * @param id
     * @return
     */
    public static Serializable getFromId(Class theClass, int id) {
        EntityManager em = ItemManager.getManager().em;
        try {
            String name = theClass.getSimpleName();
            Query query = em.createNamedQuery(name + ".findBy" + name + "Id", theClass);
            query.setParameter(name.toLowerCase() + "Id", id);
            Serializable returned = (Serializable) query.getSingleResult();
            return returned;
        } catch (NoResultException ex) {
            return null;
        }
    }

    /**
     * Get Personne bi his/her UID
     *
     * @param uid
     * @return
     */
    public static Personne getPersonneByUID(String uid) {
        if ((uid != null) && (!uid.isEmpty())) {
            EntityManager em = ItemManager.getManager().em;
            Query query = em.createNamedQuery("Personne.findByPersonneUid", Personne.class);
            query.setParameter("personneUid", uid);
            try {
                Personne personne = (Personne) query.getSingleResult();
                return personne;
            } catch (NoResultException ex) {
            }
        }
        return null;
    }

    /**
     * Get Personne by his/her UID and password
     *
     * @param login
     * @param password
     * @return
     */
    public static Personne getPersonneByLoginPassword(String login, String password) {
        if ((login != null) && (password != null) && (!login.isEmpty()) && (!password.isEmpty())) {
            EntityManager em = ItemManager.getManager().em;
            Query query = em.createNativeQuery("SELECT Personne.*"
                    + " FROM Personne"
                    + " WHERE Personne_UID=?1"
                    + " AND Personne.Personne_Valide=TRUE", Personne.class);
            query.setParameter(1, login);
            try {
                List<Personne> listePersonne = query.getResultList();
                Personne personne = null;
                for (Personne aPersonne : listePersonne) {
                    if ((aPersonne.getPersonnePassword() != null)
                            && (aPersonne.validPassword(password))) {
                        personne = aPersonne;
                        break;
                    }
                }
                return personne;
            } catch (NoResultException ex) {
            }
        }
        return null;
    }

    /**
     * Get any kind of object by its set of IDs
     *
     * @param owner
     * @param elements
     * @return
     */
    public static Serializable getUniqueFromKey(Class owner, HashMap<Class, Integer> elements) {
        EntityManager em = ItemManager.getManager().em;
        try {
            Query query = em.createNamedQuery("findByKey", owner);
            for (Class theClass : elements.keySet()) {
                String name = theClass.getSimpleName();
                query.setParameter(name.toLowerCase() + "Id", elements.get(theClass));
            }
            Serializable returned = (Serializable) query.getSingleResult();
            return returned;
        } catch (NoResultException ex) {
            return null;
        }
    }

    /**
     * Remove old connections
     */
    public static void removeOldConnexions() {
        EntityManager em = ItemManager.getManager().em;

        // Get old connections
        Query query = em.createNamedQuery("Connexion.findConnexionExpireBefore", Connexion.class);
        Date now = Utilities.getCurrentTime();
        query.setParameter("connexionExpire", now);
        Collection<Connexion> theConnexions = (Collection<Connexion>) query.getResultList();
        if (!theConnexions.isEmpty()) {
            // remove them
            em.getTransaction().begin();
            for (Connexion aConnexion : theConnexions) {
                em.remove(aConnexion);
            }
            em.getTransaction().commit();
        }
    }

    /**
     * Remove specific connexion code
     *
     * @param code
     */
    public static void cancelConnexion(String code) {
        EntityManager em = ItemManager.getManager().em;

        try {
            Query query = em.createNamedQuery("Connexion.findByConnexionCode", Connexion.class);
            query.setParameter("connexionCode", code);
            Connexion theConnexion = (Connexion) query.getSingleResult();
            if (theConnexion != null) {
                em.getTransaction().begin();
                em.remove(theConnexion);
                em.getTransaction().commit();
            }
        } catch (NoResultException ex) {
        }
    }

    /**
     *
     * @param code
     * @return
     */
    public static Connexion getConnexion(String code) {
        if ((code != null) && (!code.isEmpty())) {
            EntityManager em = ItemManager.getManager().em;
            Query query = em.createNamedQuery("Connexion.findByConnexionCode", Connexion.class);
            query.setParameter("connexionCode", code);
            try {
                Connexion connexion = (Connexion) query.getSingleResult();
                return connexion;
            } catch (Exception ex) {
                return null;
            }
        } else {
            return null;
        }
    }

    public static void resetSequences() {
        EntityManager em = ItemManager.getManager().em;

        // Get Item package name
        String packageName = ItemManager.class.getPackage().getName();
        // Get all class names in this package
        Map<String, String> classes = Utilities.getClasses(packageName);

        for (String aClass : classes.keySet()) {
            try {
                String fullClassName = classes.get(aClass);
                Class theClass = Class.forName(fullClassName);
                String className = theClass.getSimpleName().toLowerCase();

                // If class implements itemwithsequence, then it uses sequences.
                if ((ItemWithSequence.class.isAssignableFrom(theClass))
                        && (!className.equals("itemwithSequence"))) {
                    // get sequence
                    String seqName = "seq_" + className;

                    // get Query sizze
                    Query query = em.createNativeQuery("SELECT COUNT(" + className + "_id) AS cpt FROM " + className);
                    long cpt = (Long) query.getSingleResult();

                    if (cpt > 0) {
                        // There is at least 1 item in the table
                        Query query2 = em.createNativeQuery("SELECT MAX(" + className + "_id) AS cpt FROM " + className);
                        int maxVal = (Integer) query2.getSingleResult();
                        em.getTransaction().begin();
                        // next id begin with maxVal + 1;
                        Query update = em.createNativeQuery("SELECT setval('" + seqName + "', " + maxVal + ", true)");
                        update.getResultList();
                        em.getTransaction().commit();
                    } else {
                        // No item in the table;
                        em.getTransaction().begin();
                        // id begin with 1
                        Query update = em.createNamedQuery("SELECT setval('" + seqName + "', 1, false)");
                        update.getResultList();
                        em.getTransaction().commit();
                    }
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ItemManager.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(ItemManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void createDefault() {
        //
        // Get Item package name
        String packageName = ItemManager.class.getPackage().getName();
        // Get all class names in this package
        Map<String, String> classes = Utilities.getClasses(packageName);

        // invoke createDefault method of all classes in this package
        // except this class itself.
        for (String aClass : classes.keySet()) {
            try {
                String fullClassName = classes.get(aClass);
                if (!fullClassName.toLowerCase().contains("itemmanager")) {
                    // avoid ItemManager recrsively call to this method
                    Class theClass = Class.forName(fullClassName);
                    Method aMethod = theClass.getDeclaredMethod("createDefault");
                    // if the class have createDefault method, invoke it
                    if (aMethod != null) {
                        aMethod.invoke(aMethod);
                    }
                }
            } catch (NoSuchMethodException ex) {
                // not an exception to report
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ItemManager.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ItemManager.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(ItemManager.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(ItemManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
