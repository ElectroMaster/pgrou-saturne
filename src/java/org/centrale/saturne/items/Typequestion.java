/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "typequestion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Typequestion.findAll", query = "SELECT t FROM Typequestion t")
    , @NamedQuery(name = "Typequestion.findByTypequestionId", query = "SELECT t FROM Typequestion t WHERE t.typequestionId = :typequestionId")
    , @NamedQuery(name = "Typequestion.findByTypequestionLibelle", query = "SELECT t FROM Typequestion t WHERE t.typequestionLibelle = :typequestionLibelle")})
public class Typequestion extends Item implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "typequestion_id")
    private Integer typequestionId;
    @Basic(optional = false)
    @Column(name = "typequestion_libelle")
    private String typequestionLibelle;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "typequestionId")
    private Collection<Question> questionCollection;

    public Typequestion() {
    }

    public Typequestion(Integer typequestionId) {
        this.typequestionId = typequestionId;
    }

    public Typequestion(Integer typequestionId, String typequestionLibelle) {
        this.typequestionId = typequestionId;
        this.typequestionLibelle = typequestionLibelle;
    }

    public Integer getTypequestionId() {
        return typequestionId;
    }

    public void setTypequestionId(Integer typequestionId) {
        this.typequestionId = typequestionId;
    }

    public String getTypequestionLibelle() {
        return typequestionLibelle;
    }

    public void setTypequestionLibelle(String typequestionLibelle) {
        this.typequestionLibelle = typequestionLibelle;
    }

    @XmlTransient
    public Collection<Question> getQuestionCollection() {
        return questionCollection;
    }

    public void setQuestionCollection(Collection<Question> questionCollection) {
        this.questionCollection = questionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (typequestionId != null ? typequestionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Typequestion)) {
            return false;
        }
        Typequestion other = (Typequestion) object;
        if ((this.typequestionId == null && other.typequestionId != null) || (this.typequestionId != null && !this.typequestionId.equals(other.typequestionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Typequestion[ typequestionId=" + typequestionId + " ]";
    }
    
    /*-----------------------------------------------------------------*/
    public static Typequestion getItemById(int id) {
        return (Typequestion) (getItemById(Typequestion.class, id));
    }
    
}
