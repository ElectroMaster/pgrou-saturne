/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "menu")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Menu.findAll", query = "SELECT m FROM Menu m")
    , @NamedQuery(name = "Menu.findByMenuId", query = "SELECT m FROM Menu m WHERE m.menuId = :menuId")
    , @NamedQuery(name = "Menu.findByMenuLibelle", query = "SELECT m FROM Menu m WHERE m.menuLibelle = :menuLibelle")
    , @NamedQuery(name = "Menu.findByMenuOrdre", query = "SELECT m FROM Menu m WHERE m.menuOrdre = :menuOrdre")
    , @NamedQuery(name = "Menu.findByMenuFonction", query = "SELECT m FROM Menu m WHERE m.menuFonction = :menuFonction")})
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "menu_id")
    private Integer menuId;
    @Basic(optional = false)
    @Column(name = "menu_libelle")
    private String menuLibelle;
    @Basic(optional = false)
    @Column(name = "menu_ordre")
    private int menuOrdre;
    @Column(name = "menu_fonction")
    private String menuFonction;
    @JoinTable(name = "menuprofil", joinColumns = {
        @JoinColumn(name = "menu_id", referencedColumnName = "menu_id")}, inverseJoinColumns = {
        @JoinColumn(name = "profil_id", referencedColumnName = "profil_id")})
    @ManyToMany
    private Collection<Profil> profilCollection;
    @OneToMany(mappedBy = "menuPereId")
    private Collection<Menu> menuCollection;
    @JoinColumn(name = "menu_pere_id", referencedColumnName = "menu_id")
    @ManyToOne
    private Menu menuPereId;

    public Menu() {
    }

    public Menu(Integer menuId) {
        this.menuId = menuId;
    }

    public Menu(Integer menuId, String menuLibelle, int menuOrdre) {
        this.menuId = menuId;
        this.menuLibelle = menuLibelle;
        this.menuOrdre = menuOrdre;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getMenuLibelle() {
        return menuLibelle;
    }

    public void setMenuLibelle(String menuLibelle) {
        this.menuLibelle = menuLibelle;
    }

    public int getMenuOrdre() {
        return menuOrdre;
    }

    public void setMenuOrdre(int menuOrdre) {
        this.menuOrdre = menuOrdre;
    }

    public String getMenuFonction() {
        return menuFonction;
    }

    public void setMenuFonction(String menuFonction) {
        this.menuFonction = menuFonction;
    }

    @XmlTransient
    public Collection<Profil> getProfilCollection() {
        return profilCollection;
    }

    public void setProfilCollection(Collection<Profil> profilCollection) {
        this.profilCollection = profilCollection;
    }

    @XmlTransient
    public Collection<Menu> getMenuCollection() {
        return menuCollection;
    }

    public void setMenuCollection(Collection<Menu> menuCollection) {
        this.menuCollection = menuCollection;
    }

    public Menu getMenuPereId() {
        return menuPereId;
    }

    public void setMenuPereId(Menu menuPereId) {
        this.menuPereId = menuPereId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (menuId != null ? menuId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Menu)) {
            return false;
        }
        Menu other = (Menu) object;
        if ((this.menuId == null && other.menuId != null) || (this.menuId != null && !this.menuId.equals(other.menuId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Menu[ menuId=" + menuId + " ]";
    }
    
}
