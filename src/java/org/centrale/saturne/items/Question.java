/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "question")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Question.findAll", query = "SELECT q FROM Question q")
    , @NamedQuery(name = "Question.findByQuestionId", query = "SELECT q FROM Question q WHERE q.questionId = :questionId")
    , @NamedQuery(name = "Question.findByQuestionTexte", query = "SELECT q FROM Question q WHERE q.questionTexte = :questionTexte")
    , @NamedQuery(name = "Question.findByQuestionValide", query = "SELECT q FROM Question q WHERE q.questionValide = :questionValide")
    , @NamedQuery(name = "Question.findByQuestionSolutiontexte", query = "SELECT q FROM Question q WHERE q.questionSolutiontexte = :questionSolutiontexte")
    , @NamedQuery(name = "Question.findByQuestionSolutionentier", query = "SELECT q FROM Question q WHERE q.questionSolutionentier = :questionSolutionentier")
    , @NamedQuery(name = "Question.findByQuestionSolutionreel", query = "SELECT q FROM Question q WHERE q.questionSolutionreel = :questionSolutionreel")})
public class Question extends Item implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "question_id")
    private Integer questionId;
    @Column(name = "question_texte")
    private String questionTexte;
    @Basic(optional = false)
    @Column(name = "question_valide")
    private boolean questionValide;
    @Column(name = "question_solutiontexte")
    private String questionSolutiontexte;
    @Column(name = "question_solutionentier")
    private Integer questionSolutionentier;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "question_solutionreel")
    private Double questionSolutionreel;
    @ManyToMany(mappedBy = "questionCollection")
    private Collection<Motcle> motcleCollection;
    @JoinTable(name = "comportequestion", joinColumns = {
        @JoinColumn(name = "question_id", referencedColumnName = "question_id")}, inverseJoinColumns = {
        @JoinColumn(name = "questionnaire_id", referencedColumnName = "questionnaire_id")})
    @ManyToMany
    private Collection<Questionnaire> questionnaireCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "question")
    private Collection<Reponsequestion> reponsequestionCollection;
    @JoinColumn(name = "image_id", referencedColumnName = "image_id")
    @ManyToOne
    private Image imageId;
    @JoinColumn(name = "personne_id", referencedColumnName = "personne_id")
    @ManyToOne(optional = false)
    private Personne personneId;
    @JoinColumn(name = "typeacces_id", referencedColumnName = "typeacces_id")
    @ManyToOne(optional = false)
    private Typeacces typeaccesId;
    @JoinColumn(name = "typequestion_id", referencedColumnName = "typequestion_id")
    @ManyToOne(optional = false)
    private Typequestion typequestionId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "questionId")
    private Collection<Reponsepossible> reponsepossibleCollection;

    public Question() {
    }

    public Question(Integer questionId) {
        this.questionId = questionId;
    }

    public Question(Integer questionId, boolean questionValide) {
        this.questionId = questionId;
        this.questionValide = questionValide;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getQuestionTexte() {
        return questionTexte;
    }

    public void setQuestionTexte(String questionTexte) {
        this.questionTexte = questionTexte;
    }

    public boolean getQuestionValide() {
        return questionValide;
    }

    public void setQuestionValide(boolean questionValide) {
        this.questionValide = questionValide;
    }

    public String getQuestionSolutiontexte() {
        return questionSolutiontexte;
    }

    public void setQuestionSolutiontexte(String questionSolutiontexte) {
        this.questionSolutiontexte = questionSolutiontexte;
    }

    public Integer getQuestionSolutionentier() {
        return questionSolutionentier;
    }

    public void setQuestionSolutionentier(Integer questionSolutionentier) {
        this.questionSolutionentier = questionSolutionentier;
    }

    public Double getQuestionSolutionreel() {
        return questionSolutionreel;
    }

    public void setQuestionSolutionreel(Double questionSolutionreel) {
        this.questionSolutionreel = questionSolutionreel;
    }
    
    // Mots clés ---------------------------------------------------------------
    @XmlTransient
    public Collection<Motcle> getMotcleCollection() {
        return motcleCollection;
    }

    public void setMotcleCollection(Collection<Motcle> motcleCollection) {
        this.motcleCollection = motcleCollection;
    }
    
    public void addMotcle(Motcle motcle) {
        if ((motcle != null) && (!this.getMotcleCollection().contains(motcle))) {
            this.getMotcleCollection().add(motcle);
            motcle.addQuestion(this);
        }
    }

    @XmlTransient
    public Collection<Questionnaire> getQuestionnaireCollection() {
        return questionnaireCollection;
    }

    public void setQuestionnaireCollection(Collection<Questionnaire> questionnaireCollection) {
        this.questionnaireCollection = questionnaireCollection;
    }

    @XmlTransient
    public Collection<Reponsequestion> getReponsequestionCollection() {
        return reponsequestionCollection;
    }

    public void setReponsequestionCollection(Collection<Reponsequestion> reponsequestionCollection) {
        this.reponsequestionCollection = reponsequestionCollection;
    }

    public Image getImageId() {
        return imageId;
    }

    public void setImageId(Image imageId) {
        this.imageId = imageId;
    }

    public Personne getPersonneId() {
        return personneId;
    }

    public void setPersonneId(Personne personneId) {
        this.personneId = personneId;
    }

    public Typeacces getTypeaccesId() {
        return typeaccesId;
    }

    public void setTypeaccesId(Typeacces typeaccesId) {
        this.typeaccesId = typeaccesId;
    }

    public Typequestion getTypequestionId() {
        return typequestionId;
    }

    public void setTypequestionId(Typequestion typequestionId) {
        this.typequestionId = typequestionId;
    }
    
    // Réponses ----------------------------------------------------------------
    @XmlTransient
    public Collection<Reponsepossible> getReponsepossibleCollection() {
        return reponsepossibleCollection;
    }

    public void setReponsepossibleCollection(Collection<Reponsepossible> reponsepossibleCollection) {
        this.reponsepossibleCollection = reponsepossibleCollection;
    }
    
    public void addReponsepossible(Reponsepossible aReponsepossible) {
        if ((aReponsepossible != null) && (!this.getReponsepossibleCollection().contains(aReponsepossible))) {
            this.getReponsepossibleCollection().add(aReponsepossible);
            //aReponsepossible.setQuestionId(this);
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (questionId != null ? questionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Question)) {
            return false;
        }
        Question other = (Question) object;
        if ((this.questionId == null && other.questionId != null) || (this.questionId != null && !this.questionId.equals(other.questionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Question[ questionId=" + questionId + " ]";
    }
    
}
