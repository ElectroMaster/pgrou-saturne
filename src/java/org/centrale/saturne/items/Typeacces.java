/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.items;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author antoinehurard
 */
@Entity
@Table(name = "typeacces")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Typeacces.findAll", query = "SELECT t FROM Typeacces t")
    , @NamedQuery(name = "Typeacces.findByTypeaccesId", query = "SELECT t FROM Typeacces t WHERE t.typeaccesId = :typeaccesId")
    , @NamedQuery(name = "Typeacces.findByTypeaccesLibelle", query = "SELECT t FROM Typeacces t WHERE t.typeaccesLibelle = :typeaccesLibelle")})
public class Typeacces extends Item implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "typeacces_id")
    private Integer typeaccesId;
    @Basic(optional = false)
    @Column(name = "typeacces_libelle")
    private String typeaccesLibelle;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "typeaccesId")
    private Collection<Questionnaire> questionnaireCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "typeaccesId")
    private Collection<Question> questionCollection;

    public Typeacces() {
    }

    public Typeacces(Integer typeaccesId) {
        this.typeaccesId = typeaccesId;
    }

    public Typeacces(Integer typeaccesId, String typeaccesLibelle) {
        this.typeaccesId = typeaccesId;
        this.typeaccesLibelle = typeaccesLibelle;
    }

    public Integer getTypeaccesId() {
        return typeaccesId;
    }

    public void setTypeaccesId(Integer typeaccesId) {
        this.typeaccesId = typeaccesId;
    }

    public String getTypeaccesLibelle() {
        return typeaccesLibelle;
    }

    public void setTypeaccesLibelle(String typeaccesLibelle) {
        this.typeaccesLibelle = typeaccesLibelle;
    }

    @XmlTransient
    public Collection<Questionnaire> getQuestionnaireCollection() {
        return questionnaireCollection;
    }

    public void setQuestionnaireCollection(Collection<Questionnaire> questionnaireCollection) {
        this.questionnaireCollection = questionnaireCollection;
    }

    @XmlTransient
    public Collection<Question> getQuestionCollection() {
        return questionCollection;
    }

    public void setQuestionCollection(Collection<Question> questionCollection) {
        this.questionCollection = questionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (typeaccesId != null ? typeaccesId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Typeacces)) {
            return false;
        }
        Typeacces other = (Typeacces) object;
        if ((this.typeaccesId == null && other.typeaccesId != null) || (this.typeaccesId != null && !this.typeaccesId.equals(other.typeaccesId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.centrale.saturne.items.Typeacces[ typeaccesId=" + typeaccesId + " ]";
    }
    
    /*-----------------------------------------------------------------*/
    public static Typeacces getItemById(int id) {
        return (Typeacces) (getItemById(Typeacces.class, id));
    }
    
}
