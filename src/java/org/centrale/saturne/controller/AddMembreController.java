/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.controller;

import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.saturne.dao.DAOFactory;
import org.centrale.saturne.dao.GroupeDAO;
import org.centrale.saturne.dao.PersonneDAO;
import org.centrale.saturne.items.Groupe;
import org.centrale.saturne.items.Personne;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author thibault
 */
public class AddMembreController {
    
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView handleGET(HttpServletRequest request,
            HttpServletResponse response){
         return new ModelAndView("membreGroupeList");
        
    }
    /**
     * Create group
     * @param grp
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView handlePOST(HttpServletRequest request,
            HttpServletResponse response){
        String connexionId = request.getParameter("connexionId");
        String personneIdStr = request.getParameter("personneId");
        Integer prsid = Integer.parseInt(personneIdStr); 
        PersonneDAO personneDAO = DAOFactory.getPersonneDAO();
        String idStr = request.getParameter("groupeId");
        Integer id = Integer.parseInt(idStr);        
        GroupeDAO groupeDAO = DAOFactory.getGroupeDAO();
        Groupe grp = groupeDAO.findById(id);
        
        if(prsid!=-1){
            Personne prs = personneDAO.findById(prsid);        

            prs.addGroupe(grp);
            grp.addPersonne(prs);
            groupeDAO.addMembre(id,prsid);
        }
        
        
        ModelAndView returned = new ModelAndView("membreGroupeList");
        Collection<Personne> personnes = personneDAO.getAll();
        returned.addObject("personnes", personnes);
        returned.addObject("group", grp);
        returned.addObject("manager", "true");
        returned.addObject("connexionId", connexionId);
        returned.addObject("personneCollection",grp.getPersonneCollection());
        return returned;
    }
}
