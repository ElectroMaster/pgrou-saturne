//<<<<<<< HEAD
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.controller;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.saturne.dao.DAOFactory;
import org.centrale.saturne.dao.EvaluationDAO;
import org.centrale.saturne.dao.PersonneDAO;
import org.centrale.saturne.dao.ProfilDAO;

import org.centrale.saturne.items.Profil;
import org.centrale.saturne.dao.GroupeDAO;
import org.centrale.saturne.dao.ModeevaluationDAO;
import org.centrale.saturne.dao.MotcleDAO;
import org.centrale.saturne.dao.PersonneDAO;
import org.centrale.saturne.dao.QuestionDAO;
import org.centrale.saturne.dao.QuestionnaireDAO;
import org.centrale.saturne.dao.TypeaccesDAO;
import org.centrale.saturne.dao.TypequestionnaireDAO;
import org.centrale.saturne.items.Connexion;
import org.centrale.saturne.items.Evaluation;
import org.centrale.saturne.items.Groupe;
import org.centrale.saturne.items.ItemManager;
import org.centrale.saturne.items.Modeevaluation;
import org.centrale.saturne.items.Motcle;
import org.centrale.saturne.items.Personne;
import org.centrale.saturne.items.Profil;
import org.centrale.saturne.items.Question;
import org.centrale.saturne.items.Questionnaire;
import org.centrale.saturne.items.Typeacces;
import org.centrale.saturne.items.Typequestion;
import org.centrale.saturne.items.Typequestionnaire;
import org.centrale.saturne.utilities.Utilities;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author exia
 */
@Controller
public class AJAXController {

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView handlePOST(HttpServletRequest request,
            HttpServletResponse response) {
        ModelAndView returned = null;
        String action = request.getParameter("action");
        JSONObject json = new JSONObject();
        if (action != null) {
            Class[] cArg = new Class[3];
            cArg[0] = JSONObject.class;
            cArg[1] = HttpServletRequest.class;
            cArg[2] = ModelAndView.class;

            Class myClass = this.getClass();

            try {
                boolean canInvoke = false;
                // method name must be same with action name
                Method aMethod = myClass.getDeclaredMethod(action, cArg);

                AJAXMethodCallable theAnnotation = aMethod.getAnnotation(AJAXMethodCallable.class);
                // make sure the method is used for ajax action
                if (theAnnotation != null) {
                    returned = new ModelAndView("ajax");
                    canInvoke = true;
                }

                if (canInvoke) {
                    aMethod.invoke(this, json, request, returned);
                }
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(AJAXController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SecurityException ex) {
                Logger.getLogger(AJAXController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(AJAXController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(AJAXController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(AJAXController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        returned.addObject("theResponse", json.toString());
        return returned;
    }

    @AJAXMethodCallable
    private void deleteQuestionnaire(JSONObject json, HttpServletRequest request,
            ModelAndView returned) {
        String idSTR = request.getParameter("id");
        int id = Integer.parseInt(idSTR);

        QuestionnaireDAO questionnaireDAO = DAOFactory.getQuestionnaireDAO();
        Questionnaire questionnaire = questionnaireDAO.findById(id);
        questionnaireDAO.remove(questionnaire);
    }

    @AJAXMethodCallable
    private void addKeyword(JSONObject json, HttpServletRequest request,
            ModelAndView returned){
        String libelleSTR = request.getParameter("libelle");

        Motcle aMotcle = new Motcle();
        aMotcle.setMotcleLibelle(libelleSTR);
        ItemManager.create(aMotcle);
    }

    @AJAXMethodCallable
    private void deleteKeyword(JSONObject json, HttpServletRequest request,
            ModelAndView returned){
        String idSTR = request.getParameter("id");
        int id = Integer.parseInt(idSTR);

        MotcleDAO motcleDAO = DAOFactory.getMotcleDAO();
        Motcle motcle = motcleDAO.findById(id);
        motcleDAO.remove(motcle);
    }

    @AJAXMethodCallable
    private void editKeyword(JSONObject json,
            HttpServletRequest request, ModelAndView returned) {
        String rawdata = request.getParameter("data");
        System.out.println(rawdata);

        JSONObject data = new JSONObject(rawdata);
        Motcle aMotcle = new Motcle();
        aMotcle.setMotcleLibelle(data.getString("motcleLibelle"));


        MotcleDAO motcleDAO = DAOFactory.getMotcleDAO();

        String idSTR = request.getParameter("id");
        Integer id = Integer.parseInt(idSTR);

        motcleDAO.update(id, aMotcle);
    }

    @AJAXMethodCallable
    private void deleteQuestion(JSONObject json, HttpServletRequest request,
            ModelAndView returned) {
        String idSTR = request.getParameter("id");
        int id = Integer.parseInt(idSTR);

        QuestionDAO questionDAO = DAOFactory.getQuestionDAO();
        Question question = questionDAO.findById(id);
        questionDAO.remove(question);
    }

    @AJAXMethodCallable
    private void obtainTypequestionnaireAndTypeAcces(JSONObject json,
            HttpServletRequest resquest, ModelAndView returned) {
        TypequestionnaireDAO typequestionnaireDAO = DAOFactory.getTypequestionnaireDAO();
        Collection<Typequestionnaire> tqaireList = typequestionnaireDAO.getAll();

        JSONArray data = new JSONArray();

        HashMap<Integer, String> hMap = new HashMap<Integer, String>();
        for (Typequestionnaire obj : tqaireList) {
            JSONObject ajson = new JSONObject();
            ajson.put("id", obj.getTypequestionnaireId());
            ajson.put("libelle", obj.getTypequestionnaireLibelle());
            data.put(ajson);
        }
        json.put("typeQuestionnaire", data);
        data = new JSONArray();

        TypeaccesDAO typeaccesDAO = DAOFactory.getTypeaccesDAO();
        Collection<Typeacces> typeAccesList = typeaccesDAO.getAll();
        HashMap<Integer, String> hMap2 = new HashMap<Integer, String>();
        for (Typeacces obj : typeAccesList) {
            JSONObject ajson2 = new JSONObject();
            ajson2.put("id", obj.getTypeaccesId());
            ajson2.put("libelle", obj.getTypeaccesLibelle());
            data.put(ajson2);
        }

        json.put("typeAcces", data);
    }

    @AJAXMethodCallable
    private void editQuestionnaire(JSONObject json,
            HttpServletRequest request, ModelAndView returned) {
        String rawdata = request.getParameter("data");
        System.out.println(rawdata);

        JSONObject data = new JSONObject(rawdata);
        Questionnaire aQuestionnaire = new Questionnaire();
        aQuestionnaire.setQuestionnaireLibelle(data.getString("questionnaireLibelle"));

        String duree = data.getString("questionnaireDuree");

        Date dureeT = Utilities.isTime(duree);
        if (dureeT == null) {
            // if not hh:mm:ss or hh:mm or hh, we set a time
            dureeT = Utilities.isTime("00:05");
        }
        aQuestionnaire.setQuestionnaireDuree(dureeT);
//        // TODO: add createDefault for Typeaccess
        int typeId = Integer.parseInt(data.getString("typeQuestionnaire"));
        aQuestionnaire.setTypequestionnaireId(Typequestionnaire.getItemById(typeId));

        Integer level = Integer.parseInt(data.getString("typeAcces"));
        aQuestionnaire.setTypeaccesId(Typeacces.getItemById(level));
        aQuestionnaire.setQuestionnaireDescription(data.getString("questionnaireDescription"));
//        aQuestionnaire.setPersonneId(data.getString("personneNom"));
        QuestionnaireDAO questDAO = DAOFactory.getQuestionnaireDAO();

        String idSTR = request.getParameter("id");
        Integer id = Integer.parseInt(idSTR);

        questDAO.update(id, aQuestionnaire);
    }

        @AJAXMethodCallable
    private void editPersonne(JSONObject json,
            HttpServletRequest request, ModelAndView returned) {
        String rawdata = request.getParameter("data");
        System.out.println(rawdata);

        JSONObject data = new JSONObject(rawdata);
        Personne aPersonne = new Personne();
        aPersonne.setPersonneNom(data.getString("personneNom"));
        aPersonne.setPersonnePrenom(data.getString("personnePrenom"));
        aPersonne.setPersonneUid(data.getString("personneUid"));
        aPersonne.setPersonneEmail(data.getString("personneEmail"));

//        // TODO: add createDefault for Typeaccess
        int profilId = Integer.parseInt(data.getString("profil"));
        aPersonne.setProfilId(Profil.getItemById(profilId));

        PersonneDAO personneDAO = DAOFactory.getPersonneDAO();

        String idSTR = request.getParameter("id");
        Integer id = Integer.parseInt(idSTR);

        personneDAO.update(id, aPersonne);
    }

    @AJAXMethodCallable
    private void addUser(JSONObject json, HttpServletRequest request,
            ModelAndView returned) {

        //action is addItem
        // get data
        String Nom = request.getParameter("Nom");
        String Prenom = request.getParameter("Prenom");
        String email = request.getParameter("email");
        String Login = request.getParameter("Login");
        String password = request.getParameter("password");
        int ProfilId = Integer.parseInt(request.getParameter("roleId"));
        ProfilDAO profilDAO = DAOFactory.getProfilDAO();
        Profil profil = profilDAO.findById(ProfilId);

        //verify if login already exist
        PersonneDAO personneDAO = DAOFactory.getPersonneDAO();
        Personne personne = personneDAO.findByUid(Login);
        //add item if possible
        if (personne == null) {
            json.put("loginExist", 0);
            personne = personneDAO.create(Nom, Prenom, email, Login, password, profil);
        } else {
            json.put("loginExist", 1);
        }
    }

    @AJAXMethodCallable
    private void deleteGroupe(JSONObject json, HttpServletRequest request,
            ModelAndView returned) {
        String idSTR = request.getParameter("id");
        int id = Integer.parseInt(idSTR);

        GroupeDAO groupeDAO = DAOFactory.getGroupeDAO();
        Groupe grp = groupeDAO.findById(id);
        groupeDAO.remove(grp);
    }

    @AJAXMethodCallable
    private void deletePersonne(JSONObject json, HttpServletRequest request,
            ModelAndView returned) {
        String groupeIdSTR = request.getParameter("groupeId");
        Integer grpid = Integer.parseInt(groupeIdSTR);
        String personneIdSTR = request.getParameter("personneId");
        Integer prsid = Integer.parseInt(personneIdSTR);

        PersonneDAO personneDAO = DAOFactory.getPersonneDAO();
        Personne prs = personneDAO.findById(prsid);
        GroupeDAO groupeDAO = DAOFactory.getGroupeDAO();
        Groupe grp = groupeDAO.findById(grpid);

        prs.removeGroupe(grp);
        grp.removePersonne(prs);
        groupeDAO.removeMembre(grpid, prsid);

    }

    @AJAXMethodCallable
    private void deleteUser (JSONObject json, HttpServletRequest request,
            ModelAndView returned) {
        String idSTR = request.getParameter("id");
        int id = Integer.parseInt(idSTR);

        PersonneDAO personneDAO = DAOFactory.getPersonneDAO();
        Personne personne = personneDAO.findById(id);
        personneDAO.remove(personne);
    }

        @AJAXMethodCallable
    private void obtainProfil(JSONObject json,
            HttpServletRequest resquest, ModelAndView returned) {
        ProfilDAO profilDAO = DAOFactory.getProfilDAO();
        Collection<Profil> profilList = profilDAO.getAll();

        JSONArray data = new JSONArray();

        HashMap<Integer, String> hMap = new HashMap<Integer, String>();
        for (Profil obj : profilList) {
            JSONObject ajson = new JSONObject();
            ajson.put("id", obj.getProfilId());
            ajson.put("libelle", obj.getProfilLibelle());
            data.put(ajson);
        }
        json.put("profil", data);
    }

    @AJAXMethodCallable
    private void activateGroupe(JSONObject json, HttpServletRequest request,
            ModelAndView returned) {
        String idSTR = request.getParameter("groupeId");
        int grpid = Integer.parseInt(idSTR);

        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();

        GroupeDAO groupeDAO = DAOFactory.getGroupeDAO();
        Groupe grp = groupeDAO.findById(grpid);
        boolean valide = grp.getGroupeValide();
        grp.setGroupeValide(!valide);
        json.put("valide", valide);

        em.flush();
        transaction.commit();

    }

    @AJAXMethodCallable
    private void deleteSession(JSONObject json, HttpServletRequest request,
            ModelAndView returned) {
        String idSTR = request.getParameter("id");
        int id = Integer.parseInt(idSTR);

        EvaluationDAO evaluationDAO = DAOFactory.getEvaluationDAO();
        Evaluation evaluation = evaluationDAO.findById(id);
        evaluationDAO.remove(evaluation);
    }

    @AJAXMethodCallable
    private void obtainQuestionnaireAndModeevaluation(JSONObject json,
            HttpServletRequest resquest, ModelAndView returned) {
        QuestionnaireDAO questionnaireDAO = DAOFactory.getQuestionnaireDAO();
        Collection<Questionnaire> questionnaireList = questionnaireDAO.getAll();
        JSONArray data = new JSONArray();
        HashMap<Integer, String> hMap = new HashMap<Integer, String>();
        for (Questionnaire obj : questionnaireList) {
            JSONObject ajson = new JSONObject();
            ajson.put("id", obj.getQuestionnaireId());
            ajson.put("libelle", obj.getQuestionnaireLibelle());
            data.put(ajson);
        }
        json.put("questionnaire", data);

        data = new JSONArray();
        ModeevaluationDAO modeevaluationDAO = DAOFactory.getModeevaluationDAO();
        Collection<Modeevaluation> modeevaluationList = modeevaluationDAO.getAll();
        HashMap<Integer, String> hMap2 = new HashMap<Integer, String>();
        for (Modeevaluation obj : modeevaluationList) {
            JSONObject ajson2 = new JSONObject();
            ajson2.put("id", obj.getModeevaluationId());
            ajson2.put("libelle", obj.getModeevaluationLibelle());
            data.put(ajson2);
        }
        json.put("modeevaluation", data);
    }

    @AJAXMethodCallable
    private void editSession(JSONObject json,
            HttpServletRequest request, ModelAndView returned) {
        String rawdata = request.getParameter("data");
        System.out.println(rawdata);

        JSONObject data = new JSONObject(rawdata);
        Evaluation evaluation = new Evaluation();

        Integer questId = Integer.parseInt(data.getString("questionnaire"));
        evaluation.setQuestionnaireId(Questionnaire.getItemById(questId));

        String dateS = data.getString("date");
        Date date = Utilities.isDate(dateS);
        evaluation.setEvaluationDate(date);

        String debut = data.getString("evaluationDebut");
        Date debutT = Utilities.isTime(debut);
        if (debutT == null) {
            // if not hh:mm:ss or hh:mm or hh, we set a time
            debutT = Utilities.isTime("00:05");
        }
        evaluation.setEvaluationDepart(debutT);

        String duree = data.getString("evaluationDuree");
        Date dureeT = Utilities.isTime(duree);
        if (dureeT == null) {
            // if not hh:mm:ss or hh:mm or hh, we set a time
            dureeT = Utilities.isTime("00:05");
        }
        evaluation.setEvaluationDuree(dureeT);

        Integer modeId = Integer.parseInt(data.getString("modeevaluation"));
        evaluation.setModeevaluationId(Modeevaluation.getItemById(modeId));


        EvaluationDAO evalDAO = DAOFactory.getEvaluationDAO();

        String idSTR = request.getParameter("id");
        Integer id = Integer.parseInt(idSTR);

        evalDAO.update(id, evaluation);
    }
}
//=======
///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package org.centrale.saturne.controller;
//
//import java.util.Collection;
//import java.util.List;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import org.centrale.saturne.dao.DAOFactory;
//import org.centrale.saturne.dao.PersonneDAO;
//import org.centrale.saturne.dao.ProfilDAO;
//import org.centrale.saturne.items.Personne;
//import org.centrale.saturne.items.Profil;
//import org.json.JSONArray;
//import org.json.JSONObject;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.servlet.ModelAndView;
//
///**
// *
// * @author dimitri
// */
//@Controller
//public class AJAXController {
//
//    /**
//     * Create controller
//     */
//    public AJAXController() {
//    }
//
//    /**
//     * Map a GET request : use empty response. POST should ALWAYS be used with
//     * AJAX
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping(method = RequestMethod.POST)
//    public ModelAndView handleAJAX(HttpServletRequest request, HttpServletResponse response) {
//        //Get action
//        String action = request.getParameter("action");
//
//        JSONObject json = new JSONObject();
//        if (action != null) {
//            if (action.equals("addUser")) {
//                // action is addItem
//                // get data
//                String Nom = request.getParameter("Nom");
//                String Prenom = request.getParameter("Prenom");
//                String email = request.getParameter("email");
//                String Login = request.getParameter("Login");
//                String password = request.getParameter("password");
//                int ProfilId = Integer.parseInt(request.getParameter("roleId"));
//                ProfilDAO profilDAO = DAOFactory.getProfilDAO();
//                Profil profil = profilDAO.findById(ProfilId);
//
//                //add item
//                PersonneDAO personneDAO = DAOFactory.getPersonneDAO();
//                Personne personne = personneDAO.create(Nom, Prenom, email, Login, password, profil);
//
//
//            } else if (action.equals("findAllUser")){
//                JSONArray data = new JSONArray();
//                PersonneDAO personneDAO = DAOFactory.getPersonneDAO();
//                Collection<Personne> listPersonnes = personneDAO.getAll();
//
//        for (Personne personne : listPersonnes){
//            JSONObject anPerson= new JSONObject();
//            anPerson.put("id", personne.getId());
//            anPerson.put("Email", personne.getPersonneEmail());
//            anPerson.put("Nom", personne.getPersonneNom());
//            anPerson.put("Prenom", personne.getPersonnePrenom());
//            anPerson.put("Login", personne.getPersonneUid());
//            anPerson.put("Password", personne.getPersonnePassword());
//            anPerson.put("ProfilId", personne.getProfilId());
//            data.put(anPerson);
//        }
//        json.put("data", data);
//            }
//
//        }
//        // send response back
//        ModelAndView result = new ModelAndView("ajax");
//        result.addObject("theResponse", json.toString());
//        return result;
//    }
//}
//
//>>>>>>> dimitriWork
