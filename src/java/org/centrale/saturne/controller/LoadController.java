/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.controller;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.saturne.dao.DAOFactory;
import org.centrale.saturne.dao.EvaluationDAO;
import org.centrale.saturne.dao.GroupeDAO;
import org.centrale.saturne.dao.ModeevaluationDAO;
import org.centrale.saturne.dao.MotcleDAO;
import org.centrale.saturne.dao.PersonneDAO;
import org.centrale.saturne.dao.QuestionDAO;
import org.centrale.saturne.dao.QuestionnaireDAO;
import org.centrale.saturne.dao.TypeaccesDAO;
import org.centrale.saturne.dao.TypequestionDAO;
import org.centrale.saturne.dao.TypequestionnaireDAO;
import org.centrale.saturne.items.Connexion;
import org.centrale.saturne.items.Evaluation;
import org.centrale.saturne.items.Groupe;
import org.centrale.saturne.items.Modeevaluation;
import org.centrale.saturne.items.Motcle;
import org.centrale.saturne.items.Personne;
import org.centrale.saturne.items.Question;
import org.centrale.saturne.items.Questionnaire;
import org.centrale.saturne.utilities.Utilities;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author exia
 */
@Controller
public class LoadController {
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView handleGET(HttpServletRequest request,
            HttpServletResponse response){
        ModelAndView returned = null;
        // Check if user is connected, (in other word if url looks like load.do?id=..)
        Connexion aConnexion = IndexController.check(request);  
        String source = Utilities.getRequestString(request, "page");
        if((aConnexion != null)&&(source != null)&&(source != "")){
            // ensure url looks like load.do?id=..&source=..
            
            // call method according to source name.
            Class[] cArg = new Class[3];
            cArg[0] = Connexion.class;
            cArg[1] = HttpServletRequest.class;
            cArg[2] = ModelAndView.class;
            
            Class myClass = this.getClass();
            
            try{
                boolean canInvoke = false;
                // method name must be same with source name
                Method aMethod = myClass.getDeclaredMethod(source, cArg);
                
                PageCallable theAnnotation = aMethod.getAnnotation(PageCallable.class);
                // make sure the method is used for a page.
                if(theAnnotation !=null){
                    returned = new ModelAndView(source);
                    returned.addObject("connexionId", aConnexion.getConnexionCode());
                    canInvoke = true;
                }
                
                // TODO: judge the user has the right to use this page 
                
                if(canInvoke){
                    aMethod.invoke(this, aConnexion, request, returned);
                }
                
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(LoadController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SecurityException ex) {
                Logger.getLogger(LoadController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(LoadController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(LoadController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(LoadController.class.getName()).log(Level.SEVERE, null, ex);
            }

            
        } else {
            returned = new ModelAndView("empty");
        }
        
        if (returned == null){
            returned = new ModelAndView("empty");
        }

        return returned;
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView handlePOST(HttpServletRequest request,
            HttpServletResponse response){
        ModelAndView returned = null;
        returned = new ModelAndView("empty");

        return returned;
    }
    
    
    @PageCallable
    private void empty(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        
    }
    
    @PageCallable 
    private void welcome(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        
    }
    
    @PageCallable
    private void consulterQuestionnaireListe(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        QuestionnaireDAO questionnnaireDAO = DAOFactory.getQuestionnaireDAO();
        Collection<Questionnaire> questionnaireList = questionnnaireDAO.getAll();
        returned.addObject("questionnaireList", questionnaireList);
    }
    
    @PageCallable 
    private void createQuestion(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        TypequestionDAO typequestionDAO = DAOFactory.getTypequestionDAO();
        TypeaccesDAO typeaccesDAO = DAOFactory.getTypeaccesDAO();
        MotcleDAO motcleDAO = DAOFactory.getMotcleDAO();
        
        returned.addObject("listTypequestion",typequestionDAO.getAll());
        returned.addObject("listTypeacces",typeaccesDAO.getAll());
        returned.addObject("listMotcle",motcleDAO.getAll());
    }
    
    @PageCallable
    private void consulterQuestionListe(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        QuestionDAO questionDAO = DAOFactory.getQuestionDAO();
        Collection<Question> questionList = questionDAO.getAll();
        returned.addObject("questionList", questionList);
    }
    
    @PageCallable
    private void managerMotcleListe(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        MotcleDAO motcleDAO = DAOFactory.getMotcleDAO();
        Collection<Motcle> motcleList = motcleDAO.getAll();
        returned.addObject("motcleList", motcleList);
    }
    

    @PageCallable
    private void consulterGroupListe(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        GroupeDAO groupeDAO = DAOFactory.getGroupeDAO();
        String mot = request.getParameter("Libelle");
        Collection<Groupe> groupeList=null;
        if(mot==null || mot.equals("")){
            groupeList = groupeDAO.getAll();
        }
        else{
            groupeList = groupeDAO.getBySearch(mot);
        }
        returned.addObject("groupeList", groupeList);
    }
    
    @PageCallable
    private void membreGroupeList(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        GroupeDAO groupeDAO = DAOFactory.getGroupeDAO();
        String grpIdStr = Utilities.getRequestString(request, "groupeId");
        String manager = Utilities.getRequestString(request, "manager");
        String mot = request.getParameter("nom");
        Integer grpId = Integer.parseInt(grpIdStr);
        Groupe group = groupeDAO.findById(grpId);
        
        PersonneDAO personneDAO = DAOFactory.getPersonneDAO();
        Collection<Personne> personnes = personneDAO.getAll();
        
        Collection<Personne> personneGroupe = group.getPersonneCollection();
        ArrayList<Personne> personneCollection = new ArrayList<>();

        for( Personne prs : personneGroupe){
            if( mot==null || prs.getPersonneNom().toLowerCase().contains(mot.toLowerCase())
                    || prs.getPersonnePrenom().toLowerCase().contains(mot.toLowerCase())){
                personneCollection.add(prs);
            }
        }
        
        
        returned.addObject("personnes", personnes);
        returned.addObject("group", group);
        returned.addObject("manager", manager);
        returned.addObject("personneCollection", personneCollection);
        
    }
    
    @PageCallable
    private void consulterUserListe(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        PersonneDAO personneDAO = DAOFactory.getPersonneDAO();
        Collection<Personne> personneList = personneDAO.getAll();
        returned.addObject("personneList", personneList);
    }
    
    @PageCallable
    private void createGroup(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        
    }
    
    @PageCallable 
    private void createQuestionnaire(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        TypequestionnaireDAO typequestionnaireDAO = DAOFactory.getTypequestionnaireDAO();
        returned.addObject("listTypequestionnaire", typequestionnaireDAO.getAll());
    }
    
    /**
     * deprecated
     * @param aConnexion
     * @param request
     * @param returned 
     */
    @PageCallable 
    private void createQuestionnaire2(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        
    }
    
    @PageCallable
    private void createUser(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        
    }
    
    
    @PageCallable
    private void importerUser(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        
    }
    
    @PageCallable
    private void managerGroupListe(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        GroupeDAO groupeDAO = DAOFactory.getGroupeDAO();
        String mot = request.getParameter("Libelle");
        Collection<Groupe> groupeList=null;
        if(mot==null || mot.equals("")){
            groupeList = groupeDAO.getAll();
        }
        else{
            groupeList = groupeDAO.getBySearch(mot);
        }
        returned.addObject("groupeList", groupeList);
        returned.addObject("mot",mot);
    }
    
    @PageCallable 
    private void managerUserListe(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        PersonneDAO personneDAO = DAOFactory.getPersonneDAO();
        Collection<Personne> userList = personneDAO.getAll();
        returned.addObject("userList", userList);      
    }
    
    @PageCallable
    private void managerQuestionListe(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        QuestionDAO questionDAO = DAOFactory.getQuestionDAO();
        Collection<Question> questionList = questionDAO.getAll();
        returned.addObject("questionList", questionList);
    }
    
    @PageCallable
    private void managerQuestionnaireListe(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        QuestionnaireDAO questionnnaireDAO = DAOFactory.getQuestionnaireDAO();
        Collection<Questionnaire> questionnaireList = questionnnaireDAO.getAll();
        returned.addObject("questionnaireList", questionnaireList);
    }
    
    @PageCallable
    private void modifyGroup(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        
    }
    
    @PageCallable
    private void modifyUser(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        
    }
    @PageCallable
    private void modifyQuestionnaire(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        
        QuestionnaireDAO questionnnaireDAO = DAOFactory.getQuestionnaireDAO();
        Collection<Questionnaire> questionnaireList = questionnnaireDAO.getAll();
        returned.addObject("questionnaireList", questionnaireList);
        
    }
    /**
     * Change only one questionnaire.
     * @param aConnexion
     * @param request
     * @param returned 
     */
    @PageCallable
    private void modifyQuestionnaire2(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        
        QuestionnaireDAO questionnnaireDAO = DAOFactory.getQuestionnaireDAO();
        Collection<Questionnaire> questionnaireList = questionnnaireDAO.getAll();
        returned.addObject("questionnaireList", questionnaireList);
        
    }
    
    /**
     * @param aConnexion
     * @param request
     * @param returned 
     */
    @PageCallable 
    private void createSession(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        QuestionnaireDAO questionnnaireDAO = DAOFactory.getQuestionnaireDAO();
        Collection<Questionnaire> questionnaireList = questionnnaireDAO.getAll();
        ModeevaluationDAO modeevaluationDAO = DAOFactory.getModeevaluationDAO();
        Collection<Modeevaluation> modeevaluationList = modeevaluationDAO.getAll();
        returned.addObject("questionnaireList", questionnaireList);
        returned.addObject("listModeevaluations", modeevaluationList);
        
    }
    
        /**
     * @param aConnexion
     * @param request
     * @param returned 
     */
    @PageCallable 
    private void modifySession(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        
    }
    
    @PageCallable
    private void consulterSessionListe(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
//        QuestionnaireDAO questionnnaireDAO = DAOFactory.getQuestionnaireDAO();
//        Collection<Questionnaire> questionnaireList = questionnnaireDAO.getAll();
        EvaluationDAO evaluationDAO = DAOFactory.getEvaluationDAO();
        Collection<Evaluation> evaluationList = evaluationDAO.getAll();
//        returned.addObject("questionnaireList", questionnaireList);
        returned.addObject("evaluationList", evaluationList);
        
    }
    
    @PageCallable
    private void managerSessionListe(Connexion aConnexion,
            HttpServletRequest request, ModelAndView returned){
        QuestionnaireDAO questionnnaireDAO = DAOFactory.getQuestionnaireDAO();
        Collection<Questionnaire> questionnaireList = questionnnaireDAO.getAll();
        EvaluationDAO evaluationDAO = DAOFactory.getEvaluationDAO();
        Collection<Evaluation> evaluationList = evaluationDAO.getAll();
        returned.addObject("questionnaireList", questionnaireList);
        returned.addObject("evaluationList", evaluationList);
        
    }
    
}
