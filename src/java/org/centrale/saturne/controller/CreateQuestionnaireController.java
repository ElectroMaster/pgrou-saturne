/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.controller;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.saturne.items.Connexion;
import org.centrale.saturne.items.ItemManager;
import org.centrale.saturne.items.Personne;
import org.centrale.saturne.items.Questionnaire;
import org.centrale.saturne.items.Typeacces;
import org.centrale.saturne.items.Typequestionnaire;
import org.centrale.saturne.utilities.Utilities;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author exia
 */
@Controller
public class CreateQuestionnaireController {
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView handleGET(HttpServletRequest request,
            HttpServletResponse response){
        ModelAndView returned = null;
        returned = new ModelAndView("createQuestionnaire");

        return returned;
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView handlePOST(
            @RequestParam("connexionId") String connexionId,
            @RequestParam("nom_questionnaire") String nom_quest,
            @RequestParam("descr_questionnaire") String descr_quest,
            @RequestParam("dureeH") String dureeH,
            @RequestParam("dureeM") String dureeM,
            @RequestParam("visibility_level") int level,
            @RequestParam("type_questionnaire") int typeId,
            HttpServletRequest request, HttpServletResponse response){
        ModelAndView returned = new ModelAndView("createQuestionnaire");
        // ensure there is a connection
        if((connexionId != null) && (!connexionId.isEmpty())){
            Connexion aConnexion = Connexion.getConnexion(connexionId);
            Personne aPersonne = aConnexion.getPersonneId();
            returned = new ModelAndView("createQuestionnaire2");
            Questionnaire aQuestionnaire = new Questionnaire();
            aQuestionnaire.setQuestionnaireLibelle(nom_quest);
            aQuestionnaire.setQuestionnaireDescription(descr_quest);
            String duree = "00:00";
            if ((!dureeH.isEmpty()) && (!dureeM.isEmpty())) {
                duree = dureeH + ":" + dureeM;
            } else if (!dureeH.isEmpty()) {
//                duree = "00:" + dureeM;
                duree = dureeH + ":00";
            } else if (!dureeM.isEmpty()) {
//                duree = dureeH + ":00";
                duree = "00:" + dureeM;
            }
            Date dureeT = Utilities.isTime(duree);
            if (dureeT == null) {
                // if not hh:mm:ss or hh:mm or hh, we set a time 
                dureeT = Utilities.isTime("00:05");
            }
            aQuestionnaire.setQuestionnaireDuree(dureeT);
            // TODO: add createDefault for Typeaccess
            aQuestionnaire.setTypeaccesId(Typeacces.getItemById(level));
            // because we haven't add questions yet, questionnaire is not valide now
            aQuestionnaire.setQuestionnaireValide(false);
            // TODO: add createDefault for Typequesitonnaire
            
            aQuestionnaire.setTypequestionnaireId(Typequestionnaire.getItemById(typeId));
            aQuestionnaire.setPersonneId(aPersonne);
            
            ItemManager.create(aQuestionnaire);
        }


        

        return returned;
    }
}