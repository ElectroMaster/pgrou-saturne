package org.centrale.saturne.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.saturne.items.ItemManager;
import org.centrale.saturne.utilities.Utilities;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author exia
 */
@Controller
public class DisconnectController {
    /**
     * Disconnect -> call login page
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView handleGET(HttpServletRequest request,
            HttpServletResponse response){
        IndexController.check(request);
        String connexionId = Utilities.getRequestString(request, "id");
        if(connexionId != null && !connexionId.isEmpty()){
            ItemManager.cancelConnexion(connexionId);
        }
        // call login page;
        ModelAndView returned = new ModelAndView("index");
        return returned;
    }
    /**
     * Disconnect -> call login page
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView handlePOST(HttpServletRequest request,
            HttpServletResponse response){
        return handleGET(request, response);
    }
}
