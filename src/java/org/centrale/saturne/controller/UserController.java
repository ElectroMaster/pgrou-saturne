/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.controller;

import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.saturne.dao.DAOFactory;
import org.centrale.saturne.dao.PersonneDAO;
import org.centrale.saturne.dao.ProfilDAO;
import org.centrale.saturne.items.Personne;
import org.centrale.saturne.items.Profil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author utilisateur
 */
public class UserController {

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView handleGet(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PersonneDAO personneDAO = DAOFactory.getPersonneDAO();
        ProfilDAO profilDAO = DAOFactory.getProfilDAO();
        Collection<Personne> thePersons = personneDAO.getAll();
        Collection<Profil> theProfils = profilDAO.getAll();

        ModelAndView result = new ModelAndView("managerUserListe");
        result.addObject("thePersons", thePersons);
        result.addObject("theProfils", theProfils);
        return result;
    }
}
