/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.saturne.dao.DAOFactory;
import org.centrale.saturne.dao.GroupeDAO;
import org.centrale.saturne.items.Groupe;
import org.centrale.saturne.items.ItemManager;
import org.centrale.saturne.utilities.Utilities;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author GUO
 */
@Controller
public class CreateGroupController {
    
    /**
     * Create group
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView handleGET(HttpServletRequest request,
            HttpServletResponse response){
         return handleGET(request, response);
        
    }
    /**
     * Create group
     * @param grp
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView handlePOST(@ModelAttribute Groupe grp,HttpServletRequest request,
            HttpServletResponse response){
        grp.setGroupeValide(true);
        GroupeDAO groupeDAO = DAOFactory.getGroupeDAO();
        groupeDAO.create(grp);
        ModelAndView returnedValue = new ModelAndView("createGroup");
        return returnedValue;
    }
    
    
    
}
