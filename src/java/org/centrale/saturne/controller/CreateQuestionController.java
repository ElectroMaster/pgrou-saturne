/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.controller;

import java.util.Collection;
import java.util.LinkedList;
import javax.persistence.EntityTransaction;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.saturne.dao.DAOFactory;
import org.centrale.saturne.dao.MotcleDAO;
import org.centrale.saturne.items.Connexion;
import org.centrale.saturne.items.ItemManager;
import org.centrale.saturne.items.Motcle;
import org.centrale.saturne.items.Personne;
import org.centrale.saturne.items.Question;
import org.centrale.saturne.items.Reponsepossible;
import org.centrale.saturne.items.Typeacces;
import org.centrale.saturne.items.Typequestion;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
/**
 *
 * @author antoinehurard
 */
@Controller
public class CreateQuestionController {
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView handleGET(HttpServletRequest request,
            HttpServletResponse response){
        ModelAndView returned = null;
        returned = new ModelAndView("createQuestion");

        return returned;
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView handlePOST(
            @RequestParam("connexionId") String connexionId,
            @RequestParam("libelle_question") String libelle_question,
            @RequestParam("type_question") int type_question,
            @RequestParam("visibility_level") int level,
            @RequestParam("inputJSON") String inputJSON,
            @RequestParam("reponse_litterale") String reponse_litterale,
            HttpServletRequest request, HttpServletResponse response){
        ModelAndView returned = new ModelAndView("createQuestion");
        // ensure there is a connection
        if((connexionId != null) && (!connexionId.isEmpty())){
            Connexion aConnexion = Connexion.getConnexion(connexionId);
            Personne aPersonne = aConnexion.getPersonneId();
            //returned = new ModelAndView("createQuestion");
            returned = new ModelAndView("empty");
            Question aQuestion = new Question();
            aQuestion.setQuestionTexte(libelle_question);
            aQuestion.setTypeaccesId(Typeacces.getItemById(level));
            aQuestion.setTypequestionId(Typequestion.getItemById(type_question));
            //aQuestion.setQuestionValide(true);
            aQuestion.setPersonneId(aPersonne);
            ItemManager.create(aQuestion);
            
            
            JSONObject jsnobject = new JSONObject(inputJSON);
            EntityTransaction transaction;
            
            JSONArray jsonKeywords = jsnobject.getJSONArray("keywords");
            Collection<Motcle> motcleListe = new LinkedList<>();
            
            for (int i = 0; i < jsonKeywords.length(); i++) {
                JSONObject explrObject = jsonKeywords.getJSONObject(i);
                if(explrObject.getInt("idDatabase") == 0){
                    Motcle motcle = new Motcle();
                    motcle.setMotcleLibelle(explrObject.getString("libelle"));
                    ItemManager.create(motcle);
                    transaction = ItemManager.begin();
                    aQuestion.addMotcle(motcle);
                    ItemManager.commit(transaction);
                }else{
                    MotcleDAO motcleDAO = DAOFactory.getMotcleDAO();
                    Motcle motcle = motcleDAO.findById(Integer.parseInt(explrObject.getString("idDatabase")));
                    transaction = ItemManager.begin();
                    aQuestion.addMotcle(motcle);
                    ItemManager.commit(transaction);
                }
            }
            aQuestion.setMotcleCollection(motcleListe);
            
            if(type_question != 3){
                JSONArray jsonAnswers = jsnobject.getJSONArray("answers");
                Collection<Reponsepossible> reponseListe = new LinkedList<>();
                for (int i = 0; i < jsonAnswers.length(); i++) {
                    JSONObject explrObject = jsonAnswers.getJSONObject(i);
                    Reponsepossible reponsepossible = new Reponsepossible();
                    reponsepossible.setQuestionId(aQuestion);
                    reponsepossible.setReponsepossibleTexte(explrObject.getString("libelle"));
                    reponsepossible.setReponsepossibleEstsolution(explrObject.getBoolean("trueAnswer"));
                    ItemManager.create(reponsepossible);

                    transaction = ItemManager.begin();
                    aQuestion.addReponsepossible(reponsepossible);
                    ItemManager.commit(transaction);
                }
                if(aQuestion.getReponsepossibleCollection().size() > 0){
                   aQuestion.setQuestionValide(true);
                }
            }
            else{
                try {
                    Integer reponseUnique = Integer.parseInt(reponse_litterale);
                    transaction = ItemManager.begin();
                    aQuestion.setQuestionSolutionentier(reponseUnique);
                    aQuestion.setQuestionValide(true);
                    ItemManager.commit(transaction);
                }
                catch (NumberFormatException e) {
                    try{
                        Double reponseUnique = Double.parseDouble(reponse_litterale);
                        transaction = ItemManager.begin();
                        aQuestion.setQuestionSolutionreel(reponseUnique);
                        aQuestion.setQuestionValide(true);
                        ItemManager.commit(transaction);
                    }catch (NumberFormatException e2) {
                        if(!reponse_litterale.equals("")){
                            transaction = ItemManager.begin();
                            aQuestion.setQuestionSolutiontexte(reponse_litterale);
                            aQuestion.setQuestionValide(true);
                            ItemManager.commit(transaction);
                        }
                    }
                }
            }
            
            
        }
        return returned;
    }
}
