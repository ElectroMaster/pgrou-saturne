/*
 * Projet Saturne
 * 
 * Gestion de QCM en ligne
 *
 * Ecole Centrale Nantes - Fevrier 2019
 */
package org.centrale.saturne.controller;

import java.util.Collection;
import javax.persistence.EntityTransaction;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.saturne.items.Connexion;
import org.centrale.saturne.items.ItemManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.validation.BindingResult;
import org.centrale.saturne.items.User;
import org.centrale.saturne.items.Personne;
import org.centrale.saturne.utilities.Utilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;

/**
 *
 * @author antoinehurard
 */
@Controller
public class IndexController{
    
    @Autowired
    private IdentifyValidator validator;
    
    // When application start, it need to check if 
    // there are some default values in database.
    // mainly refer to function: checkDefault()
    private static boolean initialised = false;
    /**
     * Create controller
     */
    public IndexController() {
        validator = new IdentifyValidator();
    }

    /**
     * Get Identification validator
     *
     * @return
     */
    public Validator getValidator() {
        return validator;
    }
    
    /**
     * Set Identification validator
     *
     * @param validator
     */
    public void setValidator(IdentifyValidator validator) {
        this.validator = validator;
    }
    
    
    public static void checkDefault(){
        if(!initialised){
            // reset sequence, if table size is 0, 
            // reinitialize sequence counter from 0
            ItemManager.resetSequences();
            // invoke all createDefault method in package item
            // it will recursively invoke createDefault method in Itemmanager
            ItemManager.createDefault();
            
            ItemManager.removeOldConnexions();
            initialised = true;
        }
    }
    
    /**
     * Check is connection to the app is valid => id is in the database<br/>
     * Also remove old connections.
     *
     * @param request
     * @return
     */
    public static Connexion check(HttpServletRequest request) {
        ItemManager.removeOldConnexions();

        String userId = Utilities.getRequestString(request, "id");
        Connexion aConnexion = Connexion.getConnexion(userId);
        if (aConnexion != null) {
            EntityTransaction transaction = ItemManager.begin();
            aConnexion.expand();
            ItemManager.commit(transaction);
        }
        return aConnexion;
    }
    
    /**
     * GET Request => identification screen
     * 
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView handleGet(HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        // initialize database at begin
//        checkDefault();
        
        ModelAndView result = new ModelAndView("index");
        return result;
    }
    
    /**
     * POST Request => identify user
     *
     * @param anUser
     * @param result
     * @param request
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView identify(@ModelAttribute("User") User anUser, BindingResult result, HttpServletRequest request) {
        ModelAndView returned;
        Personne aPersonne = null;

        check(request);
        if (validator != null) {
            // There is a validator
            validator.validate(anUser, result);
            if (!result.hasErrors()) {
                // Valid connexion
                aPersonne = validator.getPersonne(anUser.getLogin(), anUser.getPassw());
            }
        } else {
            result.reject("Internal error");
        }

        if (aPersonne == null) {
            // Not recognized
            returned = new ModelAndView("index");
            if (result.getErrorCount() == 0) {
                returned.addObject("message", "Connexion refusée");
            } else {
                StringBuilder message = new StringBuilder();
                for (ObjectError msg : result.getAllErrors()) {
                    message.append(msg.getDefaultMessage());
                }
                returned.addObject("message", message.toString());
            }
        } else {
            // Ok can get in
            Connexion newConnexion = new Connexion();

            newConnexion.setConnexionCode();
            newConnexion.setPersonneId(aPersonne);
            newConnexion.setConnexionExpire();
            ItemManager.create(newConnexion);

            //Collection<Menu> theMenusList = aPersonne.getMenuCollection();

            returned = new ModelAndView("menu");
            returned.addObject("connexionId", newConnexion.getConnexionCode());
            //returned.addObject("menusList", theMenusList);
            returned.addObject("nom", aPersonne.getPersonneNom());
            returned.addObject("prenom", aPersonne.getPersonnePrenom());
            returned.addObject("source", "vide");
        }

        return returned;
    }
}