/*
 * Projet Saturne
 * 
 * Gestion de QCM en ligne
 *
 * Ecole Centrale Nantes - Fevrier 2019
 */
package org.centrale.saturne.controller;

import org.centrale.saturne.ldap.LDAPManager;
import org.centrale.saturne.ldap.LDAPUser;
import org.centrale.saturne.items.Personne;
import org.centrale.saturne.items.User;
import org.centrale.saturne.items.ItemManager;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Identification Validator
 *
 * @author antoinehurard
 */
@Component
public class IdentifyValidator implements Validator {

    // Default ADMIN Login and Password for the application
    private static final String DEFAULTLOGIN = "admin";
    private static final String DEFAULTPASSWORD = "admin";

    /**
     * Initialise managers
     */
    private void init() {
        LDAPManager.init();
    }

    /**
     * Create validator
     */
    public IdentifyValidator() {
        init();
    }

    /**
     * Check if a User is sent to validator
     *
     * @param type
     * @return
     */
    @Override
    public boolean supports(Class type) {
        return User.class.isAssignableFrom(type);
    }

    /**
     * Check identity
     *
     * @param o
     * @param errors
     */
    @Override
    public void validate(Object o, Errors errors) {
        User anUser = (User) o;
        String login = anUser.getLogin();
        String passwd = anUser.getPassw();
        if ((login == null) || (login.equals(""))) {
            ValidationUtils.rejectIfEmpty(errors, "login", "login.empty", "Login non défini");
        } else if (passwd == null) {
            ValidationUtils.rejectIfEmpty(errors, "passw", "passw.empty", "Mot de passe non défini");
        } else if ((login.equals(DEFAULTLOGIN)) && (passwd.equals(DEFAULTPASSWORD))) {
            // Pass through
        } else {
            LDAPUser aUser = LDAPManager.searchLDAP(anUser.getLogin(), anUser.getPassw());

            if (aUser != null) {
                // LDAP identified
                if (identifyDatabase(anUser.getLogin()) == null) {
                    // Add him/her to the database
                    addDatabase(aUser);
                }
            } else {
                // Not in LDAP
                if (identifyDatabase(anUser.getLogin(), anUser.getPassw()) == null) {
                    errors.reject("Invalid", "login/password incorrect");
                }
            }
        }
    }

    /**
     * Identify in database
     *
     * @param login
     * @return
     */
    private Personne identifyDatabase(String login) {
        return Personne.getPersonneByUID(login);
    }

    /**
     * Identify in database
     *
     * @param login
     * @param passwd
     * @return
     */
    private Personne identifyDatabase(String login, String passwd) {
        return ItemManager.getPersonneByLoginPassword(login, passwd);
    }

    /**
     * Add LDAP user to database
     *
     * @param aUser
     */
    private void addDatabase(LDAPUser aUser) {
        Personne aPersonne = new Personne();
        aPersonne.setPersonneUid(aUser.getUserUID());
        aPersonne.setPersonneNom(aUser.getUserNom());
        aPersonne.setPersonnePrenom(aUser.getUserPrenom());
        aPersonne.setPersonneEmail(aUser.getUserEmail());
        ItemManager.create(aPersonne);
    }

    /**
     * if identity is Ok, return the Personne
     *
     * @param login
     * @param passwd
     * @return
     */
    public Personne getPersonne(String login, String passwd) {
        if ((login == null) || (login.equals(""))) {
            return null;
        } else if ((login.equals(DEFAULTLOGIN)) && (passwd.equals(DEFAULTPASSWORD))) {
            return Personne.getItemById(1);
        } else {
            return Personne.getPersonneByUID(login);
        }
    }

}
