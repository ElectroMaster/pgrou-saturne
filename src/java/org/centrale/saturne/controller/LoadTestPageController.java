/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.saturne.items.Questionnaire;
import org.centrale.saturne.utilities.Utilities;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author exia
 */
@Controller
public class LoadTestPageController {
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView handleGET(HttpServletRequest request,
            HttpServletResponse response){
        ModelAndView returned = null;
        returned = new ModelAndView("empty");

        return returned;
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView handlePOST(
            @RequestParam("nom_questionnaire") String nom_quest,
            @RequestParam("descr_questionnaire") String descr_quest,
            @RequestParam("dureeH") String dureeH,
            @RequestParam("dureeM") String dureeM,
            @RequestParam("visibility_level") int level){
        ModelAndView returned = null;
        returned = new ModelAndView("empty");


        return returned;
    }
}