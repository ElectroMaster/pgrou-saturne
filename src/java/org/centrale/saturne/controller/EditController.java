/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.controller;

import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.saturne.dao.DAOFactory;
import org.centrale.saturne.dao.GroupeDAO;
import org.centrale.saturne.items.Connexion;

import org.centrale.saturne.items.Groupe;
import org.centrale.saturne.items.ItemManager;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author thibault
 */
@Controller
public class EditController {
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView handleGET(HttpServletRequest request,
            HttpServletResponse response){
         return new ModelAndView("managerGroupListe");
        
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView handlePOST(HttpServletRequest request,
            HttpServletResponse response){
        
        String groupeIdSTR = request.getParameter("groupeId");
        Integer grpid = Integer.parseInt(groupeIdSTR);
        String groupeLibelle = request.getParameter("groupeLibelle");
        String connexionId = request.getParameter("connexionId");
        String mot = request.getParameter("mot");
        
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        
        GroupeDAO groupeDAO = DAOFactory.getGroupeDAO();
        Groupe grp = groupeDAO.findById(grpid);
        grp.setGroupeLibelle(groupeLibelle);
        
        em.flush();
        transaction.commit();
        
        Collection<Groupe> groupeList ;
        if(mot==null || mot.equals("")){
            groupeList = groupeDAO.getAll();
        }
        else{
            groupeList = groupeDAO.getBySearch(mot);
        }
        
        ModelAndView returned = new ModelAndView("managerGroupListe");
        returned.addObject("groupeList", groupeList);
        returned.addObject("connexionId",connexionId);
        returned.addObject("mot",mot);
        return returned;
    }
}
