/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.saturne.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.saturne.dao.DAOFactory;
import org.centrale.saturne.dao.EvaluationDAO;
import org.centrale.saturne.items.Connexion;
import org.centrale.saturne.items.Evaluation;
import org.centrale.saturne.items.Questionnaire;
import org.centrale.saturne.items.ItemManager;
import org.centrale.saturne.items.Modeevaluation;
import org.centrale.saturne.items.Personne;
import org.centrale.saturne.utilities.Utilities;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.centrale.saturne.items.ItemManager;




/**
 *
 * @author GUO
 */
@Controller
public class CreateSessionController {
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView handleGET(HttpServletRequest request,
            HttpServletResponse response){
        ModelAndView returned = null;
        returned = new ModelAndView("createSession");

        return returned;
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView handlePOST(
            @RequestParam("connexionId") String connexionId,
            @RequestParam("questionnaireId") Integer questionnaireId,
            @RequestParam("evaluation_date") String eval_date,
            @RequestParam("debutH") String debutH,
            @RequestParam("debutM") String debutM,
            @RequestParam("dureeH") String dureeH,
            @RequestParam("dureeM") String dureeM,
            @RequestParam("modeevaluationId") Integer modeevaluationId,
            HttpServletRequest request, HttpServletResponse response) throws ParseException{
        ModelAndView returned = new ModelAndView("createSession");
        // ensure there is a connection
        if((connexionId != null) && (!connexionId.isEmpty())){
            Connexion aConnexion = Connexion.getConnexion(connexionId);
            Personne aPersonne = aConnexion.getPersonneId();
            returned = new ModelAndView("empty");
            Evaluation session = new Evaluation();
            session.setQuestionnaireId(Questionnaire.getItemById(questionnaireId));
            Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(eval_date);  
            session.setEvaluationDate(date1);
            String debut = "00:00";
            if ((!debutH.isEmpty()) && (!debutM.isEmpty())) {
                debut = debutH + ":" + debutM;
            } else if (!debutH.isEmpty()) {
                debut = debutH + ":00";
//                debut = "00:" + debutM;
            } else if (!debutM.isEmpty()) {
//                debut = debutH + ":00";
                debut = "00:" + debutM;
            }
            Date debutT = Utilities.isTime(debut);
            if (debutT == null) {
                // if not hh:mm:ss or hh:mm or hh, we set a time 
                debutT = Utilities.isTime("00:05");
            }
            session.setEvaluationDepart(debutT);
            
            String duree = "00:00";
            if ((!dureeH.isEmpty()) && (!dureeM.isEmpty())) {
                duree = dureeH + ":" + dureeM;
            } else if (!dureeH.isEmpty()) {
//                duree = "00:" + dureeM;
                duree = dureeH + ":00";
            } else if (!dureeM.isEmpty()) {
//                duree = dureeH + ":00";
                duree = "00:" + dureeM;
            }
            Date dureeT = Utilities.isTime(duree);
            if (dureeT == null) {
                // if not hh:mm:ss or hh:mm or hh, we set a time 
                dureeT = Utilities.isTime("00:05");
            }
            session.setEvaluationDuree(dureeT);
            session.setModeevaluationId(Modeevaluation.getItemById(modeevaluationId));
            session.setPersonneId(aPersonne);
            
            EvaluationDAO evaluationDAO = DAOFactory.getEvaluationDAO();
            evaluationDAO.create(session);
//            ItemManager.create(session);
         
        }
        return returned;
    }
    
}

