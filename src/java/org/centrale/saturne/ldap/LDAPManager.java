/*
 * Projet Saturne
 * 
 * Gestion de QCM en ligne
 *
 * Ecole Centrale Nantes - Fevrier 2019
 */
package org.centrale.saturne.ldap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.AuthenticationException;
import javax.naming.CommunicationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import org.centrale.saturne.utilities.Utilities;

/**
 * Manage LDAP communication
 *
 * @author antoinehurard
 */
public class LDAPManager {

    private static String ldapBasedn;
    private static String ldapHost;
    private static String ldapsecurityprotocol;
    private static String defaultLDAPdn;
    private static String defaultLDAPpass;

    private static boolean ldapAvalable;

    /**
     * initialize
     */
    public static void init() {
        if (ldapBasedn == null) {
            try {
                ResourceBundle parametre = ResourceBundle.getBundle(LDAPManager.class.getPackage().getName() + ".ldap");
                // USE config parameters
                ldapHost = Utilities.getResourceElement(parametre, "ldapHost").toLowerCase();
                ldapBasedn = Utilities.getResourceElement(parametre, "ldapBasedn").toLowerCase();
                ldapsecurityprotocol = Utilities.getResourceElement(parametre, "ldapsecurityprotocol").toLowerCase();
                defaultLDAPdn = Utilities.getResourceElement(parametre, "defaultLDAPdn");
                defaultLDAPpass = Utilities.getResourceElement(parametre, "defaultLDAPpass");

                ldapAvalable = true;
            } catch (Exception ex) {
                //unable to use LDAP authentication
                Logger.getLogger(LDAPManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Build LDAP properties for a BIND
     *
     * @return
     */
    private static Properties getLDAPProperties() {
        Properties env = new Properties();
        // Add server
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, ldapHost);
        if (ldapsecurityprotocol.equals("ssl")) {
            // Add SSL encription
            env.put(Context.SECURITY_PROTOCOL, "ssl");
            // Use locally defined socked manager to avoid certificate validation
            env.put("java.naming.ldap.factory.socket", "org.centrale.saturne.ldap.MySSLSocketFactory");
        }

        return env;
    }

    /**
     * Build LDAP properties for a BIND
     *
     * @param field
     * @param fieldContent
     * @param password
     * @return
     */
    private static Properties getLDAPProperties(String field, String fieldContent, String password) {
        Properties env = getLDAPProperties();

        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, field + "=" + fieldContent + "," + ldapBasedn);
        env.put(Context.SECURITY_CREDENTIALS, password);

        return env;
    }

    /**
     * Build LDAP properties for an admin BIND
     *
     * @return
     */
    private static Properties getLDAPDefaultProperties() {
        Properties env = getLDAPProperties();

        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, defaultLDAPdn);
        env.put(Context.SECURITY_CREDENTIALS, defaultLDAPpass);

        return env;
    }

    /**
     * Build LDAP properties for a BIND
     *
     * @param login
     * @param password
     * @return
     */
    private static Properties getLDAPProperties(String login, String password) {
        return getLDAPProperties("uid", login, password);
    }

    /**
     * Identify with a LDAP BIND
     *
     * @param login
     * @param password
     * @return
     */
    public static boolean identifyLDAP(String login, String password) {
        boolean isIdentified = false;
        try {
            Properties env = getLDAPProperties(login, password);
            if (login != null && password != null) {
                DirContext ctx = new InitialDirContext(env);
                isIdentified = true;
                ctx.close();
                ldapAvalable = true;
            }
        } catch (AuthenticationException ex) {
            // Non reconnu
        } catch (CommunicationException ex) {
            if (ldapAvalable) {
                Logger.getLogger(LDAPManager.class.getName()).log(Level.SEVERE, null, ex);
            } else {
                ldapAvalable = false;
            }
        } catch (NamingException ex) {
            // Non reconnu
        }

        return isIdentified;
    }

    /**
     * Search in LDAP
     *
     * @param login
     * @param password
     * @return
     */
    public static LDAPUser searchLDAP(String login, String password) {
        LDAPUser aUser = null;
        if (login != null && password != null) {
            Properties env = getLDAPProperties(login, password);
            try {
                DirContext ctx = new InitialDirContext(env);

                StringBuilder searchConstraint = new StringBuilder();
                searchConstraint.append("(uid=");
                searchConstraint.append(login);
                searchConstraint.append(")");

                SearchControls constraints = new SearchControls();
                constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
                NamingEnumeration results = ctx.search(ldapBasedn, searchConstraint.toString(), constraints);
                if (results != null) {
                    if (results.hasMore()) {
                        // Get all attributes
                        SearchResult si = (SearchResult) (results.next());
                        Attributes attrs = si.getAttributes();

                        aUser = new LDAPUser();
                        aUser.setUserUID(getAttribute(attrs, "uid"));
                        aUser.setUserNom(getAttribute(attrs, "sn"));
                        aUser.setUserPrenom(getAttribute(attrs, "givenName"));
                        aUser.setUserEmail(getAttribute(attrs, "mail"));
                        aUser.setUserUID(getAttribute(attrs, "uid"));
                        aUser.setUserAffiliation(getAttribute(attrs, "eduPersonAffiliation"));
                        aUser.setUserAffiliation(getAttribute(attrs, "supannEtuId"));
                    }
                }
                ctx.close();
                ldapAvalable = true;
            } catch (AuthenticationException ex) {
                // Non reconnu
            } catch (CommunicationException ex) {
                if (ldapAvalable) {
                    Logger.getLogger(LDAPManager.class.getName()).log(Level.SEVERE, null, ex);
                } else {
                    ldapAvalable = false;
                }
            } catch (NamingException ex) {
                // Non reconnu
                Logger.getLogger(LDAPManager.class.getName()).log(Level.INFO, null, ex);
            } catch (Exception ex) {
                // Non reconnu
                Logger.getLogger(LDAPManager.class.getName()).log(Level.INFO, null, ex);
            }
        }
        return aUser;
    }

    /**
     * Search in LDAP
     *
     * @param parameters
     * @return
     */
    public static List<LDAPUser> searchLDAPAll(HashMap<String, String> parameters) {
        List<LDAPUser> users = new ArrayList<LDAPUser>();
        if ((parameters != null) && (!parameters.isEmpty())) {
            // Use default properties. We don't know user password
            Properties env = getLDAPDefaultProperties();
            try {
                DirContext ctx = new InitialDirContext(env);

                StringBuilder searchConstraint = new StringBuilder();
                if (parameters.keySet().size() > 1) {
                    // If there is more than 1 parameter we add an &
                    searchConstraint.append("(&");
                }
                for (String key : parameters.keySet()) {
                    // Add new search element
                    String value = parameters.get(key);
                    searchConstraint.append("(");
                    searchConstraint.append(key);
                    searchConstraint.append("=");
                    searchConstraint.append(value);
                    searchConstraint.append(")");
                }
                if (parameters.keySet().size() > 1) {
                    // close parenthesis
                    searchConstraint.append(")");
                }

                // search
                SearchControls constraints = new SearchControls();
                constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
                NamingEnumeration results = ctx.search(ldapBasedn, searchConstraint.toString(), constraints);
                if (results != null) {
                    while (results.hasMore()) {
                        // Get all attributes
                        SearchResult si = (SearchResult) (results.next());
                        Attributes attrs = si.getAttributes();

                        LDAPUser aUser = new LDAPUser();
                        aUser.setUserUID(getAttribute(attrs, "uid"));
                        aUser.setUserNom(getAttribute(attrs, "sn"));
                        aUser.setUserPrenom(getAttribute(attrs, "givenName"));
                        aUser.setUserEmail(getAttribute(attrs, "mail"));
                        aUser.setUserUID(getAttribute(attrs, "uid"));
                        aUser.setUserAffiliation(getAttribute(attrs, "eduPersonAffiliation"));
                        users.add(aUser);
                    }
                }
                ctx.close();
                ldapAvalable = true;
            } catch (AuthenticationException ex) {
                // Non reconnu
            } catch (CommunicationException ex) {
                // Can't connect to LDAP
                if (ldapAvalable) {
                    Logger.getLogger(LDAPManager.class.getName()).log(Level.SEVERE, null, ex);
                } else {
                    ldapAvalable = false;
                }
            } catch (NamingException ex) {
                // Non reconnu
                Logger.getLogger(LDAPManager.class.getName()).log(Level.INFO, null, ex);
            } catch (Exception ex) {
                // Non reconnu
                Logger.getLogger(LDAPManager.class.getName()).log(Level.INFO, null, ex);
            }
        }
        return users;
    }

    /**
     * Get LDAP attribute
     *
     * @param attrs
     * @param attribute
     * @return
     */
    private static String getAttribute(Attributes attrs, String attribute) {
        Attribute attr = attrs.get(attribute);
        if (attr != null) {
            try {
                return (String) (attr.get());
            } catch (NamingException ex) {
                return "";
            }
        } else {
            return "";
        }
    }
}
