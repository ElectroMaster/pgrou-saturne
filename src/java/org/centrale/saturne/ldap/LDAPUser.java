/*
 * Projet Saturne
 * 
 * Gestion de QCM en ligne
 *
 * Ecole Centrale Nantes - Fevrier 2019
 */
package org.centrale.saturne.ldap;

/**
 * LDAPUser
 *
 * @author antoinehurard
 */
public class LDAPUser implements Comparable<LDAPUser> {

    private String userNom;
    private String userPrenom;
    private String userUID;
    private String userEmail;
    private String userAffiliation;
    private String userNumero;
    private String password;

    /**
     * Create
     */
    public LDAPUser() {
        this.userNom = null;
        this.userPrenom = null;
        this.userUID = null;
        this.userEmail = null;
        this.userAffiliation = null;
        this.userNumero = null;
        this.password = null;
    }

    /**
     * Get LastName
     *
     * @return
     */
    public String getUserNom() {
        return userNom;
    }

    /**
     * Set lastname
     *
     * @param userNom
     */
    public void setUserNom(String userNom) {
        this.userNom = userNom;
    }

    /**
     * Get firstname
     *
     * @return
     */
    public String getUserPrenom() {
        return userPrenom;
    }

    /**
     * Set firstname
     *
     * @param userPrenom
     */
    public void setUserPrenom(String userPrenom) {
        this.userPrenom = userPrenom;
    }

    /**
     * Get UID
     *
     * @return
     */
    public String getUserUID() {
        return userUID;
    }

    /**
     * Set UID
     *
     * @param userUID
     */
    public void setUserUID(String userUID) {
        this.userUID = userUID;
    }

    /**
     * Get Email
     *
     * @return
     */
    public String getUserEmail() {
        return userEmail;
    }

    /**
     * Set Email
     *
     * @param userEmail
     */
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    /**
     * get Affiliation
     * @return 
     */
    public String getUserAffiliation() {
        return userAffiliation;
    }

    /**
     * Set Affiliation
     * @param userAffiliation 
     */
    public void setUserAffiliation(String userAffiliation) {
        this.userAffiliation = userAffiliation;
    }

    /**
     * Get password
     * @return 
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set Password
     * @param password 
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * get Student ID
     * @return 
     */
    public String getUserNumero() {
        return userNumero;
    }

    /**
     * Set Student ID
     * @param userNumero 
     */
    public void setUserNumero(String userNumero) {
        this.userNumero = userNumero;
    }

    @Override
    public int compareTo(LDAPUser o) {
        if (o == null) {
            return -1;
        } else {
            String s1 = this.getUserNom() + " " + this.getUserPrenom();
            String s2 = o.getUserNom() + " " + o.getUserPrenom();
            return s1.compareTo(s2);
        }
    }

}
