## Before you run
> CAI Pengfei

**Already done**: Add `postgresql-42.2.5.jar` in  `<saturne_root>/lib/PostgreSQLDriver`.

**For linux users**:

Note: tomcat Version after 9.0.16 seems to have fixed this bug.

add these lines in the head
if you you have seen `Error: project not deployed successful`, In fact it's a problem of tomcat (Directory Permission problem).
You need to do:
```
cd <tomcat_root>
sudo chmod 777 ./work/Catalina
sudo chmod 777 ./work/Catalina/localhost
sudo chmod 777 ./conf/Catalina
sudo chmod 777 ./conf/Catalina/localhost
```
Then you run the project and you can see a simple login web page.

## Avoid commit `persistence.xml` and `nbproject/private/private.properties`
After you merge with orign/master, please make sure that you didn't commit this two files.
If you have commit these two files, these steps can help you solve this:
```
git rm -rf --cached .
git add .
git commit -m "remove untracked files: persistence.xml and private.properties"
```

## screenshots:
- Login page
    > Done: add jsp page

    > Done: login

    > Done: disconnect

    > Done: show error message.

<img src="./screenshots/login.png" alt="screenshot" style="display:block; max-width:500px;">

- Home page - TODO: add more things

<img src="./screenshots/home_page.png" alt="screenshot" style="display:block; max-width:500px;">

- createUser - TODO:

<img src="./screenshots/createUser.png" alt="screenshot" style="display:block; max-width:500px;">

- modifyUser - TODO:

<img src="./screenshots/modifyUser.png" alt="screenshot" style="display:block; max-width:500px;">

- manageUserListe - TODO:
Liste
<img src="./screenshots/manageUserListe.png" alt="screenshot" style="display:block; max-width:500px;">

- consulteUserListe - TODO:

<img src="./screenshots/consulteUserListe.png" alt="screenshot" style="display:block; max-width:500px;">

- createGroup - TODO:

<img src="./screenshots/createGroup.png" alt="screenshot" style="display:block; max-width:500px;">

- modifyGroup - TODO:

<img src="./screenshots/modifyGroup.png" alt="screenshot" style="display:block; max-width:500px;">

- manageGroupListe - TODO:

<img src="./screenshots/manageGroupListe.png" alt="screenshot" style="display:block; max-width:500px;">

- consulteGroupListe - TODO:

<img src="./screenshots/consulteGroupListe.png" alt="screenshot" style="display:block; max-width:500px;">

- createQuestionnaire
    > Done: add questionnaire in database.

    > TODO: add createDefault method for class typeQuestionnaire.

    > Done: update loadController --> can pass different parameters for different page

    > Done: complete mot cle design

    > TODO: add mot cle into database, perheps need to add one colomn in database.

    > Done: invalid feedback

<img src="./screenshots/createQuestionnaire.png" alt="screenshot" style="display:block; max-width:500px;">

<img src="./screenshots/mot_cle.png" alt="screenshot" style="display:block; max-width:300px;">

<img src="./screenshots/createQuestionnaire_invalidInput.png" alt="screenshot" style="display:block; max-width:500px;">

- modifyQuestionnaire

    > Done: complete design

    > Done: add jsp page 1

    > Done: add color for avator link

    > TODO: add jsp page 2

<img src="./screenshots/modifyQuestionnaire1.png" alt="screenshot" style="display:block; max-width:500px;">


- consulterQuestionnaireListe
    > Done.

<img src="./screenshots/consulteQuestionnaireListe.png" alt="screenshot" style="display:block; max-width:500px;">

- managerQuestionnaireListe
    > Done: delete row

    > Done: edit row in table

    > Done: modify row

    > Done: invalid feedback


<img src="./screenshots/managerQuestionnaireListe_step1.png" alt="screenshot" style="display:block; max-width:500px;">

<img src="./screenshots/managerQuestionnaireListe_step2.png" alt="screenshot" style="display:block; max-width:500px;">

<img src="./screenshots/managerListe_invalidInput.png" alt="screenshot" style="display:block; max-width:500px;">
