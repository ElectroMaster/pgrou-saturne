function getMyTag(ref) {
  var theTDRef = ref;
  var found = false;
  while (theTDRef !== null && !found) {
    if (theTDRef.nodeType == 3) {
      theTDRef = theTDRef.parentNode;
    } else if (theTDRef.tagName == "TR") {
      found = true;
    } else {
      theTDRef = theTDRef.parentNode;
    }
  }
  return theTDRef;
}

function deleteGroupe(ref,id) {
  $.ajax({
    url: "AJAX.do",
    data: {
      "action": "deleteGroupe",
      "id": id
    },
    method: "POST",
    success: function(result) {
      var theTR = getMyTag(ref);
      if(theTR!=null){
          var parent = theTR.parentNode;
          parent.removeChild(theTR);
      }
    },
    error: function(resultat, status, erreur) {
      console.log("error");
    }
  });
}

function ajouterMembre(){
    let tableau = document.getElementById("idtableau");
    let ligne = document.createElement("tr");
    tableau.appendChild(ligne); 
}

function deletePersonne(ref,groupeId,personneId) {
  $.ajax({
    url: "AJAX.do",
    data: {
      "action": "deletePersonne",
      "groupeId": groupeId,
      "personneId":personneId
    },
    method: "POST",
    success: function(result) {
      var theTR = getMyTag(ref);
      if(theTR!=null){
          var parent = theTR.parentNode;
          parent.removeChild(theTR);
      }
    },
    error: function(resultat, status, erreur) {
      console.log("error");
    }
  });
}


function editGroupe(ref,libelle,connexion,mot){
    let input=document.getElementById("libelle"+ref);
    input.innerHTML="<form action='edit.do' method='POST'>"
                       +"<input type='hidden' name='mot' value='"+mot+"'/>"
                       +"<input type='hidden' name='connexionId' value='"+connexion+"'/>"
                       +"<input type='hidden' name='groupeId' value='"+ref+"'/>"
                       +"<input id='libel"+ref+"' name='groupeLibelle' type='text' value='"+libelle+"'/>" 
                        +"<button>Edit</button></form>";
}



function activateGroupe(ref,grpId){
    let tr=getMyTag(ref);
    console.log(tr);
    $.ajax({
    url: "AJAX.do",
    data: {
      "action": "activateGroupe",
      "groupeId": String(grpId)
    },
    method: "POST",
    success: function(result) {
        console.log(result);
        if(result.valide){
            tr.setAttribute("style","background-color:lightcoral");
        }
        else{
          tr.setAttribute("style","");  
        }
        
        console.log("ok");
      
    },
    error: function(resultat, status, erreur) {
      console.log("error");
    }
  });
}

function rechercherGroupe(id,mot){
    let p = document.getElementById("rechercher");
    p.innerHTML ="<form action='load.do' method='GET'>"
                  +"<input type='hidden' name='mot' value='"+mot+"'/>"
                  +"<input type='hidden' name='id' value='"+id+"'/>"
                  +"<input type='hidden' name='page' value='managerGroupListe'/>"
                  +"<input type='text' name='Libelle'/>"
                    +"<button>Rechercher</button></form>";
}

function rechercherGroupeCons(id){
    let p = document.getElementById("rechercher");
    p.innerHTML ="<form action='load.do' method='GET'>"
                  +"<input type='hidden' name='id' value='"+id+"'/>"
                  +"<input type='hidden' name='page' value='consulterGroupListe'/>"
                  +"<input type='text' name='Libelle'/>"
                    +"<button>Rechercher</button></form>";
}

function validateEdit(grpId){
    console.log("test");
    let grpLibelle=document.getElementById("libel"+String(grpId)).textContent;
    console.log(grpLibelle);
    $.ajax({
    url: "AJAX.do",
    data: {
      "action": "editGroupe",
      "groupeId": String(grpId),
      "groupeLibelle": grpLibelle
    },
    method: "POST",
    success: function(result) {
        alert("mod");
      
    },
    error: function(resultat, status, erreur) {
      console.log("error");
    }
  });
   
}

function rechercherMembreGroupe(id, groupeId,manager){
    let p = document.getElementById("rechercherMembre");
    p.innerHTML ="<form action='load.do' method='GET'>"
                  +"<input type='hidden' name='id' value='"+id+"'/>"
                  +"<input type='hidden' name='page' value='membreGroupeList'/>"
                  +"<input type='hidden' name='groupeId' value='"+groupeId+"' />"
                  +"<input type='hidden' name='manager' value='"+manager+"' />"
                  +"<input type='text' name='nom'/>"
                    +"<button>Rechercher</button></form>";
}