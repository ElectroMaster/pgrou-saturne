var mot_id = 0;
var answer_id = 0;
var jsonStr = '{"keywords":[],"answers":[]}';
var trueAnswers = 0;

function getMyLIButtonTag(ref) {
    var theButtonRef = ref;
    var found = false;
    while ((theButtonRef !== null) && (!found)) {
        if (theButtonRef.tagName === "LI") {
            found = true;
        } else {
            theButtonRef = theButtonRef.parentNode;
        }
    }
    return theButtonRef;
}

function getMySPANButtonTag(ref) {
    var theButtonRef = ref;
    var found = false;
    while ((theButtonRef !== null) && (!found)) {
        if (theButtonRef.tagName === "SPAN") {
            found = true;
        } else {
            theButtonRef = theButtonRef.parentNode;
        }
    }
    return theButtonRef;
}

function addMot(ref) {
    mot_id += 1;
    var mot_name = "mot_" + mot_id;
    
    hideAddMot(true);

    $(ref).before($('<input>', {
        type: "text",
        class: "form-control",
        style: "display:inline;width:30%;min-width:400px;",
        name: mot_name,
        onkeypress: 'show(this,event,'+mot_id+')',
        placeholder: "Press Enter to confirm"}),
            $('<button>', {
                style: "width:30px; display:inline;margin-left:-30px;border:none;background:transparent; margin-right:10px;",
                type: "button",
                onclick: "deleteCurrent(this)"
            }).append("<i class='fas fa-times'>"));
}

function addSelection(){
    var selectedMotcle = $( "#selectMotcle" );
    if (selectedMotcle.length > 0) {
        
        mot_id += 1;
        var keywordId = selectedMotcle.val();
        var keywordText = $( "#selectMotcle option:selected").text();

        var testItem = $("#used_" + keywordId);
        if (testItem.length < 1 ) {
            $("#mot_cle").before($('<span>', {
                    class: "badge badge-pill badge-info",
                    style: "padding: 5px; font-size: 12px; margin-right:10px;",
                    id: "used_"+keywordId
                }).append(keywordText).append(
                $('<button>', {
                style: "width:30px; display:inline;border:none;background:transparent; padding: 5px;",
                type: "button",
                onclick: 'deleteKeyword(this,' + mot_id + ')'
            }).append("<i style='color:white;' class='fas fa-times'>")));
            var obj = JSON.parse(jsonStr);
            obj['keywords'].push({"id": mot_id, "libelle": keywordText, "idDatabase":keywordId});
            jsonStr = JSON.stringify(obj);
            changeInputJSON();
        }
    }
}

function addAnswer(ref) {
    answer_id += 1;
    $(ref).before($('<input>', {
        type: "text",
        class: "form-control",
        style: "display:inline;width:30%;min-width:400px;",
        onkeypress: "showAnswer(this,event,answer_id)",
        placeholder: "Press Enter to confirm"}),
            $('<button>', {
                style: "width:30px; display:inline;margin-left:-30px;border:none;background:transparent; margin-right:10px;",
                type: "button",
                onclick: "deleteCurrent(this)"
            }).append("<i class='fas fa-times'>"));
}

function hideAddMot(hide){
    if(hide){
        $("#mot_cle").attr("disabled", true);
    }else{
        $("#mot_cle").attr("disabled", false);
    }
}

function checkMot(mot,ref){
    //do suppress
    var selectedMotcle = $( "#selectMotcle" );
    var obj = JSON.parse(jsonStr);
    
    if (selectedMotcle.length > 0 || obj.length > 0) {
        if(obj['keywords'].some(item => item.libelle === mot)){
            $(ref).attr("style", "border-color:rgb(255, 0, 0);box-shadow: 0 0 0 0.2rem rgba(255, 0, 0, 0.25);display:inline;width:30%;min-width:400px;");
            $(ref).blur(function () {
                $(this).attr("style", "display:inline;width:30%;min-width:400px;");
            });
            $("#mot_cle_invalid").attr("style", "display:block;");
            return false;
        }else{
            var keywordId = 0;
            $('#selectMotcle  option').each(function(){
                if ($(this).text() === mot) {
                    keywordId = $(this).val();
                }
            });
            if(keywordId > 0){
                mot_id += 1;

               $("#mot_cle").before($('<span>', {
                        class: "badge badge-pill badge-info",
                        style: "padding: 5px; font-size: 12px; margin-right:10px;",
                        id: "used_"+keywordId
                    }).append(mot).append(
                    $('<button>', {
                    style: "width:30px; display:inline;border:none;background:transparent; padding: 5px;",
                    type: "button",
                    onclick: 'deleteKeyword(this,' + mot_id + ')'
                }).append("<i style='color:white;' class='fas fa-times'>")));
                var obj = JSON.parse(jsonStr);
                obj['keywords'].push({"id": mot_id, "libelle": mot, "idDatabase":keywordId});
                jsonStr = JSON.stringify(obj);
                changeInputJSON();
                return false;
            }
            $("#mot_cle_invalid").attr("style", "display:none;");
            return true;
        }
    }
    return false;
}

function show(ref, e, id) {
    if (e.keyCode === 13) {
        var mot = $(ref).val();
        var selectedMotcle = $( "#selectMotcle" );
        var obj = JSON.parse(jsonStr);
        
        if (selectedMotcle.length > 0 || obj.length > 0) {
            if(obj['keywords'].some(item => item.libelle === mot)){
                $(ref).attr("style", "border-color:rgb(255, 0, 0);box-shadow: 0 0 0 0.2rem rgba(255, 0, 0, 0.25);display:inline;width:30%;min-width:400px;");
                $(ref).blur(function () {
                    $(this).attr("style", "display:inline;width:30%;min-width:400px;");
                });
                $("#mot_cle_invalid").attr("style", "display:block;");
            }else{
                hideAddMot(false);
                var keywordId = 0;
                $('#selectMotcle  option').each(function(){
                    if ($(this).text() === mot) {
                        keywordId = $(this).val();
                    }
                });
                if(keywordId > 0){
                    mot_id += 1;
                    $(ref).next().remove();
                    $("#mot_cle").before($('<span>', {
                            class: "badge badge-pill badge-info",
                            style: "padding: 5px; font-size: 12px; margin-right:10px;",
                            id: "used_"+keywordId
                        }).append(mot).append(
                        $('<button>', {
                        style: "width:30px; display:inline;border:none;background:transparent; padding: 5px;",
                        type: "button",
                        onclick: 'deleteKeyword(this,' + mot_id + ')'
                    }).append("<i style='color:white;' class='fas fa-times'>")));
                    $(ref).remove();
                    var obj = JSON.parse(jsonStr);
                    obj['keywords'].push({"id": mot_id, "libelle": mot, "idDatabase":keywordId});
                    jsonStr = JSON.stringify(obj);
                    changeInputJSON();
                }else{
                    $(ref).next().remove();
                    $(ref).before($('<span>', {
                            class: "badge badge-pill badge-info",
                            style: "padding: 5px; font-size: 12px; margin-right:10px;"
                        }).append(mot).append(
                        $('<button>', {
                        style: "width:30px; display:inline;border:none;background:transparent; padding: 5px;",
                        type: "button",
                        onclick: 'deleteKeyword(this,' + id + ')'
                    }).append("<i style='color:white;' class='fas fa-times'>")));
                    $(ref).remove();
                    var obj = JSON.parse(jsonStr);
                    obj['keywords'].push({"id": id, "libelle": mot, "idDatabase": 0});
                    jsonStr = JSON.stringify(obj);
                    changeInputJSON();
                }
            }
        }
    }
}

function showAnswer(ref, e, id) {
    if (e.keyCode === 13) {
        var questionTexte = $(ref).val();
        $(ref).next().remove();

        $("#liste_reponses").append($('<li class="list-group-item d-flex justify-content-between align-items-center">'
                + questionTexte
                + '<div>'
                + '<button type="button" class="btn btn-light" onclick="validateAnswer(this,' + id + ')"><i class="fas fa-check"></button><button type="button" class="btn btn-light" onclick="deleteAnswer(this,' + id + ')"><i class="fas fa-times"></button>'
                + '</div>'
                + '</li>'));
        $(ref).remove();
        var obj = JSON.parse(jsonStr);
        obj['answers'].push({"id": id, "libelle": questionTexte, "trueAnswer": false});
        jsonStr = JSON.stringify(obj);
        changeInputJSON();
    }
}

function deleteAnswer(ref, id) {
    theLIRef = getMyLIButtonTag(ref);
    if (theLIRef !== null) {
        theLIRef.parentNode.removeChild(theLIRef);
        var obj = JSON.parse(jsonStr);
        for (var i = 0; i < obj.answers.length; i++) {
            if (obj.answers[i].id === id) {
                if(obj.answers[i].trueAnswer){
                    trueAnswers -= 1;
                }
                obj.answers.splice(i, 1);
                break;
            }
        }
        jsonStr = JSON.stringify(obj);
        changeInputJSON();
    }
}

function deleteKeyword(ref, id) {
    theSPANRef = getMySPANButtonTag(ref);
    if (theSPANRef !== null) {
        theSPANRef.parentNode.removeChild(theSPANRef);
        var obj = JSON.parse(jsonStr);
        for (var i = 0; i < obj.keywords.length; i++) {
            if (obj.keywords[i].id === id) {
                obj.keywords.splice(i, 1);
                break;
            }
        }
        jsonStr = JSON.stringify(obj);
        changeInputJSON();
    }
}

function deleteCurrent(ref) {
    $(ref).prev().remove();
    $(ref).remove();
    hideAddMot(false);
}

function validateAnswer(ref, id) {
    if($( "#type_question" ).val()==="1"){
        setAllAnswersFalse();
    }
    theLIRef = getMyLIButtonTag(ref);
    if (theLIRef !== null) {
        if ($(theLIRef).hasClass("list-group-item-success")) {
            $(theLIRef).removeClass("list-group-item-success");
            trueAnswers -= 1;
            var obj = JSON.parse(jsonStr);
            for (var i = 0; i < obj.answers.length; i++) {
                if (obj.answers[i].id === id) {
                    obj.answers[i].trueAnswer = false;
                    break;
                }
            }
            jsonStr = JSON.stringify(obj);
            changeInputJSON();
        } else {
            $(theLIRef).addClass("list-group-item-success");
            trueAnswers += 1;
            var obj = JSON.parse(jsonStr);
            for (var i = 0; i < obj.answers.length; i++) {
                if (obj.answers[i].id === id) {
                    obj.answers[i].trueAnswer = true;
                    break;
                }
            }
            jsonStr = JSON.stringify(obj);
            changeInputJSON();
        }
    }
}

function changeInputJSON() {
    $('#inputJSON').val(jsonStr);
}
changeInputJSON();

function changeTypeQuestion(){
    if($( "#type_question" ).val()==="3"){
        $('#answersDiv').attr("style", "display:none;");
        $("#uniqueAnswerDiv").attr("style", "display:block;");
    }
    else{
        $("#uniqueAnswerDiv").attr("style", "display:none;");
        $('#answersDiv').attr("style", "display:block;");
        if($( "#type_question" ).val()==="1" && trueAnswers > 1){
                setAllAnswersFalse();
        }
    }
}

function setAllAnswersFalse(){
    $('#liste_reponses li').each(function(){
        if ($(this).hasClass("list-group-item-success")) {
            $(this).removeClass("list-group-item-success");
        }
    });
    var obj = JSON.parse(jsonStr);
    for (var i = 0; i < obj.answers.length; i++) {
        obj.answers[i].trueAnswer = false;
    }
    jsonStr = JSON.stringify(obj);
    changeInputJSON();
}

function validateMyForm() {
    if ($('#libelle_question').val() === "") {
        $('#libelle_question').attr("style", "border-color:rgb(255, 0, 0);box-shadow: 0 0 0 0.2rem rgba(255, 0, 0, 0.25);");
        $('#libelle_question').blur(function () {
            $(this).removeAttr("style");
        });
        $('#libelle_question').next(".invalid-feedback").attr("style", "display:block;");
        return false;
    } else {
        $('#libelle_question').next(".invalid-feedback").attr("style", "display:none;");
        if($( "#type_question" ).val()==="3"){
            var obj = JSON.parse(jsonStr);
            obj.answers = [];
            jsonStr = JSON.stringify(obj);
            changeInputJSON();
        }else{
            $('#reponse_litterale').val("");
        }
        return true;
    }
}
