
function change_status_color(id, firstCharacter) {
    var colors = ["rgba(0,123,255, 0.4)", "rgba(102,16,242, 0.4)", "rgba(111,66,193, 0.4)",
    "rgba(232,62,140,0.4)", "rgba(220, 53, 69, 0.4)", "rgba(253,126,20,0.4)",
    "rgba(255,193,7,0.4)", "rgba(40, 167, 69, 0.4)", "rgba(32,201,151,0.4)",
    "rgba(23,162,184,0.4)"];
    var name = "#" + id
    var len = colors.length;
    var color_str = colors[firstCharacter.charCodeAt(0) % len];
    $(name).css("background-color", color_str);
};
