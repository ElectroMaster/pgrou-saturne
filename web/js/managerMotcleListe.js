
function getMyTRTag(ref) {
    var theTDRef = ref;
    var found = false;
    while ((theTDRef !== null) && (!found)) {
        if (theTDRef.tagName === "TR") {
            found = true;
        } else {
            theTDRef = theTDRef.parentNode;
        }
    }
    return theTDRef;
}


function deleteLine(theTDRef, id) {
    $.ajax({
        url: "AJAX.do",
        data: {
            "id": id,
            "action": "deleteKeyword"
        },
        method: "POST",
        success: function(result) {
            theTRRef = getMyTRTag(theTDRef);
            if (theTRRef !== null) {
                theTRRef.parentNode.removeChild(theTRRef);
            }
            console.log("delete done");
        },
        error: function(result, statut, erreur) {
            console.log("delete error");
        }
    });
}





function editLine(ref, id){
    $(ref).children().removeClass().addClass("fas fa-check");
    $(ref).next().children().removeClass().addClass("fas fa-minus");


    $(ref).parent().parent().children().each(function() {
        // td not contain button
        if ($(this).attr("class")!="operationButton" && ($(this).attr("class")!="nombreQuestions") && $(this).attr("scope")!="row"){
            $(this).html($('<input>', {
                type: "text",
                class: 'form-control',
                name: $(this).attr("class"),
                value: $(this).html(),
                placeholder: $(this).html()
            }));
        }
    });
    $(ref).attr("onclick", "sendData(this, "+id+")");
    $(ref).next().attr("onclick", "rowReset(this, "+id+")");
}

function sendData(ref, id){
    var data = {};
    try{
        $(ref).parent().parent().children().each(function() {
            if ($(this).attr("class") != "operationButton" && $(this).attr("scope")!= "row"){
                data[$(this).attr("class")] = $(this).children(":input").val();
            }

        });
        console.log(data);
        if(data["motcleLibelle"] === ""){
            throw "le libellé ne peut pas être vide";
        }
        $.ajax({
            url: "AJAX.do",
            data: {
                "id": id,
                "action": "editKeyword",
                "data": JSON.stringify(data)
            },
            method: "POST",
            success: function(result) {
                console.log(result);
                saveRow(ref, id);


            },
            error: function(result, statut, erreur) {
                console.log("error");
            }
        });
    } catch(err){
        alert(err);
        $(ref).parent().parent().children().each(function() {
        if ($(this).attr("class") === "motcleLibelle"){
            $(this).children("input").attr("style", "border-color:rgb(255, 0, 0);box-shadow: 0 0 0 0.2rem rgba(255, 0, 0, 0.25);");
        }});
    }

}





function rowReset(ref, id){
    $(ref).children().removeClass().addClass("fas fa-trash-alt");
    $(ref).prev().children().removeClass().addClass("fas fa-edit");
    $(ref).parent().parent().children().each(function() {
        if ($(this).attr("class") != "operationButton" && $(this).attr("scope")!= "row"){
            $(this).html($(this).children(":input").attr("placeholder"));
        }

    });
    $(ref).attr("onclick", "deleteLine(this, "+id+")");
    $(ref).prev().attr("onclick", "editLine(this, "+id+")");
}

function saveRow(ref, id){
    $(ref).next().children().removeClass().addClass("fas fa-trash-alt");
    $(ref).children().removeClass().addClass("fas fa-edit");
    $(ref).parent().parent().children().each(function() {
        if ($(this).attr("class") != "operationButton" && $(this).attr("scope")!= "row"){
            $(this).html($(this).children(":input").val());
        }
    });
    $(ref).next().attr("onclick", "deleteLine(this, "+id+")");
    $(ref).attr("onclick", "editLine(this, "+id+")");
}
