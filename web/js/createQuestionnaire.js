var mot_list = {};
var mot_id = 0;

function addMot(ref){
    mot_id += 1;
    var mot_name = "mot_"+mot_id;

    $(ref).before($('<input>', {
        type: "text",
        class: "form-control",
        style: "display:inline;width:15%;min-width:140px;",
        name: mot_name,
        onkeypress: "show(this,event)",
        placeholder: "Press Enter to confirm"}),
        $('<button>', {
            style: "width:30px; display:inline;margin-left:-30px;border:none;background:transparent; margin-right:10px;",
            type: "button",
            onclick: "deleteCurrent(this)"
        }).append("<i class='fas fa-times'>"));
}

function show(ref,e){
    if(e.keyCode == 13){
            // alert($(ref).val());
        var mot = $(ref).val();
        $(ref).next().remove();

        // alert(mot);
        $(ref).before($('<span>',{
            class: "badge badge-pill badge-info",
            style: "padding: 5px; font-size: 12px; margin-right:10px;"
        }).append(mot));
                $(ref).remove();
    }

}

function deleteCurrent(ref){
    $(ref).prev().remove();
    $(ref).remove();
}


function chooseTypeQuest() {
    if($('#type_questionnaire').val() != "2"){
        $('#duree_setting').attr("style","display:block;");
    } else {
        $('#duree_setting').attr("style","display:none;");
    }
}

function validateMyForm(){
    if ($('#nom_questionnaire').val() == ""){
        $('#nom_questionnaire').attr("style", "border-color:rgb(255, 0, 0);box-shadow: 0 0 0 0.2rem rgba(255, 0, 0, 0.25);");
        $('#nom_questionnaire').blur(function() {
            $(this).removeAttr("style");
        });
        $('#nom_questionnaire').next(".invalid-feedback").attr("style", "display:block;");
        return false;
    }
    else {
        $('#nom_questionnaire').next(".invalid-feedback").attr("style", "display:none;");
        return true;
    }
}
