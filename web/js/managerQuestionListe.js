
function getMyTRTag(ref) {
    var theTDRef = ref;
    var found = false;
    while ((theTDRef !== null) && (!found)) {
        if (theTDRef.tagName === "TR") {
            found = true;
        } else {
            theTDRef = theTDRef.parentNode;
        }
    }
    return theTDRef;
}


function deleteLine(theTDRef, id) {
    $.ajax({
        url: "AJAX.do",
        data: {
            "id": id,
            "action": "deleteQuestion"
        },
        method: "POST",
        success: function(result) {
            theTRRef = getMyTRTag(theTDRef);
            if (theTRRef !== null) {
                theTRRef.parentNode.removeChild(theTRRef);
            }
            console.log("delete done");
        },
        error: function(result, statut, erreur) {
            console.log("delete error");
        }
    });
}

/*function editLine(ref, id) {
    $.ajax({
        url: "AJAX.do",
        data: {
            "id": id,
            "action": "obtainTypequestionnaireAndTypeAcces"
        },
        method: "POST",
        success: function(result) {
            console.log(result);
            var typeacces = result.typeAcces;
            var typequest = result.typeQuestionnaire;
            editMode(ref, id, typequest, typeacces);

        },
        error: function(result, statut, erreur) {
            console.log("error");
        }
    });
}*/





function editMode(ref, id, typequest, typeacces){
    $(ref).children().removeClass().addClass("fas fa-check");
    $(ref).next().children().removeClass().addClass("fas fa-minus");


    $(ref).parent().parent().children().each(function() {
        // td not contain button
        if ($(this).attr("class")!="operationButton" && $(this).attr("scope")!="row"){
            $(this).html($('<input>', {
                type: "text",
                class: 'form-control',
                name: $(this).attr("class"),
                value: $(this).html(),
                placeholder: $(this).html()
            }));
            if($(this).attr("class") == "typeQuetionnaire"){
                // console.log($(this).children().attr("placeholder"));
                var typeQuestionnaire_chosen = $(this).children().attr("placeholder");
                $(this).children().attr("type","hidden");
                var sel = $('<select>',{
                    class: "form-control form-control-sm",
                    name: "type_questionnaire"
                });
                $(typequest).each(function(){
                    // sel.append($('<option>').attr("value", this.id).text(this.libelle));
                    if (this.libelle == typeQuestionnaire_chosen){
                        sel.append($('<option>',{value: this.id, selected: "selected"}).text(this.libelle));
                    } else {
                        sel.append($('<option>',{value: this.id}).text(this.libelle));
                    }

                });
                $(this).append(sel);
            }

            if($(this).attr("class") == "typeAcces"){
                var typeAcces_chosen = $(this).children().attr("placeholder");
                $(this).children().attr("type","hidden");
                var sel = $('<select>',{
                    class: "form-control form-control-sm",
                    name: "type_acces"
                });
                $(typeacces).each(function(){
                    if (this.libelle == typeAcces_chosen){
                        sel.append($('<option>', {value: this.id, selected: "selected"}).text(this.libelle));
                    } else {
                        sel.append($('<option>', {value: this.id}).text(this.libelle));
                    }

                });
                $(this).append(sel);
            }

            if($(this).attr("class") == "personneNom"){
                $(this).children().attr("type", "hidden");
                $(this).append($(this).children().attr("placeholder"));
            }
        }

        // if ($(this).children().length == 0 && $(this).attr("scope")!= "row")
        //     $(this).html($('<input>', {
        //         type: 'text',
        //         class: 'form-control',
        //         value: $(this).html(),
        //         placeholder: $(this).html()
        //     }));
    });
    $(ref).attr("onclick", "sendData(this, "+id+")");
    $(ref).next().attr("onclick", "rowReset(this, "+id+")");
}

function sendData(ref, id){
    var data = {};
    try{
        $(ref).parent().parent().children().each(function() {
            if ($(this).attr("class") != "operationButton" && $(this).attr("scope")!= "row"){
                if($(this).attr("class") == "typeQuetionnaire" || $(this).attr("class") == "typeAcces"){
                    // alert($(this).children("select").find(":selected").text());
                    // console.log($(this).children("select").find(":selected").text());
                    data[$(this).attr("class")] = $(this).children("select").val();

                } else{
                    // console.log($(this).children(":input").val());
                    data[$(this).attr("class")] = $(this).children(":input").val();

                }

            }

        });
        // console.log(data);
        if(data["questionnaireLibelle"] == ""){
            throw "le nom du questionnaire ne peut pas être vide";
        }
        $.ajax({
            url: "AJAX.do",
            data: {
                "id": id,
                "action": "editQuestionnaire",
                "data": JSON.stringify(data)
            },
            method: "POST",
            success: function(result) {
                console.log(result);
                saveRow(ref, id);


            },
            error: function(result, statut, erreur) {
                console.log("error");
            }
        });
    } catch(err){
        alert(err);
        $(ref).parent().parent().children().each(function() {
        if ($(this).attr("class") == "questionnaireLibelle"){
            $(this).children("input").attr("style", "border-color:rgb(255, 0, 0);box-shadow: 0 0 0 0.2rem rgba(255, 0, 0, 0.25);");
        }});
    }

}





function rowReset(ref, id){
    $(ref).children().removeClass().addClass("fas fa-trash-alt");
    $(ref).prev().children().removeClass().addClass("fas fa-edit");
    $(ref).parent().parent().children().each(function() {
        if ($(this).attr("class") != "operationButton" && $(this).attr("scope")!= "row"){
            $(this).html($(this).children(":input").attr("placeholder"));
        }

    });
    $(ref).attr("onclick", "deleteLine(this, "+id+")");
    $(ref).prev().attr("onclick", "editLine(this, "+id+")");
}

function saveRow(ref, id){
    $(ref).next().children().removeClass().addClass("fas fa-trash-alt");
    $(ref).children().removeClass().addClass("fas fa-edit");
    $(ref).parent().parent().children().each(function() {
        if ($(this).attr("class") != "operationButton" && $(this).attr("scope")!= "row"){
            if($(this).attr("class") == "typeQuetionnaire" || $(this).attr("class") == "typeAcces"){
                // alert($(this).children("select").find(":selected").text());
                // console.log($(this).children("select").find(":selected").text());
                $(this).html($(this).children("select").find(":selected").text());

            } else{
                // console.log($(this).children(":input").val());
                $(this).html($(this).children(":input").val());

            }

        }

    });
    $(ref).next().attr("onclick", "deleteLine(this, "+id+")");
    $(ref).attr("onclick", "editLine(this, "+id+")");
}
