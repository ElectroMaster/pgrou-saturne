/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function callBackChargeQuestionnaire(xhr) {
    var docXML = xhr.responseXML;

    // remove questions
    var session = docXML.getElementsByTagName("session");
    var modeeval = docXML.getElementsByTagName("modeevaluation");
    var nbValues = session.length;
    if (nbValues > 0) {
        var aNewLineItem;
        var aNewBoxItem;
        var theText;
        var anInput;
        var endListItem = document.getElementById("endList");
        var nom = session[0].firstChild.data;
        var theID = session[0].attributes[0].value;
        var descriptions = docXML.getElementsByTagName("description");
        var description = descriptions[0].firstChild.data;

        // count current sessions
        var tr = docXML.getElementsByTagName("tr");
        var nbTR = tr.length;
        var curNbSessions = 0;
        for (var i = 0; i < nbTR; i++) {
            if ((tr[i] !== null) && (tr[i].id !== null) && (tr[i].id.substr(0, 8) === "session_")) {
                curNbSessions++;
            }
        }
        curNbSessions++;

        // We generate a new negative value for the session ID
        curNbSessions = -curNbSessions;

        // ---------------------------------------------------------------------
        aNewLineItem = createTableRow("session_" + curNbSessions,"sessionLine");

        aNewBoxItem = createTableCell(aNewLineItem, "td", null, null, 0, 3, nom);
        createInput(aNewBoxItem, "hidden", "sessionquest_" + curNbSessions, "sessionquest_" + curNbSessions,theID, null);

        createTableCell(aNewLineItem, "td", null, null, 0, 3, description);

        createTableCell(aNewLineItem, "td", null, null, 0, 0, "date :");

        aNewBoxItem = createTableCell(aNewLineItem, "td", null, null, 0, 0, null);
        anInput = createInput(aNewBoxItem, "text", "date_" + curNbSessions, "date_" + curNbSessions, null, "linkCalendarTo(this); return false;");
        anInput.setAttribute("size", "10");
        anInput.setAttribute("readonly", "readonly");

        createTableCell(aNewLineItem, "td", null, null, 0, 3, null);

        aNewBoxItem = createTableCell(aNewLineItem, "td", null, null, 0, 3, null);
        anInput = document.createElement("select");
        anInput.setAttribute("name", "modeEvaluationId_" + curNbSessions);
        anInput.setAttribute("id", "modeEvaluationId_" + curNbSessions);
        var nbMode = modeeval.length;
        for (var i = 0; i < nbMode; i++) {
            var theModeId = modeeval[i].id;
            var theModeLib = modeeval[i].firstChild.data;
            addItemToSelect(anInput, theModeId, theModeLib);
        }
        aNewBoxItem.appendChild(anInput);
        aNewLineItem.appendChild(aNewBoxItem);

        aNewBoxItem = createTableCell(aNewLineItem, "td", null, null, 0, 0, null);
        createButton(aNewBoxItem, null, "saveSession", "Enregistrer", "saveSession(this);return false;");

        endListItem.parentNode.insertBefore(aNewLineItem, endListItem);

        // ---------------------------------------------------------------------
        aNewLineItem = createTableRow(null, "sessionLine");

        createTableCell(aNewLineItem, "td", null, null, 0, 0, "début :");

        aNewBoxItem = createTableCell(aNewLineItem, "td", null, null, 0, 0, null);
        anInput = document.createElement("select");
        anInput.setAttribute("name", "start_H_" + curNbSessions);
        anInput.setAttribute("id", "start_H_" + curNbSessions);
        for (var i = 0; i <= 23; i++) {
            addItemToSelect(anInput, i, i);
        }
        aNewBoxItem.appendChild(anInput);
        anInput = document.createElement("select");
        anInput.setAttribute("name", "start_M_" + curNbSessions);
        anInput.setAttribute("id", "start_M_" + curNbSessions);
        for (var i = 0; i <= 59; i++) {
            addItemToSelect(anInput, i, i);
        }
        aNewBoxItem.appendChild(anInput);
        aNewLineItem.appendChild(aNewBoxItem);

        aNewBoxItem = createTableCell(aNewLineItem, "td", null, null, 0, 2, null);
        createButton(aNewBoxItem, null, "removeSession", "Supprimer", "removeSession(this);return false;");

        endListItem.parentNode.insertBefore(aNewLineItem, endListItem);

        // ---------------------------------------------------------------------
        aNewLineItem = document.createElement("tr");
        aNewLineItem.setAttribute("class", "sessionLine");

        aNewBoxItem = createTableCell(aNewLineItem, "td", null, null, 0, 0, "durée :");

        aNewBoxItem = createTableCell(aNewLineItem, "td", null, null, 0, 0, null);
        anInput = document.createElement("select");
        anInput.setAttribute("name", "length_H_" + curNbSessions);
        anInput.setAttribute("id", "length_H_" + curNbSessions);
        for (var i = 0; i <= 23; i++) {
            addItemToSelect(anInput, i, i);
        }
        aNewBoxItem.appendChild(anInput);
        anInput = document.createElement("select");
        anInput.setAttribute("name", "length_M_" + curNbSessions);
        anInput.setAttribute("id", "length_M_" + curNbSessions);
        for (var i = 0; i <= 59; i++) {
            addItemToSelect(anInput, i, i);
        }
        aNewBoxItem.appendChild(anInput);
        aNewLineItem.appendChild(aNewBoxItem);

        endListItem.parentNode.insertBefore(aNewLineItem, endListItem);
        // ---------------------------------------------------------------------
    }

    // reset questionnaire list
    var qIdItem = document.getElementById("qId");
    qIdItem.selectedIndex = 0;
}

// -------------------------------------------------------------
function chargeQuestionnaire() {
    var xhr = getXMLHTTP();
    if (xhr !== null) {
        var parametre = prepareParametre("chargeQuestionnaire");
        parametre += addParametreFromInput("qId", "qId");
        sendAJAX(xhr, parametre, callBackChargeQuestionnaire);
    }
}

function getParentLine(curButton) {
    var lineItem = curButton;
    var found = false;
    while (!found) {
        if (lineItem === null) {
            found = true;
        } else if ((lineItem.tagName !== null) && (lineItem.tagName === "TR")) {
            found = true;
        } else {
            lineItem = lineItem.parentNode;
        }
    }
    return lineItem;
}

// -------------------------------------------------------------
function saveSession(curButton) {
    var xhr = getXMLHTTP();
    if (xhr !== null) {
        var start = getParametreFromSelectValue("start_H_" + idValue) + ":" + getParametreFromSelectValue("start_M_" + idValue);
        var length = getParametreFromSelectValue("length_H_" + idValue) + ":" + getParametreFromSelectValue("length_M_" + idValue);

        var parametre = prepareParametre("saveSession");
        parametre += addParametreFromInput("questionnaireId", "questionnaireId");
        parametre += addParametreFromInput("sessionId", "sessionId");
        parametre += addParametreFromInput("date", "date");
        parametre += addParametreFromInput("hDebut", "hDebut");
        parametre += addParametreFromInput("mDebut", "mDebut");
        parametre += addParametreFromInput("duree", "duree");
        parametre += addParametreFromInput("modeevaluationId", "modeevaluationId");

        // Launch AJAX
        prepareMessage(document.getElementById("message"));
        sendAJAX(xhr, parametre, callBackSaveSession);
    }
}

function changeItem(base, oldId, theId) {
    var item = document.getElementById(base + oldId);
    if (item !== null) {
        item.id = base + theId;
        item.name = base + theId;
    }
}

function callBackSaveSession(xhr) {
    var docXML = xhr.responseXML;

    var msgItem = document.getElementById("message");
    var session = docXML.getElementsByTagName("session");
    var nbValues = session.length;
    if (nbValues === 0) {
        setMessage(msgItem, 1, "not saved");
    } else {
        // Change IDs and items
	    var sessionItem = document.getElementById("sessionId");
	    sessionItem.value = session[0].textContent;

        setMessage(msgItem, 0, "saved");
    }
}


// -------------------------------------------------------------
function removeSession(curButton) {
    var xhr = getXMLHTTP();
    if (xhr !== null) {
        // Get the line
        var lineItem = curButton;
        var found = false;
        while (!found) {
            if ((lineItem.tagName !== null) && (lineItem.tagName === "TR")) {
                found = true;
            } else {
                lineItem = lineItem.parentNode;
            }
        }

        var idValue = lineItem.id.substr(8);
        var parametre = prepareParametre("removeSession");
        parametre += addParametre("sid", idValue);

        // Launch AJAX
        prepareMessage(document.getElementById("message"));
        sendAJAX(xhr, parametre, callBackRemoveSession);
    }
}

function callBackRemoveSession(xhr) {
    var docXML = xhr.responseXML;

    var msgItem = document.getElementById("message");
    var session = docXML.getElementsByTagName("session");
    var nbValues = session.length;
    if (nbValues === 0) {
        setMessage(msgItem, 1, "not removed");
    } else {
        setMessage(msgItem, 0, "removed");
    }
}
