/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getMyTRTag(ref) {
    var theTRRef = ref;
    var found = false;
    while ((theTRRef !== null) && (!found)) {
        if (theTRRef.nodeType === 3) {
            theTRRef = theTRRef.parentNode;
        } else if (theTRRef.tagName === "TR") {
            found = true;
        } else {
            theTRRef = theTRRef.parentNode;
        }
    }
    return theTRRef;
}

function changeMyTDTag(ref, style) {

    getMyTDTag(ref).className = style;
    /*ref.style.backgroundColor = "red";*/
}

function addUser() {
    $("#result").html("");
    $("#loginExistError").empty();
//    $("#loginExistError").html("");
    var formName = "addUserForm";
    var theForm = document.getElementById(formName);
    var Nom = theForm.elements[0].value;
    var Prenom = theForm.elements[1].value;
    var email = theForm.elements[2].value;
    var Login = theForm.elements[3].value;
    var password = theForm.elements[4].value;

    var roleId = theForm.elements[5].value;

    $.ajax({

        url: "AJAX.do",
        data: {
            "Nom": Nom,
            "Prenom": Prenom,
            "email": email,
            "Login": Login,
            "password": password,
            "roleId": roleId,

            "action": "addUser"
        },
        method: "POST",
        success: function (result) {
            if (result.loginExist === 1) {
                $('#loginExistError').prepend('Ce login existe déjà');
            } else {
                console.log("ok");
                $('#result').prepend(
                        Prenom + ' ' + Nom + ' ajouté avec succès');
                clearInputs(formName);
            }
        },
        error: function (resultat, statut, erreur) {
            console.log("error");
        }
    });

}



function clearInputs(formName) {
    document.getElementById(formName).reset();
}

function ajouterTableau(lines) {
    var l = lines.length;
    for (var i = 0; i < lines.length; i++) {
        var id = lines[i].id;
        var Email = lines[i].Email;
        var Nom = lines[i].Nom;
        var Prenom = lines[i].Prenom;
        var Login = lines[i].Login;
        var ProfilId = lines[i].ProfilId;
        addNewLine(id, Email, Nom, Prenom, Login, ProfilId);
    }
}

function removeLines() {
    $(".row").remove();
}

function addNewLine(id, Email, Nom, Prenom, Login, ProfilId) {
    var string = "<tr class=\"line\">";
    string += "<td>" + id + "</td>";
    string += "<td>" + ProfilId + "</td>";
    string += "<td>" + Nom + "</td>";
    string += "<td>" + Prenom + "</td>";
    string += "<td>" + Email + "</td>";
    string += "<td>" + Login + "</td>";
    string += '<td><button class="btn"><i class="fa fa-edit"></i></button>';
    string += '<button class="btn"><i class="fa fa-trash"></i></button></td>';
    string += "</tr>";

    $('#tableau tr:last').before(string);
}

function deleteLine(theTDRef, id) {
    $.ajax({
        url: "AJAX.do",
        data: {
            "id": id,
            "action": "deleteUser"
        },
        method: "POST",
        success: function (result) {
            theTRRef = getMyTRTag(theTDRef);
            if (theTRRef !== null) {
                theTRRef.parentNode.removeChild(theTRRef);
            }
            console.log("delete done");
        },
        error: function (result, statut, erreur) {
            console.log("delete error");
        }
    });
}

function editLine(ref, id) {
    $.ajax({
        url: "AJAX.do",
        data: {
            "id": id,
            "action": "obtainProfil"
        },
        method: "POST",
        success: function (result) {
            console.log(result);
            var profil = result.profil;
            editMode(ref, id, profil);

        },
        error: function (result, statut, erreur) {
            console.log("error");
        }
    });
}

function editMode(ref, id, typequest, typeacces) {
    $(ref).children().removeClass().addClass("fas fa-check");
    $(ref).next().children().removeClass().addClass("fas fa-minus");


    $(ref).parent().parent().children().each(function () {
        // td not contain button
        if ($(this).attr("class") != "operationButton" && $(this).attr("scope") != "row") {
            $(this).html($('<input>', {
                type: "text",
                class: 'form-control',
                name: $(this).attr("class"),
                value: $(this).html(),
                placeholder: $(this).html()
            }));
            if ($(this).attr("class") == "profil") {
                // console.log($(this).children().attr("placeholder"));
                var profil_chosen = $(this).children().attr("placeholder");
                $(this).children().attr("type", "hidden");
                var sel = $('<select>', {
                    class: "form-control form-control-sm",
                    name: "profil_"
                });
                $(typequest).each(function () {
                    // sel.append($('<option>').attr("value", this.id).text(this.libelle));
                    if (this.libelle == profil_chosen) {
                        sel.append($('<option>', {value: this.id, selected: "selected"}).text(this.libelle));
                    } else {
                        sel.append($('<option>', {value: this.id}).text(this.libelle));
                    }

                });
                $(this).append(sel);
            }
        }

        // if ($(this).children().length == 0 && $(this).attr("scope")!= "row")
        //     $(this).html($('<input>', {
        //         type: 'text',
        //         class: 'form-control',
        //         value: $(this).html(),
        //         placeholder: $(this).html()
        //     }));
    });
    $(ref).attr("onclick", "sendData(this, " + id + ")");
    $(ref).next().attr("onclick", "rowReset(this, " + id + ")");
}
function sendData(ref, id) {
    var data = {};
    try {
        $(ref).parent().parent().children().each(function () {
            if ($(this).attr("class") != "operationButton" && $(this).attr("scope") != "row") {
                if ($(this).attr("class") == "profil") {
                    // alert($(this).children("select").find(":selected").text());
                    // console.log($(this).children("select").find(":selected").text());
                    data[$(this).attr("class")] = $(this).children("select").val();

                } else {
                    // console.log($(this).children(":input").val());
                    data[$(this).attr("class")] = $(this).children(":input").val();

                }

            }

        });
        // console.log(data);
        if (data["personneUid"] == "") {
            throw "le login ne peut pas être vide";
        }
        if (data["personneNom"] == "") {
            throw "le nom ne peut pas être vide";
        }
        if (data["personnePrenom"] == "") {
            throw "le prenom ne peut pas être vide";
        }
        if (data["personneEmail"] == "") {
            throw "l'email ne peut pas être vide";
        }
        $.ajax({
            url: "AJAX.do",
            data: {
                "id": id,
                "action": "editPersonne",
                "data": JSON.stringify(data)
            },
            method: "POST",
            success: function (result) {
                console.log(result);
                saveRow(ref, id);


            },
            error: function (result, statut, erreur) {
                console.log("error");
            }
        });
    } catch (err) {
        alert(err);
        $(ref).parent().parent().children().each(function () {
            if ($(this).attr("class") == "personneUid") {
                $(this).children("input").attr("style", "border-color:rgb(255, 0, 0);box-shadow: 0 0 0 0.2rem rgba(255, 0, 0, 0.25);");
            }
        });
    }

}





function rowReset(ref, id) {
    $(ref).children().removeClass().addClass("fas fa-trash-alt");
    $(ref).prev().children().removeClass().addClass("fas fa-edit");
    $(ref).parent().parent().children().each(function () {
        if ($(this).attr("class") != "operationButton" && $(this).attr("scope") != "row") {
            $(this).html($(this).children(":input").attr("placeholder"));
        }

    });
    $(ref).attr("onclick", "deleteLine(this, " + id + ")");
    $(ref).prev().attr("onclick", "editLine(this, " + id + ")");
}

function saveRow(ref, id) {
    $(ref).next().children().removeClass().addClass("fas fa-trash-alt");
    $(ref).children().removeClass().addClass("fas fa-edit");
    $(ref).parent().parent().children().each(function () {
        if ($(this).attr("class") != "operationButton" && $(this).attr("scope") != "row") {
            if ($(this).attr("class") == "profil") {
                // alert($(this).children("select").find(":selected").text());
                // console.log($(this).children("select").find(":selected").text());
                $(this).html($(this).children("select").find(":selected").text());

            } else {
                // console.log($(this).children(":input").val());
                $(this).html($(this).children(":input").val());

            }

        }

    });
    $(ref).next().attr("onclick", "deleteLine(this, " + id + ")");
    $(ref).attr("onclick", "editLine(this, " + id + ")");
}