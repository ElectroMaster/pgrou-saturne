/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function getMyTag(ref) {
  var theTDRef = ref;
  var found = false;
  while (theTDRef !== null && !found) {
    if (theTDRef.nodeType == 3) {
      theTDRef = theTDRef.parentNode;
    } else if (theTDRef.tagName == "TR") {
      found = true;
    } else {
      theTDRef = theTDRef.parentNode;
    }
  }
  return theTDRef;
}

function deleteSession(ref,id) {
  $.ajax({
    url: "AJAX.do",
    data: {
      "action": "deleteSession",
      "id": id
    },
    method: "POST",
    success: function(result) {
      var theTR = getMyTag(ref);
      if(theTR!=null){
          var parent = theTR.parentNode;
          parent.removeChild(theTR);
      }
      console.log("delete done");
    },
    error: function(resultat, status, erreur) {
      console.log("delete error");
    }
  });
}

function editLine(ref, id) {
    $.ajax({
        url: "AJAX.do",
        data: {
            "id": id,
            "action": "obtainQuestionnaireAndModeevaluation"
        },
        method: "POST",
        success: function(result) {
            console.log(result);
            var quest = result.questionnaire;
            var modeeval = result.modeevaluation;
            editMode(ref, id, quest, modeeval);

        },
        error: function(result, statut, erreur) {
            console.log("error");
        }
    });
}

function editMode(ref, id, quest, modeeval){
    $(ref).children().removeClass().addClass("fas fa-check");
    $(ref).next().children().removeClass().addClass("fas fa-minus");


    $(ref).parent().parent().children().each(function() {
        // td not contain button
        if ($(this).attr("class")!="operationButton" && $(this).attr("scope")!="row"){
            $(this).html($('<input>', {
                type: "text",
                class: 'form-control',
                name: $(this).attr("class"),
                value: $(this).html(),
                placeholder: $(this).html()
            }));
            if($(this).attr("class") == "questionnaire"){
                // console.log($(this).children().attr("placeholder"));
                var questionnaire_chosen = $(this).children().attr("placeholder");
                $(this).children().attr("type","hidden");
                var sel = $('<select>',{
                    class: "form-control form-control-sm",
                    name: "questionnaire"
                });
                $(quest).each(function(){
                    // sel.append($('<option>').attr("value", this.id).text(this.libelle));
                    if (this.libelle == questionnaire_chosen){
                        sel.append($('<option>',{value: this.id, selected: "selected"}).text(this.libelle));
                    } else {
                        sel.append($('<option>',{value: this.id}).text(this.libelle));
                    }

                });
                $(this).append(sel);
            }

            if($(this).attr("class") == "modeevaluation"){
                var modeevaluation_chosen = $(this).children().attr("placeholder");
                $(this).children().attr("type","hidden");
                var sel = $('<select>',{
                    class: "form-control form-control-sm",
                    name: "modeevaluation"
                });
                $(modeeval).each(function(){
                    if (this.libelle == modeevaluation_chosen){
                        sel.append($('<option>', {value: this.id, selected: "selected"}).text(this.libelle));
                    } else {
                        sel.append($('<option>', {value: this.id}).text(this.libelle));
                    }

                });
                $(this).append(sel);
            }
        }

        // if ($(this).children().length == 0 && $(this).attr("scope")!= "row")
        //     $(this).html($('<input>', {
        //         type: 'text',
        //         class: 'form-control',
        //         value: $(this).html(),
        //         placeholder: $(this).html()
        //     }));
    });
    $(ref).attr("onclick", "sendData(this, "+id+")");
    $(ref).next().attr("onclick", "rowReset(this, "+id+")");
}

function sendData(ref, id){
    var data = {};
    try{
        $(ref).parent().parent().children().each(function() {
            if ($(this).attr("class") != "operationButton" && $(this).attr("scope")!= "row"){
                if($(this).attr("class") == "questionnaire" || $(this).attr("class") == "modeevaluation"){
                    // alert($(this).children("select").find(":selected").text());
                    // console.log($(this).children("select").find(":selected").text());
                    data[$(this).attr("class")] = $(this).children("select").val();

                } else{
                    // console.log($(this).children(":input").val());
                    data[$(this).attr("class")] = $(this).children(":input").val();

                }

            }

        });
        // console.log(data);
        if(data["questionnaire"] == ""){
            throw "le nom du questionnaire ne peut pas être vide";
        }
        $.ajax({
            url: "AJAX.do",
            data: {
                "id": id,
                "action": "editSession",
                "data": JSON.stringify(data)
            },
            method: "POST",
            success: function(result) {
                console.log(result);
                saveRow(ref, id);
            },
            error: function(result, statut, erreur) {
                console.log("error");
            }
        });
    } catch(err){
        alert(err);
        $(ref).parent().parent().children().each(function() {
        if ($(this).attr("class") == "questionnaire"){
            $(this).children("input").attr("style", "border-color:rgb(255, 0, 0);box-shadow: 0 0 0 0.2rem rgba(255, 0, 0, 0.25);");
        }});
    }

}





function rowReset(ref, id){
    $(ref).children().removeClass().addClass("fas fa-trash-alt");
    $(ref).prev().children().removeClass().addClass("fas fa-edit");
    $(ref).parent().parent().children().each(function() {
        if ($(this).attr("class") != "operationButton" && $(this).attr("scope")!= "row"){
            $(this).html($(this).children(":input").attr("placeholder"));
        }

    });
    $(ref).attr("onclick", "deleteLine(this, "+id+")");
    $(ref).prev().attr("onclick", "editLine(this, "+id+")");
}

function saveRow(ref, id){
    $(ref).next().children().removeClass().addClass("fas fa-trash-alt");
    $(ref).children().removeClass().addClass("fas fa-edit");
    $(ref).parent().parent().children().each(function() {
        if ($(this).attr("class") != "operationButton" && $(this).attr("scope")!= "row"){
            if($(this).attr("class") == "questionnaire" || $(this).attr("class") == "modeevaluation"){
                // alert($(this).children("select").find(":selected").text());
                // console.log($(this).children("select").find(":selected").text());
                $(this).html($(this).children("select").find(":selected").text());

            } else{
                // console.log($(this).children(":input").val());
                $(this).html($(this).children(":input").val());

            }

        }

    });
    $(ref).next().attr("onclick", "deleteLine(this, "+id+")");
    $(ref).attr("onclick", "editLine(this, "+id+")");
}
