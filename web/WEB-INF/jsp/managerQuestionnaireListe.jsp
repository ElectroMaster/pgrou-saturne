<%--
    Document   : managerQuestionnaireListe
    Created on : 2019-3-9, 19:02:56
    Author     : exia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <!-- Bootstrap -->
        <link rel="stylesheet" href="css/custom-font.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!--<link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">-->
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
<!--        <script src="js/bootstrap.min.js" type="text/javascript"></script>-->
        <script src="js/managerQuestionnaireListe.js" type="text/javascript"></script>
        <title>Saturne</title>
    </head>
    <body>
    <form>
        <h1>Liste des Questionnaires</h1>
        <table class="table table-hover table-striped">
            <thead class="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Libelle</th>
                <th scope="col">Durée</th>
                <th scope="col">Type</th>
                <th scope="col">Description</th>
                <th scope="col">Niveau de visibilité</th>
                <th scope="col">Propriétaire</th>
                <th scope="col">Opérations</th>
              </tr>
            </thead>
          <tbody>
              <c:forEach var="questionnaire" items="${questionnaireList}">
                  <tr>
                    <th scope="row"><c:out value="${questionnaire.questionnaireId}"/></th>
                    <td class="questionnaireLibelle"><c:out value="${questionnaire.questionnaireLibelle}"/></td>
                    <td class="questionnaireDuree"><fmt:formatDate pattern = "hh:mm" value = "${questionnaire.questionnaireDuree}" /></td>
                    <td class="typeQuestionnaire"><c:out value="${questionnaire.typequestionnaireId.typequestionnaireLibelle}"/></td>
                    <td class="questionnaireDescription"><c:out value="${questionnaire.questionnaireDescription}"/></td>
                    <td class="typeAcces"><c:out value="${questionnaire.typeaccesId.typeaccesLibelle}"/></td>
                    <td class="personneNom"><c:out value="${questionnaire.personneId.personneNom}"/></td>
                    <td class="operationButton">
                        <button type="button" class="btn btn-light" onclick="editLine(this, ${questionnaire.questionnaireId})">
                            <i class="fas fa-edit"></i>
                        </button>
                        <button type="button" class="btn btn-light" onclick="deleteLine(this, ${questionnaire.questionnaireId})">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </td>
                  </tr>
              </c:forEach>
          </tbody>
        </table>
    </form>
    </body>
</html>
