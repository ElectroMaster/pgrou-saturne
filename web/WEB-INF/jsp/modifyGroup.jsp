<%-- 
    Document   : modifyGroup
    Created on : 2019-2-28, 10:01:05
    Author     : GUO
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <link href="css/menu.css" type="text/css" rel="stylesheet">
        <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <title>Saturne</title>
    </head>
    <body>
    <form>
        <input type="hidden" name="id" id="id" value="${connexionId}" />
        <h1>Affectations à un groupe</h1>
        <div class="form-group" style="width:20%;">
            <label for="gorigine">Groupe d'origine</label>
            <select class="form-control" id="gorigine">
                <!--TODO:-->
                <option value="1">- Aucun Groupe -</option>
                <option value="2">- Toutes les personnes -</option>
                <c:forEach var="group" items="${groups}"><option value="${group.groupId}">${group.groupLiballe}</option>
                </c:forEach>
            </select>
        </div>
        <div class="form-group" style="width:20%;">
            <label for="gaffect">Groupe d'affectation</label>
            <select class="form-control" id="gaffect">
                <!--TODO:-->
                <option value="1"> - </option>
                <c:forEach var="group" items="${groups}"><option value="${group.groupId}">${group.groupLiballe}</option>
                </c:forEach>

            </select>
        </div>
        <hr/>
        <div class="form-group" style="width:20%;">
            <c:forEach var="user" items="${users}">
                <c:if test="${user.personneId != 1}">
                ${user.personneNom} ${user.personnePrenom}<br/>
                </c:if>
            </c:forEach>
        </div>
        <hr/>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </form>

    </body>
</html>
