<%-- 
    Document   : createQuestionnaire2
    Created on : 2019-3-3, 18:33:29
    Author     : exia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/custom-font.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/questionnaire.css">
    <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
    <title>Saturne</title>
</head>

<body>

    
    <h1>Créer un questionnaire</h1>
    <div class="card-group" style="width: 70%; margin: auto;">
        <div class="card">
            <div class="card-body card-no-active">
                <h5 class="card-title">Step 1:</h5>
                <p class="card-text">Créer un questionnaire</p>
                <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Step 2:</h5>
                <p class="card-text">Ajouter des questions</p>
                <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
            </div>
        </div>
    </div>

    <br>
    <br>






</body>

</html>
