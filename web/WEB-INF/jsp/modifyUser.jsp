<%-- 
    Document   : modifyUser
    Created on : 2019-2-28, 8:41:51
    Author     : exia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <link href="css/menu.css" type="text/css" rel="stylesheet">
        <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <title>Saturne</title>
    </head>
    <body>
    <form>
        <h1>Modifier un utilisateur</h1>
        <div class="form-group">
            <label for="login">Utilisateur</label>
            <input type="text" class="form-control" id="login" name="uid" aria-describedby="utilisateurHelp" placeholder="login name">
            <small id="utilisateurHelp" class="form-text text-muted">Entrer le nom de login que vou voulez changer</small>
        </div>
        <div class="row">
            <div class="col">
                <label for="nom">Nom</label>
                <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom">
            </div>
            <div class="col">
                <label for="prenom">Prenom</label>
                <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prenom">
            </div>
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
        </div>

        <div class="form-group" style="width:20%;">
            <label for="role">Rôle</label>
            <select class="form-control" id="role">
                <!--TODO:-->
                <option value="1"> Administrateur</option>
                <option value="2"> Direction</option>
                <option value="3"> Enseignant</option>
                <option value="4"> Elève</option>
            </select>
        </div>
        
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </form>

    </body>
</html>
