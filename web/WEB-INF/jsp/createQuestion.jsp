<%-- 
    Document   : createQuestion
    Created on : 28 févr. 2019, 11:44:32
    Author     : antoinehurard
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="css/custom-font.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script src="js/createQuestion.js" type="text/javascript"></script>
        <title>Saturne</title>
    </head>
    <body>
        <h1>Créer une question</h1>
        <form:form action="createQuestion.do" method="POST" onsubmit="return validateMyForm();">
            <input type="hidden"  name="connexionId" value="${connexionId}">
            <div class="form-group" style="height:5em;">
                <label for="libelle_question">Libellé</label>
                <input type="text" class="form-control" id="libelle_question" name="libelle_question" placeholder="Libellé de la question">
                <div class="invalid-feedback">
                Le libellé ne doit pas être vide.
                </div>
                <div class="valid-feedback">
                Libellé ok.
                </div>
            </div>

            <div class="form-group" style="width:20%;display:block;">
                <label for="type_question">Type</label>
                <select class="form-control form-control-sm" id="type_question" name="type_question"
                        onchange="changeTypeQuestion()">
                    <c:forEach var="typeQuestion" items="${listTypequestion}">
                        <c:choose>
                            <c:when test="${typeQuestion.typequestionLibelle == 'Choix unique'}">
                                <option value="${typeQuestion.typequestionId}" selected="true">
                                ${typeQuestion.typequestionLibelle}
                                </option>
                            </c:when>
                            <c:otherwise>
                                <option value="${typeQuestion.typequestionId}">
                                ${typeQuestion.typequestionLibelle}
                                </option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <label style="display:block;" id="header_mot_cle" for="mot_cle">Mot-clés</label>
                <div id="mot_cle_invalid" class="invalid-feedback">
                    Le mot-clé est déjà ajouté.
                </div>
                <div id="mot_cle_valid" class="valid-feedback">
                    Libellé ok.
                </div>
                <button style="border-radius: 50%;" type="button" id="mot_cle" class="btn btn-light" onclick="addMot(this)"><i class="fa fa-plus fw"></i></button>
            </div>
            <div style="width:20%;display:block;" class="form-group">
                <select class="form-control form-control-sm" name="selectMotcle" id="selectMotcle">
                    <c:forEach var="aMotcle" items="${listMotcle}">
                        <option value="${aMotcle.motcleId}">${aMotcle.motcleLibelle}</option>
                    </c:forEach>
                </select>
                <span>
                    <button type="button" class="btn btn-primary" 
                            name="addSelectedKeyword" onclick="addSelection()">Ajouter le mot clé</button>
                </span>
            </div>
            <div class="form-group" id="answersDiv">
                <label style="display:block;" for="reponse_possible">Réponses possibles</label>
                <button style="border-radius: 50%;" type="button" id="answer" class="btn btn-light" onclick="addAnswer(this)"><i class="fa fa-plus fw"></i></button>
                <ul id="liste_reponses" class="list-group">
                </ul>
            </div>
            <input type="hidden"  id="inputJSON" name="inputJSON" value="{}">
            <div class="form-group" id="uniqueAnswerDiv" style="display:none;">
                <label label style="display:block;" for="reponse_litterale">Réponse littérale</label>
                <input type="text" class="form-control" id="reponse_litterale" name="reponse_litterale" placeholder="Entrez la réponse attendue">
            </div>
            <div class="form-group">
                <label for="visibility_level">Niveau de visibilité</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="visibility_level" id="gridRadios1" value="3" checked>
                    <label style="margin-left:20px;" class="form-check-label" for="gridRadios1">
                        <i aria-hidden="true" class="fa fa-lock fa-fw"></i>
                        Privé
                        <small style="display:block;">L'accès à la question doit être explicitement admis pour chaque utilisateur.</small>
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="visibility_level" id="gridRadios2" value="2">
                    <label style="margin-left:20px;" class="form-check-label" for="gridRadios2">
                        <i aria-hidden="true" class="fas fa-shield-alt"></i>
                        Restreint
                        <small style="display:block;">La question est accessible à tout utilisateur connecté.</small>
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="visibility_level" id="gridRadios3" value="1">
                    <label style="margin-left:20px;" class="form-check-label" for="gridRadios3">
                        <i aria-hidden="true" class="fas fa-globe-europe"></i>
                        Public
                        <small style="display:block;">La question est accessible sans aucune authentification.</small>
                    </label>
                </div>
            </div>
            <button type="submit" name="button" class="btn btn-primary">Enregistrer</button>
        </form:form>
    </body>
</html>
