<%-- 
    Document   : consulterListe
    Created on : 2019-2-28, 9:16:23
    Author     : exia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!--<link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">-->
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <title>Saturne</title>
    </head>
    <body>
        <form>
            <h1>Liste des utilisateurs</h1>
            <table class="table table-hover table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Rôle</th>
                        <th scope="col">Nom</th>
                        <th scope="col">Prénom</th>
                        <th scope="col">Email</th>
                        <th scope="col">Login</th>
                        <th scope="col">PersonneValide</th>
                    </tr>
                </thead>
                <tbody>

                    <c:forEach var="personne" items="${personneList}">
                        <tr>
                            <th scope="row"><c:out value="${personne.personneId}"/></th>
                            <td class="profil"><c:out value="${personne.profilId.profilLibelle}"/></td>
                            <td class="personneNom"><c:out value="${personne.personneNom}"/></td>
                            <td class="personnePrenom"><c:out value="${personne.personnePrenom}"/></td>
                            <td class="personneEmail"><c:out value="${personne.personneEmail}"/></td>
                            <td class="personneUid"><c:out value="${personne.personneUid}"/></td>
                            <td class="personneValide"><c:out value="${personne.personneValide}"/></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </form>
    </body>
</html>
