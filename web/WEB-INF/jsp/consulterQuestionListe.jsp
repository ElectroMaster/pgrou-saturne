<%-- 
    Document   : consulterQuestionListe
    Created on : 15 mars 2019, 17:34:27
    Author     : antoinehurard
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/custom-font.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <title>Saturne</title>
    </head>
    <body>
        <h1>Liste des Questions</h1>
        <table class="table table-hover table-striped">
            <thead class="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Libellé</th>
                <th scope="col">Type</th>
                <th scope="col">Réponses</th>
                <th scope="col">Mot-clés</th>
                <th scope="col">Niveau de visibilité</th>
                <th scope="col">Validité</th>
                <th scope="col">Propriétaire</th>
              </tr>
            </thead>
          <tbody>
              <c:forEach var="question" items="${questionList}">
                  <tr>
                    <th scope="row"><c:out value="${question.questionId}"/></th>
                    <td><c:out value="${question.questionTexte}"/></td>
                    <td><c:out value="${question.typequestionId.typequestionLibelle}"/></td>
                    <td>
                        <c:choose>
                            <c:when test="${!empty question.reponsepossibleCollection}">
                                <ul>
                                    <c:forEach var="reponse" items="${question.reponsepossibleCollection}">
                                        <c:choose>
                                            <c:when test="${reponse.reponsepossibleEstsolution}">
                                                <li style="color: green;"><c:out value="${reponse.reponsepossibleTexte}"/></li>
                                            </c:when>
                                            <c:otherwise>
                                                <li style="color:red;"><c:out value="${reponse.reponsepossibleTexte}"/></li>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </ul>
                            </c:when>
                            <c:otherwise>
                                <c:if test="${not empty question.questionSolutiontexte}">
                                    <span style="color:green"><c:out value="${question.questionSolutiontexte}"/></span>
                                </c:if>
                                <c:if test="${not empty question.questionSolutionentier}">
                                    <span style="color:green"><c:out value="${question.questionSolutionentier}"/></span>
                                </c:if>
                                <c:if test="${not empty question.questionSolutionreel}">
                                    <span style="color:green"><c:out value="${question.questionSolutionreel}"/></span>
                                </c:if>
                             </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <ul>
                            <c:forEach var="motcle" items="${question.motcleCollection}">
                                <li><c:out value="${motcle.motcleLibelle}"/></li>
                            </c:forEach>
                        </ul>
                    </td>
                    <td><c:out value="${question.typeaccesId.typeaccesLibelle}"/></td>
                    <td>
                        <c:choose>
                            <c:when test="${question.questionValide}">
                                <i class="fas fa-check" style="color: green;"></i>
                            </c:when>
                            <c:otherwise>
                                <i class="fas fa-times" style="color: red;"></i>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td><c:out value="${question.personneId.personneNom}"/></td>
                  </tr>
              </c:forEach>
          </tbody>
        </table>
    </body>
</html>
