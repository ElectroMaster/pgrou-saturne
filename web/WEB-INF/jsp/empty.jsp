<%-- 
    Document   : empty
    Created on : 2019-2-27, 14:34:19
    Author     : exia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <!--link href="css/menu.css" type="text/css" rel="stylesheet"-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <title>Saturne</title>
    </head>
    <body>
        <h1>Empty page!</h1>
    </body>
</html>
