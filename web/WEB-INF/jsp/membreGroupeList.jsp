<%-- 
    Document   : membreGroupeList
    Created on : 10 mars 2019, 17:12:04
    Author     : thibault
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/groupe.js" type="text/javascript"></script>
        <title>JSP Page</title>
    </head>
    <body>
        <p><a href="load.do?id=${connexionId}&page=managerGroupListe" target="subframe">Retour</a></p>

        <h1>${group.groupeLibelle}</h1>
        <p id="rechercherMembre"><button onclick="rechercherMembreGroupe('${connexionId}','${group.groupeId}','${manager}')">Rechercher</button>
        <a href="load.do?id=${connexionId}&page=membreGroupeList&groupeId=${group.groupeId}&manager=${manager}" target="subframe">Tous les membres</a></p>

        
        <table id="idtableau" class="table table-hover table-striped">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Nom</th>
                    <th scope="col">Prenom</th>
                    <th scope="col">Email</th>
                        <c:if test="${manager}">
                        <th scope="col">Action</th>
                        </c:if>

                </tr>
            </thead>
            <tbody>
                <c:if test="${manager}">
                    <form:form action="addMembre.do" method="POST">
                    <input type="hidden" name="connexionId" value="${connexionId}">
                    <input type="hidden" name="groupeId" value="${group.groupeId}">
                    <tr>
                        <td colspan="3">
                            <select name="personneId">
                                <option value=-1>Ajouter un membre au groupe</option>
                                <c:forEach var="membre" items="${personnes}">
                                    <c:if test="${membre.profilId.profilId==4}">
                                        <option value="${membre.personneId}">${membre.personneNom} ${membre.personnePrenom}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </td>
                        <td>
                            <button>Add</button>
                        </td>
                    </tr>
                </form:form>  
            </c:if>   
            <c:forEach var="user" items="${personneCollection}">
                <tr>
                    <td>${user.personneNom}</td>
                    <td>${user.personnePrenom}</td>
                    <td>${user.personneEmail}</td>
                    <c:if test="${manager}">
                        <td><button class="btn" onclick="deletePersonne(this,${group.groupeId},${user.personneId});"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Supprimer" ></i></button></td>
                            </c:if>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</body>
</html>
