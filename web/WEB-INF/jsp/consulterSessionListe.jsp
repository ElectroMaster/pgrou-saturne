<%-- 
    Document   : consulterSessionListe
    Created on : 2019-3-19, 21:37:49
    Author     : GUO
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/custom-font.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <title>Saturne</title>
    </head>
    <body>
    <form>
        <h1>Liste des Sessions</h1>
        <table class="table table-hover table-striped">
            <thead class="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Libelle de questionnaire</th>
                <th scope="col">Date de l'examen</th>
                <th scope="col">Début</th>
                <th scope="col">Durée</th>
                <th scope="col">Mode évaluation</th>
              </tr>
            </thead>
          <tbody>
              <c:forEach var="evaluation" items="${evaluationList}">
                  <tr>
                    <th scope="row"><c:out value="${evaluation.evaluationId}"/></th>
                    <td><c:out value="${evaluation.questionnaireId.questionnaireLibelle}"/></td>
                    <td><fmt:formatDate type = "date" value="${evaluation.evaluationDate}"/></td>
                     <td><fmt:formatDate pattern = "hh:mm" value = "${evaluation.evaluationDepart}" /></td>
                    <td><fmt:formatDate pattern = "hh:mm"  value="${evaluation.evaluationDuree}"/></td>
                    <td><c:out value="${evaluation.modeevaluationId.modeevaluationLibelle}"/></td>
                  </tr>
              </c:forEach>
          </tbody>
        </table>
    </form>
    </body>
</html>
