<%-- 
    Document   : creatGroup
    Created on : 2019-2-28, 9:38:20
    Author     : GUO
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <link href="css/menu.css" type="text/css" rel="stylesheet">
        <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <title>Saturne</title>
    </head>
    <body>
    <form action="groupe.do" method="POST">
        <input type="hidden" name="id" id="id" value="${connexionId}" />
        <h1>Créer un groupe</h1>
        <div class="form-group">
            <label for="libelle">Libellé</label>
            <input autofocus type="text" name="groupeLibelle" class="form-control" id="libelle">
            <input type="hidden" name="groupeId" id="groupeId" value="-1" />
        </div>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
        <button type="submit" class="btn btn-primary">Enregistrer et suivant</button>
        <span id="message"></span>
    </form>

    </body>
</html>

