<%-- 
    Document   : modifyQuestionnaire
    Created on : 2019-3-13, 13:34:41
    Author     : exia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="css/custom-font.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/questionnaire.css">
    <link rel="stylesheet" href="css/modifyQuestionnaire.css">
    <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script src="https://d3js.org/d3.v5.min.js"></script>
    <script src="js/modifyQuestionnaire.js" type="text/javascript"></script>
    <title>Saturne</title>
</head>

<body>
    <h1>Mes Questionnaires</h1>
    <ul class="list-group">
        <c:forEach var="questionnaire" items="${questionnaireList}">

        <li class="list-group-item item">
            <div class="flex-container-row">
                <div class="avater-div" id="${questionnaire.questionnaireId}">
                    <a href="load.do?id=${connexionId}&page=modifyQuestionnaire2&questionnaireId=${questionnaire.questionnaireId}" class="avater-link">${fn:toUpperCase(fn:substring(questionnaire.questionnaireLibelle, 0, 1))}</a>
                </div>
                <script>
                change_status_color("${questionnaire.questionnaireId}", "${fn:toUpperCase(fn:substring(questionnaire.questionnaireLibelle, 0, 1))}");
                </script>
                <div class="flex-container-col">
                    <div class="flex-wrapper">
                        <a href="load.do?id=${connexionId}&page=modifyQuestionnaire2&questionnaireId=${questionnaire.questionnaireId}" class="project-name">
                            <span class="namespace-name">${questionnaire.personneId.personneNom} / </span>
                            <span class="questionnaire-name">${questionnaire.questionnaireLibelle}</span>
                        </a>
                            <c:choose>
                                <c:when test="${questionnaire.typeaccesId.typeaccesLibelle == 'Public'}">
                                    <span class="icon-acces"><i aria-hidden="true" class="fas fa-globe-europe"></i></span>
                                    <span class="label-acces">Public</span>
                                </c:when>
                                <c:when test="${questionnaire.typeaccesId.typeaccesLibelle == 'Restreint'}">
                                    <span class="icon-acces"><i aria-hidden="true" class="fas fa-shield-alt"></i></span>
                                    <span class="label-acces">Restreint</span>
                                </c:when>
                                <c:when test="${questionnaire.typeaccesId.typeaccesLibelle == 'Privé'}">
                                    <span class="icon-acces"><i aria-hidden="true" class="fa fa-lock fa-fw"></i></span>
                                    <span class="label-acces">Privé</span>
                                </c:when>
                                <c:otherwise>
                                    
                                </c:otherwise>
                            </c:choose>
                        
                                    <c:choose>
                                        <c:when test="${questionnaire.questionnaireDescription == '' || questionnaire.questionnaireDescription==null}">
                                            <p class="quest-description">&nbsp;</p>
                                        </c:when>
                                    <c:otherwise>
                                        <p class="quest-description">${questionnaire.questionnaireDescription}</p>
                                    </c:otherwise>
                                    </c:choose>
                        
                    </div>
                    <div class="type-acces">
                        <span class="icon-type"><i class="fas fa-angle-right"></i></i></span>
                        <span class="quest-type-name">${questionnaire.typequestionnaireId.typequestionnaireLibelle}</span>
                    </div>
                </div>
                <div class="quest-time-div">
                    <span class="icon-time"><i class="far fa-clock"></i></span>
                    <span class="quest-time"><fmt:formatDate pattern = "hh:mm" value = "${questionnaire.questionnaireDuree}" /></span>
                </div>
            </div>
        </li>
            
        </c:forEach>
    </ul>
</body>

</html>
