<%-- 
    Document   : createUser
    Created on : 2019-2-27, 15:14:20
    Author     : exia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/user.js" type="text/javascript"></script>
        <title>Saturne</title>
    </head>
    <body>
        <form id="addUserForm">
            <h1>Créer un utilisateur</h1>
            <div class="row">
                <div class="col">
                    <label>Nom</label>
                    <input type="text" class="form-control" name="nom" placeholder="Nom">
                </div>
                <div class="col">
                    <label>Prenom</label>
                    <input type="text" class="form-control" name="prenom" placeholder="Prenom">
                </div>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="email">
            </div>
            <div class="form-group">
                <label for="login">Login</label>
                <input type="text" class="form-control" id="login" name="uid" placeholder="Login">
                <div class="text-danger" id="loginExistError"></div>
            </div>
            <c:choose>
                <c:when test="${not empty message['loginFeedback']}">
                    <div class="invalid-feedback">
                        ${message['loginFeedback']}
                    </div>
                </c:when>
                <%--<c:otherwise>&nbsp;</c:otherwise>--%>
            </c:choose>

            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
            </div>
            <c:choose>
                <c:when test="${not empty message['pwdFeedback']}">
                    <div class="invalid-feedback">
                        ${message['pwdFeedback']}
                    </div>
                </c:when>
                <%--<c:otherwise>&nbsp;</c:otherwise>--%>
            </c:choose>
            <div class="form-group" style="width:20%;">
                <label for="role">Rôle</label>
                <select class="form-control" id="role">
                    <!--TODO:-->
                    <option value="1"> Administrateur</option>
                    <option value="2"> Direction</option>
                    <option value="3"> Enseignant</option>
                    <option value="4"> Elève</option>
                </select>
            </div>

            <input type="button" onclick="addUser()" value="Enregistrer" class="btn btn-primary"></button>
        </form>
        <div class="mx-auto">
            <div class="text-success" id="result"></div>
        </div>
    </body>
</html>
