<%-- 
    Document   : managerGroupListe
    Created on : 2019-2-28, 10:14:58
    Author     : GUO
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="js/groupe.js" type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <title>Saturne</title>
    </head>
    <body>

        <input type="hidden" name="id" id="id" value="${connexionId}" />
        <h1>Liste des groupes</h1>
        <p id="rechercher"><button onclick="rechercherGroupe('${connexionId}','${mot}')">Rechercher</button>
        <a href="load.do?id=${connexionId}&page=managerGroupListe" target="subframe">Tous les groupes</a></p>

        <table class="table table-hover table-striped">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Libellé</th>
                    <th scope="col">Opérations</th>
                </tr>
            </thead>
            <tbody>

                <c:forEach var="group" items="${groupeList}">
                    <tr id="${group.groupeId}" <c:if test="${group.groupeValide==false}">style="background-color:lightcoral"</c:if>>
                        <td>${group.groupeId}</td>
                        <td id="libelle${group.groupeId}"><a href="load.do?id=${connexionId}&page=membreGroupeList&groupeId=${group.groupeId}&manager=true">${group.groupeLibelle}</a></td>                        
                        <td style="text-align:center;width:110px">
                            <!--<button id="R${group.groupeId}" onclick="return activeGroup(${group.groupeId});" <c:if test="${group.groupeValide==true}">style="visibility:hidden"</c:if>>Réactiver</button>-->
                                <button class="btn" onclick="editGroupe(${group.groupeId},'${group.groupeLibelle}','${connexionId}','${mot}')"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Modifier"></i></button>
                                <button class="btn" onclick="activateGroupe(this,${group.groupeId})"><i class="fas fa-check-circle" data-toggle="tooltip" data-placement="top" title="Réactiver"></i></button>
                            <button class="btn" onclick="deleteGroupe(this,${group.groupeId});"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Supprimer" ></i></button>
                        </td>
                    </tr>
                </c:forEach>

            </tbody>
        </table>

    </body>
</html>
