<%-- 
    Document   : menu
    Created on : 4 févr. 2019, 14:33:21
    Author     : antoinehurard
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <!--<link href="css/menu.css" type="text/css" rel="stylesheet">-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <link rel="icon" href="https://hippocampus.ec-nantes.fr/theme/image.php/archaius/theme/1550169298/favicon" >
        <title>Saturne</title>
    </head>
    <body>
        <header>
            <div id="imageHeader">
                <img src="img/logo-centrale.png" alt="Centrale Nantes" height="75" />
            </div>
        </header>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Saturne</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="load.do?id=${connexionId}&page=welcome" target="subframe">Home</a></li>
                    <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Utilisateurs
                      <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="load.do?id=${connexionId}&page=createUser" target="subframe">Créer</a></li>
                        <li><a href="load.do?id=${connexionId}&page=modifyUser" target="subframe">Modifier</a></li>
                        <li><a href="load.do?id=${connexionId}&page=managerUserListe" target="subframe">Gérer Liste</a></li>
                        <li><a href="load.do?id=${connexionId}&page=consulterUserListe" target="subframe">Consulter Liste</a></li>
                        <li><a href="load.do?id=${connexionId}&page=importerUser" target="subframe">Importer</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Groupe
                      <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="load.do?id=${connexionId}&page=createGroup" target="subframe">Créer</a></li>
                        <li><a href="load.do?id=${connexionId}&page=managerGroupListe" target="subframe">Gérer Liste</a></li>
                        <li><a href="load.do?id=${connexionId}&page=consulterGroupListe" target="subframe">Consulter Liste</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Questions
                      <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="load.do?id=${connexionId}&page=createQuestion" target="subframe">Créer</a></li>
                        <li><a href="load.do?id=${connexionId}&page=managerQuestionListe" target="subframe">Manager</a></li>
                        <li><a href="load.do?id=${connexionId}&page=consulterQuestionListe" target="subframe">Liste</a></li>
                       <li><a href="load.do?id=${connexionId}&page=managerMotcleListe" target="subframe">Mots-clés</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Questionnaire
                      <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="load.do?id=${connexionId}&page=createQuestionnaire" target="subframe">Créer</a></li>
                        <li><a href="load.do?id=${connexionId}&page=modifyQuestionnaire" target="subframe">Modifier</a></li>
                        <li><a href="load.do?id=${connexionId}&page=managerQuestionnaireListe" target="subframe">Manager</a></li>
                        <li><a href="load.do?id=${connexionId}&page=consulterQuestionnaireListe" target="subframe">Liste</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Session
                      <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="load.do?id=${connexionId}&page=createSession" target="subframe">Créer</a></li>
                        <li><a href="load.do?id=${connexionId}&page=managerSessionListe" target="subframe">Manager</a></li>
                        <li><a href="load.do?id=${connexionId}&page=consulterSessionListe" target="subframe">Liste</a></li>
                        <li><a href="#">Passer</a></li>
                        <li><a href="#">Entraînement</a></li>
                      </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#"><span class="fas fa-user"></span>${nom}</a></li>
                    <li><a href="disconnect.do?id=${connexionId}"><span class="fas fa-sign-out-alt"></span>Déconnexion</a></li>
                </ul>
            </div>
        </nav>
        <div class="container" style="margin-top:30px">
<!--            Container-->
            <!-- 4:3 aspect ratio -->
<!--            <div class="embed-responsive embed-responsive-4by3">
              <iframe class="embed-responsive-item" name="subframe" src="load.do?id=${connexionId}&page=welcome" scrolling="no"></iframe>
            </div>-->
            <iframe name="subframe" src="load.do?id=${connexionId}&page=welcome" scrolling="yes" style="top: 0;bottom: 0;left: 0;width: 100%;height:800px;border: 0;"></iframe>
        </div>
        <div class="jumbotron text-center" style="margin-bottom:0">
            @Centrale Nantes - France
        </div>
    </body>
</html>
