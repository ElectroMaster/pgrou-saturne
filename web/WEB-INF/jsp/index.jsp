<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Saturne</title>
        <link rel="stylesheet" type="text/css" href="css/login.css">
        <link rel="icon" href="https://hippocampus.ec-nantes.fr/theme/image.php/archaius/theme/1550169298/favicon" >
        
    </head>

    <body>
        <form:form action="index.do" method="POST">
            <table>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td rowspan="5">
                        <img src="img/saturne_no_backg.svg.png" alt="saturne" width="150">
                    </td>
                    <td class="header">Saturne</td>
                    <td><img src="img/logo-centrale.png" alt="ecn logo" width="80"></td>
                </tr>
                <tr>
                    <td colspan="2"><p class="error">
                        <c:choose>
                            <c:when test="${not empty message}">${message}</c:when>
                            <%--<c:otherwise>&nbsp;</c:otherwise>--%>
                        </c:choose>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><input type="text" name="login" placeholder="Username"></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="password" name="passw" placeholder="Password"></td>
                </tr>
                <tr>
                    <td colspan="2"><button type="submit">Sign in</button></td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
            </table>
        </form:form>
        <footer>
            @Equipe pgrou-saturne - Centrale Nantes - France
        </footer>
    </body>
</html>
