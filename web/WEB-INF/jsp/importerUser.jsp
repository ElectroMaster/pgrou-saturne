<%-- 
    Document   : consulterListe
    Created on : 2019-2-28, 9:16:23
    Author     : exia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!--<link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">-->
    <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <title>Saturne</title>
</head>

<body>
    <h1>Importer des utilisateur</h1>
    <br>
    <hr class="my-4">
    <h2>Importer depuis l'annulaire LDAP</h2>
    <br>
    <form>
        <div class="form-group row">
            <label for="critere" class="col-sm-2 col-form-label">Critère</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="critere" name="critere" placeholder="Critère">
            </div>
        </div>
        <div class="form-group row">
            <label for="role" class="col-sm-2 col-form-label">Rôle</label>
            <div class="col-sm-10">
            <select class="form-control" id="role" name="role">
                <!--TODO:-->
                <option value="1"> Administrateur</option>
                <option value="2"> Direction</option>
                <option value="3"> Enseignant</option>
                <option value="4"> Elève</option>
            </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="group" class="col-sm-2 col-form-label">Group</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="group" name="group" placeholder="Group">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Enregistrer</button>
            </div>
        </div>
    </form>
    <!-- perheps import from csv file -->

</body>
