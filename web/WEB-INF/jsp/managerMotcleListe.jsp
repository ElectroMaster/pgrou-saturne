<%-- 
    Document   : consulterMotcleListe
    Created on : 18 mars 2019, 10:28:30
    Author     : antoinehurard
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <!-- Bootstrap -->
        <link rel="stylesheet" href="css/custom-font.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="js/managerMotcleListe.js" type="text/javascript"></script>
        <title>Saturne</title>
    </head>
    <body>
        <h1>Liste des Mots-clés</h1>
        <table class="table table-hover table-striped table-fixed">
            <thead class="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Libellé</th>
                <th scope="col">Nombre de questions liées</th>
                <th scope="col">Opérations</th>
              </tr>
            </thead>
          <tbody style="overflow-y:auto">
              <c:forEach var="motcle" items="${motcleList}">
                  <tr>
                    <th scope="row"><c:out value="${motcle.motcleId}"/></th>
                    <td class="motcleLibelle"><c:out value="${motcle.motcleLibelle}"/></td>
                    <td class="nombreQuestions"><c:out value="${fn:length(motcle.questionCollection)}"/></td>
                    <td class="operationButton">
                        <button type="button" class="btn btn-light" onclick="editLine(this, ${motcle.motcleId})">
                            <i class="fas fa-edit"></i>
                        </button>
                        <button type="button" class="btn btn-light" onclick="deleteLine(this, ${motcle.motcleId})">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </td>
                  </tr>
              </c:forEach>
          </tbody>
        </table>
    </body>
</html>
