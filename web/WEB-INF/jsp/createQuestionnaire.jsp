<%--
    Document   : questionnaire
    Created on : 2019-3-2, 18:11:59
    Author     : exia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="css/custom-font.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/questionnaire.css">
    <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script src="js/createQuestionnaire.js" type="text/javascript"></script>
    <title>Saturne</title>
</head>

<body>


    <h1>Créer un questionnaire</h1>
    <div class="card-group" style="width: 70%; margin: auto;">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title card-active">Step 1:</h5>
                <p class="card-text">Créer un questionnaire</p>
                <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
            </div>
        </div>
        <div class="card">
            <div class="card-body card-no-active">
                <h5 class="card-title">Step 2:</h5>
                <p class="card-text">Ajouter des questions</p>
                <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
            </div>
        </div>
    </div>

    <br>
    <br>


    <form:form action="createQuestionnaire.do" method="POST" onsubmit="return validateMyForm();">
        <input type="hidden"  name="connexionId" value="${connexionId}">
        <div class="form-group">
            <label for="nom_questionnaire">Nom du questionnaire</label>
            <input type="text" class="form-control" id="nom_questionnaire" name="nom_questionnaire" placeholder="Mon nouvelle questionnaire">
            <div class="invalid-feedback">
            Le nom du quesitonnaire ne doit être vide.
            </div>
            <div class="valid-feedback">
            Looks good.
            </div>
        </div>

        <div class="form-group">
            <label for="descr_questionnaire">Description du questionnaire</label>
            <textarea class="form-control" id="descr_questionnaire" name="descr_questionnaire" rows="3" placeholder="Description text"></textarea>
            <div class="invalid-feedback">
            Le nom du quesitonnaire ne doit être vide.
            </div>
        </div>
        <div class="form-group" style="width:20%;">
            <label for="rtype_questionnaire">Type</label>
            <select class="form-control form-control-sm" id="type_questionnaire" name="type_questionnaire" onchange="chooseTypeQuest()">
                <!--TODO:-->
<!--                <option value="1">QCM</option>
                <option value="2">AutoEvaluation</option>-->
                <c:forEach var="typeQuestionnaire" items="${listTypequestionnaire}">
                    <c:choose>
                        <c:when test="${typeQuestionnaire.typequestionnaireLibelle == 'QCM'}">
                            <option value="${typeQuestionnaire.typequestionnaireId}" selected="true">
                            ${typeQuestionnaire.typequestionnaireLibelle}
                            </option>
                        </c:when>
                        <c:otherwise>
                            <option value="${typeQuestionnaire.typequestionnaireId}">
                            ${typeQuestionnaire.typequestionnaireLibelle}
                            </option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
        </div>
        <div class="form-group" id="duree_setting">
            <label for="duree">Durée</label>
            <div class="form-row" style="margin-left: 1px;">
            <span><input type="number" name="dureeH" id="dureeH" class="form-control col" value="" min="0" max="23" size="3" /></span>h
            <span><input type="number" name="dureeM" id="dureeM" class="form-control col" value="" min="0" max="59" size="3" /></span>mn
            </div>
        </div>
        <div class="form-group">
            <label style="display:block;" for="mot_cle">Mot clé</label>
            <button style="border-radius: 50%;" type="button" id="mot_cle" class="btn btn-light" onclick="addMot(this)"><i class="fa fa-plus fw"></i></button>
        </div>
        <div class="form-group">
            <label for="visibility_level">Niveau de visibilité</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="visibility_level" id="gridRadios1" value="3" checked>
                <label style="margin-left:20px;" class="form-check-label" for="gridRadios1">
                    <i aria-hidden="true" class="fa fa-lock fa-fw"></i>
                    Privé
                    <small style="display:block;">L'accès au questionnaire doit être explicitement admis pour chaque utilisateur.</small>
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="visibility_level" id="gridRadios2" value="2">
                <label style="margin-left:20px;" class="form-check-label" for="gridRadios2">
                    <i aria-hidden="true" class="fas fa-shield-alt"></i>
                    Restreint
                    <small style="display:block;">Le questionnaire est accessible à tout utilisateur connecté.</small>
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="visibility_level" id="gridRadios3" value="1">
                <label style="margin-left:20px;" class="form-check-label" for="gridRadios3">
                    <i aria-hidden="true" class="fas fa-globe-europe"></i>
                    Public
                    <small style="display:block;">Le questionnaire est accessible sans aucune authentification.</small>
                </label>
            </div>
        </div>
        <button type="submit" name="button" class="btn btn-primary">Enregistrer</button>
    </form:form>
</body>

</html>
