<%-- 
    Document   : consulterGroupList
    Created on : 2019-2-28, 11:04:26
    Author     : GUO
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/groupe.js" type="text/javascript"></script>
        <title>Saturne</title>
    </head>
    <body>
    <form>
        <h1>Liste des groupes</h1>
        <p id="rechercher"><button onclick="rechercherGroupeCons('${connexionId}')">Rechercher</button>
        <a href="load.do?id=${connexionId}&page=consulterGroupListe" target="subframe">Tous les groupes</a></p>
        <table class="table table-hover table-striped">
            <thead class="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Libelle</th>

              </tr>
            </thead>
          <tbody>
             
            <c:forEach var="group" items="${groupeList}">
            <tr>
                <td colspan="1">${group.groupeId}</td>
                <td colspan="1"><a href="load.do?id=${connexionId}&page=membreGroupeList&groupeId=${group.groupeId}&manager=false">${group.groupeLibelle}</a></td>
            </tr>

            </c:forEach>
           
          </tbody>
        </table>
    </form>
    </body>
</html>

