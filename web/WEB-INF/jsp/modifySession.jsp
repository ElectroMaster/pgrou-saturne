<%-- 
    Document   : modifySession
    Created on : 2019-3-13, 16:01:20
    Author     : GUO
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <link href="css/menu.css" type="text/css" rel="stylesheet">
        <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/ajax.js" type="text/javascript"></script>
        <script src="js/sessiontools.js" type="text/javascript"></script>
        <script src="js/calendar.js" type="text/javascript"></script>
        <title>Saturne</title>
    </head>
    <body>
    <form>
        <input type="hidden" name="id" id="id" value="${connexionId}" />
        <h1>Modifier une session</h1>
        <div class="form-group" style="width:20%;">
            <label for="questionnaireId">Questionnaire</label>
            <select class="form-control" name="questionnaireId" id="questionnaireId">
                <!--TODO:-->
                <option value="1">- Questionnaire 1 -</option>
                <option value="2">- Questionnaire 2 -</option>
                <c:forEach var="questionnaire" items="${listQuestionnaires}"><option value="${questionnaire.questionnaireId}">${questionnaire.questionnaireLibelle}</option>
                </c:forEach>
            </select>
            <input type="hidden" name="sessionId" id="sessionId" value="-1" />
        </div>
        <div class="form-group" style="width:20%;">
            <label for="date">Date de l'examen</label>
            <!--<input type="text" id="date" name="date" value="" size="10" readonly="readOnly"  onclick="linkCalendarTo(this); return false;" />-->
            <input class="form-control" type="date" id="date" name="date" value="" size="10"/>
        </div>
         <div class="form-group" style="width:20%;">
            <label for="hDebut">Début</label>
            <div class="form-row" style="margin-left: 1px;">
            <span><input type="number" name="hDebut" id="hDebut" class="form-control col" value="" min="0"  style="width:50px"  /></span> h
            <span><input type="number" name="mDebut" id="mDebut" class="form-control col" value="" min="0" max="59" style="width:50px"  /></span> mn
            </div>
        </div>
        <div class="form-group" style="width:20%;">
            <label for="duree">Durée</label>
            <div class="form-row" style="margin-left: 1px;">
           <span><input type="number" name="duree" id="duree" class="form-control" value="" min="0" style="width:50px"  /></span> minutes
           </div>
        </div>
        <div class="form-group" style="width:20%;">
            <label for="modeevaluationId">Mode évaluation</label>
            <select class="form-control" name="modeevaluationId" id="modeevaluationId">
                <!--TODO:-->
                <option value="1">- 1=vrai,0=faux -</option>
                <option value="2">- 2=vrai,-1=faux -</option>
                <c:forEach var="modeevaluation" items="${listModeevaluations}"><option value="${modeevaluation.modeevaluationId}">${modeevaluation.modeevaluationLibelle}</option>
                </c:forEach>
            </select>
        </div>
        <button type="submit" class="btn btn-primary" onclick="saveSession();">Enregistrer</button>
        <span id="message"></span>
    </form>
        
    </body>
</html>
